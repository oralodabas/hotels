<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCountriesTable extends Migration
{
    /**
     * CREATE TABLE `countries` (
    `id` mediumint(8) UNSIGNED NOT NULL,
    `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
    `iso3` char(3) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    `iso2` char(2) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    `phonecode` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    `capital` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    `currency` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    `currency_symbol` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    `tld` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    `native` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    `region` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    `subregion` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    `timezones` text COLLATE utf8mb4_unicode_ci,
    `translations` text COLLATE utf8mb4_unicode_ci,
    `latitude` decimal(10,8) DEFAULT NULL,
    `longitude` decimal(11,8) DEFAULT NULL,
    `emoji` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    `emojiU` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    `created_at` timestamp NULL DEFAULT NULL,
    `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `flag` tinyint(1) NOT NULL DEFAULT '1',
    `wikiDataId` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Rapid API GeoDB Cities'
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
     */
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('countries', function (Blueprint $table) {
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';
            $table->id('id');
            $table->string('name',255);
            $table->string('iso3',3)->nullable();
            $table->string('iso2',2)->nullable();
            $table->string('phonecode',255)->nullable();
            $table->string('capital',255)->nullable();
            $table->string('currency',255)->nullable();
            $table->string('currency_symbol',255)->nullable();
            $table->string('tld',255)->nullable();
            $table->string('native',255)->nullable();
            $table->string('region',255)->nullable();
            $table->string('subregion',255)->nullable();
            $table->text('timezones')->nullable();
            $table->text('translations')->nullable();
            $table->decimal('latitude',10,8)->nullable();
            $table->decimal('longitude',11,8)->nullable();
            $table->string('emoji',255)->nullable();
            $table->string('emojiU',255)->nullable();
            $table->string('wikiDataId',255)->nullable();
            $table->tinyInteger('flag')->default(1)->index();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
            $table->softDeletes();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('countries');
    }
}
