<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImageRoomTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('image_room', function (Blueprint $table) {
            $table->id();
            //FOREIGN KEY CONSTRAINTS
            $table->foreignId('room_id')
                ->constrained()
                ->onDelete('cascade');
            $table->foreignId('image_id')
                ->constrained()
                ->onDelete('cascade');
            $table->tinyInteger('flag')->index()->default(0);
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('image_room');
    }
}
