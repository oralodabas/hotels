<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHotelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hotels', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->json('name')->nullable();
            $table->json('overview')->nullable();
            $table->string('curl_url',500)->nullable();
            $table->string('google_map_url',500)->nullable();
            $table->string('domain_name',100)->nullable();
            $table->string('logo_name',100)->nullable();
            $table->integer('stars')->default(10)->nullable();
            $table->json('slug')->nullable();
            $table->string('check_in_from',100)->nullable();
            $table->string('check_in_until',100)->nullable();
            $table->string('check_out',100)->nullable();
            $table->decimal('lat', 10, 8)->default(0)->nullable();
            $table->decimal('lng', 11, 8)->default(0)->nullable();
            $table->enum('status', ['active', 'passive'])->default('passive')->nullable();
            $table->enum('is_deleted', ['active', 'passive'])->default('active')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('hotels');
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');

    }
}
