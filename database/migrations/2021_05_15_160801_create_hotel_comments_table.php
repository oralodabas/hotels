<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHotelCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hotel_comments', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';

            $table->bigIncrements('id');
            $table->bigInteger('hotel_id')->unsigned();
            $table->string('name');
            $table->string('email')->nullable();
            $table->string('country')->nullable();
            $table->string('travel_month', 2)->nullable();
            $table->string('travel_year', 4)->nullable();
            $table->tinyInteger('rating')->default(0);
            $table->text('comment_plus')->nullable();
            $table->text('comment_minus')->nullable();
            $table->enum('status', ['active', 'passive'])->default('active');
            $table->timestamps();

            $table->index(['hotel_id']);
            $table->foreign('hotel_id')->references('id')->on('hotels')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hotel_comments');
    }
}
