<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersSeeder::class);
        $this->call(CountriesSeeder::class);
        $this->call(StateSeeder::class);
        $this->call(CitiesSeeder::class);
        // \App\Models\User::factory(10)->create();
    }
}
