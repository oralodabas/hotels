<?php
namespace Database\Seeders;


use App\Models\User;
use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->dummyData() as $key) {
            $user = User::where('email', $key['email'])->first();

            if (!$user) {
                $users = new User();
                $users->email = $key['email'];
                $users->name = $key['name'];
                $users->password = $key['password'];
                $users->is_admin = $key['is_admin'];
                $users->save();
            }
        }

    }


    /**
     * @return array[]
     */
    public function dummyData(): array
    {
        return [
            1 => [
                'name' => 'admin',
                'email' => 'admin@admin.com',
                'password' => 'Kartal1903',
                'is_admin' => 1
            ]
        ];
    }
}
