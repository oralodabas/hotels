<?php

namespace Database\Seeders;

use App\Exceptions\ServiceException;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            $sql = file_get_contents(database_path() . '/seeders/sql/cities.sql');
            DB::unprepared($sql);

        } catch (\Exception $e) {
            throw new ServiceException($e->getMessage());
        }
    }
}
