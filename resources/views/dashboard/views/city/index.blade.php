{{-- Extends layout --}}
@extends('layout.default')

@section('styles')
<link rel="stylesheet" href="{{ asset('panel/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
 <link rel="stylesheet" href="{{ asset('panel/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
 <link href="{{ asset('assets/plugins/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    <div class="card card-custom">
        <div class="card-body">
            <div class="table-responsive">
                {!! $dataTable->table(['class' => 'table table-separate table-head-custom table-checkable']) !!}
            </div>
        </div>
    </div>
@endsection



{{-- Scripts Section --}}
@section('scripts')

    <script src="{{ asset('assets/js/pages/widgets.js') }}" type="text/javascript"></script>

    <script src="{{ asset('assets/panel/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('assets/panel/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{ asset('assets/panel/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{ asset('assets/panel/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.0.3/css/buttons.dataTables.min.css">
    <script src="https://cdn.datatables.net/buttons/1.0.3/js/dataTables.buttons.min.js"></script>
    <script src="{{ asset('assets/vendor/datatables/buttons.server-side.js')}}"></script>
    <script src="{{ asset('assets/js/pages/crud/ktdatatable/base/html-table.js') }}" type="text/javascript"></script>
    {!! $dataTable->scripts() !!}
@endsection
