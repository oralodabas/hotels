{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')

    @include('layout.base._alert')

    <div class="card card-custom">
        <form class="form" method="POST" action="{{ route('dashboard.cities.store') }}">
            {{csrf_field()}}

            <div class="card-body">
                <h3 class="font-size-lg text-dark font-weight-bold mb-6">{{ __('Add New City') }}:</h3>
                <div class="mb-15">
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">{{ __('City Name') }}:</label>
                        <div class="col-lg-6">
                            <input name="title" type="title" class="form-control" placeholder="Enter full name"/>
                            <span class="form-text text-muted">{{ __('Please enter city full name') }}</span>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">{{ __('Active') }}:</label>
                            <div class="col-lg-6">
                                <span class="switch">
                                    <label>
                                    <input name="status" type="checkbox" checked="checked"/>
                                    <span></span>
                                    </label>
                                </span>
                                <span class="form-text text-muted">{{ __('Is it active?') }}</span>
                            </div>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <div class="row">
                    <div class="col-lg-3"></div>
                    <div class="col-lg-6">
                        <button type="submit" class="btn btn-success mr-2">{{ __('Kaydet') }}</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection

{{-- Scripts Section --}}
@section('scripts')

@endsection