<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>{{ config('app.name') }} | @yield('title', $pageTitle ?? '')</title>
    <meta name="description" content="@yield('pageDescription', $pageDescription ?? '')"/>

    <!-- Styles -->

    <link rel="stylesheet" href="{{url('/assets/frontend/vendor/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{url('/assets/frontend/css/main.min.css')}}">
    <link href="{{url('/assets/frontend/css/icons.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{url('/assets/frontend/vendor/slick/slick.css')}}">
    <link rel="stylesheet" href="{{url('/assets/frontend/vendor/slick/slick-theme.css')}}">
    <link rel="stylesheet" href="{{url('/assets/frontend/vendor/niceselect/css/nice-select.css')}}">
    <link rel="stylesheet" href="{{url('/assets/frontend/vendor/datepicker/css/datepicker.min.css')}}">
@yield('HeadScripts')
</head>

<body>
@yield('header')

@yield('content')

<!-- Scripts -->
<script src="{{url('/assets/frontend/js/jquery-3.6.0.min.js')}}"></script>
<script src="{{url('/assets/frontend/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{url('/assets/frontend/vendor/slick/slick.min.js')}}"></script>
<script src="{{url('/assets/frontend/vendor/niceselect/js/jquery.nice-select.min.js')}}"></script>
<script src="{{url('/assets/frontend/vendor/datepicker/js/datepicker.min.js')}}"></script>
<script src="{{url('/assets/frontend/vendor/datepicker/js/i18n/datepicker.en.js')}}"></script>
<script src="{{url('/assets/frontend/vendor/mask/jquery.mask.js')}}"></script>
<script src="{{url('/assets/frontend/vendor/sticky/jquery.sticky-sidebar.min.js')}}"></script>
<script src="{{url('/assets/frontend/js/jquery.lazy.min.js')}}"></script>
<script src="{{url('/assets/frontend/js/main.js')}}"></script>
@yield('FootScripts')
</body>
</html>
