@extends('frontend.layouts.master')

@section('header')

@endsection

@section('content')
    <!-- Main Start -->
    <div class="container mt-100px">
        <div class="row">

            <div class="col-12 col-xl-9 main-area">
                @if($dataset['hotelGroups'])
                    @foreach($dataset['hotelGroups'] as $key=>$hotels)

                <div class="ps-lg-8 collapse-area">
                    <button class="btn shadow-none fs-18" type="button" data-bs-toggle="collapse" data-bs-target="#collapseExample"
                            aria-expanded="false" aria-controls="collapseExample">
                        {{strtoupper($key)}}
                    </button>

                    <div class="collapse show" id="collapseExample">
                        <ul class="row nav mt-25">
                            @foreach($hotels as $hotel)
                            <li class="nav-item col-12 col-md-6"><a href="{{url(app()->getLocale().'/hotel/'.$hotel->getTranslation('slug',app()->getLocale(),false))}}" class="nav-link text-cello text-hover-burning px-0">{{$hotel->getTranslation('name',app()->getLocale(),false)}}</a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>

    <!-- Main End -->
@endsection
