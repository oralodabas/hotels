@extends('frontend.layouts.master')

@section('header')
    @include('frontend.includes.show.head')
    @include('frontend.includes.show.slider')
@endsection

@section('content')
    <!-- Main Start -->
    <main class="main mt-2 mt-lg-5">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-3 order-2 order-lg-1 mobile-hidden sidebar__inner">
                    <a class="btn btn-menu-mobile d-block d-lg-none shadow-none mb-3 radius-12px" data-bs-toggle="collapse" href="#optionsMenu" role="button" aria-expanded="false" aria-controls="optionsMenu">Menu</a>
                    <div class="collapse sidebar" id="optionsMenu">
                        <nav class="nav flex-column side-menu">
                            <a href="#overview" class="nav-link text-gullgray text-hover-cello fs-24"><i class="icon-dots align-middle me-2"></i>{{__('site.overview')}}</a>
                            <a href="#rooms" class="nav-link text-gullgray text-hover-cello fs-24"><i class="icon-door align-middle me-2"></i>{{__('site.rooms')}}</a>
                            <a href="#location" class="nav-link text-gullgray text-hover-cello fs-24"><i class="icon-marker align-middle me-2"></i>{{__('site.location')}}</a>
                            <a href="#facilities" class="nav-link text-gullgray text-hover-cello fs-24"><i class="icon-heart align-middle me-2"></i>{{__('site.facilities')}}</a>
                            <a href="#gallery" class="nav-link text-gullgray text-hover-cello fs-24"><i class="icon-image align-middle me-2"></i>{{__(('site.gallery'))}}</a>
                            <a href="#reviews" class="nav-link text-gullgray text-hover-cello fs-24"><i class="icon-bubble align-middle me-2"></i>{{__('site.reviews')}} <span class="badge rounded-pill ms-2 transition-3s fs-14 fw-500 align-middle mt--9"></span></a>
                            <a href="#faq" class="nav-link text-gullgray text-hover-cello fs-24"><i class="icon-question align-middle me-2"></i>{{__('site.faq')}}</a>
                        </nav>
                    </div>
                </div>
                <div class="col-12 col-lg-6 order-3 order-lg-2 main-content mt-3 mt-lg-0">
                    <!-- Overview Start -->
                    <div id="overview">
                        <!-- Title Area Start -->
                        <div class="title-area fs-24 fw-500">
                            <i class="icon-dots icon mr-24 align-middle text-burning"></i>{{__('site.overview')}}
                        </div>
                        <!-- Title Area End -->

                        <!-- Content Start -->
                        <div class="content text-gullgray mt-2 fw-400">
                            {{$dataset['hotel']->overview}}
                        </div>
                        <!-- Content End -->
                    </div>
                    <!-- Overview End -->

                    <!-- Rooms Start -->
                    <div id="rooms" class="mt-100px">
                        <!-- Title Area Start -->
                        <div class="title-area fs-24 fw-500">
                            <i class="icon-dots icon mr-24 align-middle text-burning"></i>{{__('site.rooms')}}
                        </div>
                        <!-- Title Area End -->

                        <!-- Accordion Room Start -->
                        @include('frontend.includes.show.rooms')
                        <!-- Accordion Room End -->
                    </div>
                    <!-- Rooms End -->

                    <!--Info Offcanvas Rooms -->
                    @include('frontend.includes.show.room_detail')
                    <!-- Info Offcanvas Rooms End-->
                    <!-- Check Prices -->
                    @include('frontend.includes.show.check_price')
                    <!-- Check Prices End -->

                    <!-- Location Start -->
                    <div id="location" class="mt-100px">
                        <!-- Title Area Start -->
                        <div class="title-area fs-24 fw-500">
                            <i class="icon-marker icon mr-24 align-middle text-burning"></i>{{__('location')}}
                        </div>
                        <!-- Title Area End -->

                        <!-- Checks Start -->
                        <div class="checks d-flex mt-3">

                        </div>
                        <!-- Checks End -->

                        <!-- Map Area Start -->
                        <div class="map-area">
                            <iframe class="radius-16px my-3" src="{{$dataset['hotel']->google_map_url}}" width="600" height="450" style="border:0; margin-top: -150px" allowfullscreen="" loading="lazy"></iframe>
                        </div>
                        <!-- Map Area End -->

                    <!-- Facilities Start -->
                    @include('frontend.includes.show.facilities')

                    <!-- Facilities End -->

                    <!-- Gallery Start -->
                    @include('frontend.includes.show.gallery')
                    <!-- Gallery End -->

                    <!-- Reviews Start -->
                    @include('frontend.includes.show.review_show')
                    <!-- Reviews End -->

                    <!-- Write a Review Start -->
                    @include('frontend.includes.show.review_write')
                    <!-- Write a Review End -->

                    <!-- FAQ Start -->
                    @include('frontend.includes.show.faq')
                    <!-- FAQ End -->
                    </div>
                    <!-- Location End -->
                </div>
                @include('frontend.includes.show.checkin')
            </div>
        </div>
    </main>
@include('frontend.includes.show.gallery_two')
    <!-- Main End -->
@endsection
@section('FootScripts')

<script>
    let crawl_url = "{{route('crawl.data')}}";
    let check_price = "{{__('site.checkPrices')}}";
    let send_review = "{{__('site.sendReview')}}";
    let loading = "{{__('site.loading')}}";
    let show_all_facilities = "{{__('site.show_all_facilities')}}";
    let total_price = "{{__('site.totalPrice')}}";
    let nightly_per_price = "{{__('site.nightlyPerPrice')}}";
    let book_now = "{{__('site.bookNow')}}";
    let session_get_room = "{{route('session.get.room')}}";
    let token = "{{csrf_token()}}";
    let add_review = "{{route('add.review',['id'=>$dataset['hotel']->id])}}";
</script>
@endsection
