<div class="offcanvas offcanvas-end" tabindex="-1" id="checkPrices" aria-labelledby="checkPricesLabel" style="visibility: visible;" aria-modal="true" role="dialog">
    <div class="offcanvas-header">

        <h5 class="offcanvas-title text-cello fs-24 fw-bold" id="checkPricesLabel">{{__('site.checkPrices')}}</h5>
        <button type="button" class="btn-close text-white bg-burning shadow-none rounded-circle" data-bs-dismiss="offcanvas" aria-label="Close"><i class="icon-exit fs-14"></i></button>
    </div>
    <div class="offcanvas-body">
        <form id="check_prices">
            <div class="mb-3">
                <label for="" class="text-uppercase fs-14 fw-bold mb-2 text-gullgray">{{__('site.inputDate')}}</label>
                <input type="text" name="start" class="form-control shadow-none radius-8px in-out datepicker-here" placeholder="{{__('site.inputDatePlace')}}" data-language="en" data-multiple-dates="1" data-multiple-dates-separator=", " data-position="bottom left">
            </div>
            <div class="mb-3">
                <label for="" class="text-uppercase fs-14 fw-bold mb-2 text-gullgray">{{__('site.outputDate')}}</label>
                <input type="text" class="form-control shadow-none radius-8px in-out datepicker-here" name="end" placeholder="{{__('site.outputDatePlace')}}" data-language="en" data-multiple-dates="1" data-multiple-dates-separator=", " data-position="bottom left">
            </div>
            <div class="mb-3">
                <label for="" class="text-uppercase fs-14 fw-bold mb-2 text-gullgray">{{__('site.adults')}}</label>
                <input type="text" name="adults" class="form-control just-number shadow-none radius-8px " placeholder="{{__('site.adults')}}" maxlength="3">
            </div>
            <div class="mb-3">
                <label for="" class="text-uppercase fs-14 fw-bold mb-2 text-gullgray">{{__('site.kids')}}</label>
                <input type="text" name="children" class="form-control just-number shadow-none radius-8px " placeholder="{{__('site.kids')}}" maxlength="3">
            </div>
            <div class="alert alert-danger" id="check_price_alert" role="alert" style="display: none">
                {{__('site.room_dangers')}}
            </div>
            <div class="mt-3">
                <button id="check_prices_submit" data-loading-text="<i class='fa fa-spinner fa-spin '></i> {{__(('site.loading'))}}" class="btn text-white bg-burning radius-16px w-100 btn-static text-uppercase hover-bg-cello">{{__('site.checkAvailability')}}</button>
            </div>

            <input type="hidden" name="curl_url" value="@if($dataset['hotel']){{$dataset['hotel']->curl_url}}@endif">
        </form>
    </div>
</div>

