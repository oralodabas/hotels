<div class="accordion mt-2 rooms" id="room_list">
    @foreach($dataset['hotel']->rooms()->get() as $room)
        <div class="accordion-item room-detail radius-16px mb-4">
            <h2 class="accordion-header" id="headingOne">
                <button id="myCollapsible" class="accordion-button radius-16px d-flex shadow-none collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#none" aria-expanded="true" aria-controls="itemOne">

                    <div class="left d-flex">
                        <!-- Image Start -->
                        @if($room->images()->first())
                            <img alt="{{$room->images()->first()->name}}" class="thumbnail radius-16px transition-3s lazy" src="{{asset('storage/'.$room->images()->first()->path.$room->images()->first()->name)}}" style="display: inline;">
                            <!-- Image End -->
                    @endif

                    <!-- Content Start -->
                        <div class="content ms-3">
                            <!-- Title Start -->
                            <div class="title fw-600 text-cello fs-24">{{$room->getTranslation('title',app()->getLocale(),false)}}</div>
                            <!-- Title End -->

                            <!-- Desc Start -->
                            <div class="desc text-gullgray fw-light mt-2 fs-14"><i class="icon-bed me-2 text-burning align-middle"></i>{{__('site.singleOrDouble')}}</div>
                            <!-- Desc End -->
                        </div>
                        <!-- Content End -->
                    </div>

                    <!-- Button Start -->
                    <div class="btn bg-cello text-white ms-auto radius-12px text-uppercase hover-bg-burning" data-bs-toggle="offcanvas" data-bs-target="#checkPrices" aria-controls="checkPrices">{{__('site.checkPrices')}}</div>
                    <!-- Button End -->
                </button>
            </h2>
            @include('frontend.includes.show.open_rooms')
        </div>
    @endforeach
</div>
