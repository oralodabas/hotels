<div id="facilities" class="mt-100px">
    <!-- Title Area Start -->
    <div class="title-area fs-24 fw-500">
        <i class="icon-heart icon mr-24 align-middle text-burning"></i>{{__('site.facilities')}}
    </div>
    <!-- Title Area End -->

    <div class="row mt-3">

        @foreach($dataset['facilities'] as $facility)

            <div class="col-12 col-md-6 d-flex mb-5">
                <div class="icon-box">
                    <i class="{{$facility['icon_name']}} text-burning fs-20 me-3"></i>
                </div>
                <div class="content">
                    <div class="title fs-18 text-cello mb-2 fw-600">{{$facility['name']}}</div>
                    <ul class="nav flex-column check-list">
                        @foreach($facility['facilities'] as $item)
                            <li class="nav-item text-gullgray">{{$item->getTranslation('name',app()->getLocale(),false)}}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        @endforeach
    </div>
</div>
