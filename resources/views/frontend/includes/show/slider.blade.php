<!-- Slider Area Start -->
<div class="slider-area">
    <div class="container">
        <!-- Slider Start -->
        @if($dataset['hotel']->images()->get())
            <div class="slider">
                @foreach($dataset['hotel']->images()->get() as $image)
                    <div class="slide">
                        <img class="radius-16px lazy" data-src="{{asset('storage/'.$image->path.$image->name)}}"
                             alt="{{$image->title}}">
                    </div>
                @endforeach
            </div>
    @endif
    <!-- Slider End -->

        <!-- Slider Buttons Start -->
        <div class="slider-buttons d-none d-lg-flex">
            <div class="content ms-auto d-flex align-items-center">
                <div class="left-arrow text-burning text-hover-cello transition-3s"><i class="icon-left-arrow"></i>
                </div>
                <div class="count mx-3 text-gullgray fs-18"></div>
                <div class="right-arrow text-burning text-hover-cello transition-3s"><i class="icon-right-arrow"></i>
                </div>
            </div>
        </div>
        <!-- Slider Buttons End -->
    </div>
</div>
<!-- Slider Area End -->
