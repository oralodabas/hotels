<!-- Header Start -->

<header class="header py-5">
    <div class="container d-flex align-items-center">
        <div class="left d-flex">
            <!-- Image Area Start -->
            <a href="{{url('hotel/'.app()->getLocale().'/'.$dataset['hotel']->getTranslation('slug',app()->getLocale(),false))}}" class="image-area">
                <img class="image" src="{{asset('storage/uploads/hotels/logo/'.$dataset['hotel']->logo_name)}}" alt="{{$dataset['hotel']->getTranslation('name',app()->getLocale(),false)}}">
            </a>
            <!-- Image Area End -->

            <!-- Content Start -->
            <div class="content-area ms-3">
                <!-- Stars Start -->
                <ul class="stars nav">
                @for($i = 0;$i < $dataset['hotel']->stars; $i++)
                    @if($i>5) @break @endif
                    <li class="nav-item"><i class="icon-star"></i></li>
                    @endfor
                </ul>
                <!-- Stars End -->

                <!-- Title Start -->
                <div class="text-cello title fw-bold">{{$dataset['hotel']->getTranslation('name',app()->getLocale(),false)}}<a href="#" class="marker text-cornflower"><i class="icon-marker icon"></i></a></div>
                <!-- Title End -->

                <!-- Meta Start -->
                @if($dataset['reviews']->avg('point')!= null)
                <div class="meta d-flex align-items-center">
                    <!-- Icon Start -->
                    <i class="icon-bubble icon text-cello mt-1"></i>

                    <!-- Vote Start -->
                    <div class="vote fw-bold text-cello mx-3">{{$dataset['reviews']->avg('point')}}</div>

                    <span class="text-gullgray">{{$dataset['reviews']->count()}} {{__('site.reviews')}}</span>
                    <!-- Reviews End -->

                    <a href="#reviews" class="btn text-cello shadow-none p-2 mt-1 ms-2"><i class="icon-chevron-right"></i></a>
                </div>
                @endif
                <!-- Meta End -->
            </div>
            <!-- Content Area End -->
        </div>

        <!-- Right Area Start -->
        <div class="right-area ms-auto">
            <a class="btn text-white p-0 shadow-none bg-burning text-uppercase btn-available radius-16px transition-3s hover-bg-cello fw-600" data-bs-toggle="offcanvas" role="button"  data-bs-target="#checkPrices" aria-controls="checkPrices">{{__('site.check')}}</a>
        </div>
        <!-- Right Area End -->
    </div>
</header>
<!-- Header End -->

