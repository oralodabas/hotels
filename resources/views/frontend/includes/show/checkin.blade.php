<div class="col-12 col-lg-3 order-1 order-lg-3 mb-4 mb-lg-0 sidebar__inner pl-70px">
    <div class="sidebar">
        <div class="checkinbox d-flex d-lg-block border border-1 border-porcelain radius-8px">
            <ul class="nav flex-column mb-4 flex-50">
                <li class="nav-item header-text d-flex align-items-start mb-1">
                    <span class="icon"><i class="icon-in text-burning fs-18"></i></span>
                    <span class="text-uppercase text-gullgray fs-12 fw-500">{{__('site.checkIn')}}</span>
                </li>
                <li class="nav-item ps-nav">
                    <span class="text d-block text-cello fs-16 fw-500">{{__('site.from')}} {{$dataset['hotel']->check_in_from}}</span>
                    <span class="text d-block text-cello fs-16 fw-500">{{__('site.until')}} {{$dataset['hotel']->check_in_until}}</span>
                </li>
            </ul>
            <ul class="nav flex-column flex-50">
                <li class="nav-item header-text d-flex align-items-start mb-1">
                    <span class="icon"><i class="icon-out text-burning fs-18"></i></span>
                    <span class="text-uppercase text-gullgray fs-12 fw-500">{{__('site.checkOut')}}</span>
                </li>
                <li class="nav-item ps-nav">
                    <span class="text d-block text-cello fs-16 fw-500">{{__('site.until')}} 12:00</span>
                </li>
            </ul>
        </div>

        <a href="#" class="helpbox mt-4 border border-1 border-porcelain radius-8px d-none d-lg-flex">
            <i class="icon-support icon me-4 text-burning"></i>
            <div class="text text-cello fs-16 fw-600">{{__('site.help')}}</div>
        </a>
    </div>
</div>
