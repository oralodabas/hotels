
<div id="faq" class="mt-100px pb-5">
    <!-- Title Area Start -->
    <div class="title-area fs-24 fw-500">
        <i class="icon-question icon mr-24 align-middle text-burning"></i>{{__('site.faq_long')}}
    </div>
    <!-- Title Area End -->

    <div class="accordion-area mt-16" id="faq">
        @foreach($dataset['hotel']->faqs()->get() as $faq)
        <div class="accordion-item bg-transparent mb-3 border-porcelain radius-8px border overflow-hidden">
            <input type="checkbox" id="item{{$faq->id}}">
            <label for="item{{$faq->id}}" class="d-flex align-items-center"><i class="icon-support text-gullgray fs-24"></i><span class="text-gullgray ms-3 fs-18 fw-600">{{$faq->getTranslation('questions', app()->getLocale(),false)}}</span></label>
            <div class="text-gullgray item-content transition-3s">
                {{$faq->getTranslation('answers', app()->getLocale(),false)}}
            </div>
        </div>
        @endforeach

    </div>
</div>
