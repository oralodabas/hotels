
<!-- Gallery 2 Start -->
<div id="galleryTwo" class="transition-3s position-fixed top-0 start-0 end-0 bottom-0 bg-cello">
    <a href="#" class="btn-close position-absolute bg-burning text-white shadow-none close-gallery"><i class="icon-exit"></i></a>
    <div class="gallery-top-area position-relative">
        <div class="prev-arrow position-absolute top-50 bg-white text-dark transition-3s start-0"><i class="icon-chevron-left"></i></div>
        <div class="gallery-for">
            @foreach($dataset['hotel']->images()->get() as $image)
                <div class="slide">
                        <img src="{{asset('storage/'.$image->path.$image->name)}}" alt="" class="image">

                </div>
            @endforeach
        </div>
        <div class="next-arrow position-absolute top-50 end-0 bg-white text-dark transition-3s"><i class="icon-chevron-right"></i></div>
    </div>
    <div class="gallery-nav">
        @foreach($dataset['hotel']->images()->get() as $image)
            <div class="slide">
                <img src="{{asset('storage/'.$image->path.$image->name)}}" alt="" class="image">
            </div>
        @endforeach
           </div>
</div>
<!-- Gallery 2 End -->
