<div id="itemOne" class="accordion-collapse collapse" aria-labelledby="headingOne" data-bs-parent="#rooms" style="">
    <div class="accordion-body">
        <!-- Images Start -->
        <div class="images d-flex justify-content-between">
            @foreach($room->images()->get() as $roomImage)
                <a href="#" class="gallery-one"><img class="radius-16px image lazy" alt="" src="{{asset('storage/'.$roomImage->path.$roomImage->name)}}" style=""></a>
            @endforeach
        </div>
        <!-- Images End -->

        <!-- Gallery Link Start -->
        <a class="text-cornflower mt-3 d-inline-block text-hover-cello transition-3s fs-14" data-bs-toggle="offcanvas" href="#infoOffCanvas" role="button" aria-controls="infoOffCanvas">{{__('site.showAll')}} <i class="icon-chevron-right align-middle fs-12"></i></a>
        <!-- Gallery Link End -->

        <!-- Bottom Start -->
        <div class="bottom mt-4 d-block d-lg-flex align-items-center">
            <div class="left">
                <div class="d-flex d-lg-block align-items-end">
                    <div class="flex-50">
                        <!-- Price Area Start -->
                        <div class="price-area">
                            <!-- Old Price Start -->
                            <div class="old-price text-gullgray text-decoration-line-through fs-12">$908</div>
                            <!-- Old Price End -->

                            <!-- Price Start -->
                            <div class="price text-burning fs-24 fw-bold">$408</div>
                            <!-- Price End -->
                        </div>
                        <!-- Price Area End -->

                        <!-- Text Start -->
                        <div class="text-cello fs-12">nightly price per room</div>
                        <!-- Text End -->
                    </div>

                    <div class="flex-50">
                        <!-- Nights Text Start -->
                        <div class="nights-text text-burning my-1 fw-500 fs-12">total $1,334 for 4 nights</div>
                        <!-- Nights Text End -->

                        <!-- Cancellation Date Start -->
                        <div class="cancellation-date text-cornflower py-1 px-2 rounded-pill fs-10 text-center">Free cancellation Until 07/12/21</div>
                        <!-- Cancellation Date End -->
                    </div>
                </div>
            </div>
            <div class="right ms-auto">
                <a href="#" class="btn btn-standart radius-12px shadow-none bg-burning text-white text-uppercase  hover-bg-cello btn-mobile-w-100 fw-600">Book Now</a>
            </div>
        </div>
        <!-- Bottom End -->
    </div>
</div>
