<form id="add_review_form" action="" name="add_review_form">
    <input type="hidden" name="_token" value="{{csrf_token()}}">
<div id="addReview" class="mt-100px">
    <!-- Title Area Start -->
    <div class="title-area fs-24 fw-500">
        <i class="icon-bubble icon mr-24 align-middle text-burning"></i>{{__('site.writeReview')}}
    </div>
    <!-- Title Area End -->

    <div class="row mt-3">
        <div class="col-12 col-md-6 mb-3">
            <label for="" class="text-gullgray text-uppercase fs-12 fw-500 mb-2">{{__('site.name')}}</label>
            <input type="text" class="form-control shadow-none radius-8px" required name="name" placeholder="{{__('site.name')}}">
        </div>
        <div class="col-12 col-md-6 mb-3">
            <label for="" class="text-gullgray text-uppercase fs-12 fw-500 mb-2">{{__('site.sur_name')}}</label>
            <input type="text" class="form-control shadow-none radius-8px" required name="surname" placeholder="{{__('site.sur_name')}}">
        </div>
        <div class="col-12 col-md-6 mb-3">
            <label for="" class="text-gullgray text-uppercase fs-12 fw-500 mb-2">{{__('site.email')}}</label>
            <input type="text" class="form-control shadow-none radius-8px" required name="email" placeholder="{{__('site.email')}}">
        </div>
        <div class="col-12 col-md-6 mb-3">
            <label for="" class="text-gullgray text-uppercase fs-12 fw-500 mb-2">{{__('site.country')}}</label>
            <select name="country" id="" class="select form-control w-100 shadow-none" required>
                <option value="turkey">{{__('site.turkey')}}</option>
            </select>
        </div>
        <div class="col-12 col-md-6 mb-3">
            <div class="text-gullgray text-uppercase fs-12 fw-500 mb-2">{{__('site.when_did_you_travel')}}</div>
            <span class="text-gullgray">{{__('site.please_select_your_travel')}}</span>
        </div>
        <div class="col-12 col-md-6 mb-3">
            <div class="row">
                <div class="col-12 col-md-6">
                    <label for="" class="text-gullgray text-uppercase fs-12 fw-500 mb-2">{{__('site.month')}}</label>
                    <select name="month" id="" class="select form-control w-100 shadow-none">
                        <option value="1">{{__('site.january')}}</option>
                        <option value="2">{{__('site.february')}}</option>
                        <option value="3">{{__('site.march')}}</option>
                        <option value="4">{{__('site.april')}}</option>
                        <option value="5">{{__('site.may')}}</option>
                        <option value="6">{{__('site.june')}}</option>
                        <option value="7">{{__('site.july')}}</option>
                        <option value="8">{{__('site.august')}}</option>
                        <option value="9">{{__('site.september')}}</option>
                        <option value="10">{{__('site.october')}}</option>
                        <option value="11">{{__('site.november')}}</option>
                        <option value="12">{{__('site.december')}}</option>
                    </select>
                </div>
                <div class="col-12 col-md-6">
                    <label for="" class="text-gullgray text-uppercase fs-12 fw-500 mb-2">{{__('site.year')}}</label>
                    <select name="year" id="" class="select form-control w-100 shadow-none">
                       @for($i=date('Y'); $i>= date('Y')-10;$i--)
                            <option value="{{$i}}">{{$i}}</option>
                        @endfor
                    </select>
                </div>
            </div>
        </div>
        <div class="col-12 mb-3">
            <label for="" class="text-gullgray text-uppercase fs-12 fw-500 mb-2">{{__('site.rating')}}</label>

            <div class="points d-flex justify-content-between">
                <input type="radio" class="btn-check" name="rating" id="one" required value="1" autocomplete="off">
                <label class="btn shadow-none radius-8px" for="one" data-id="1">1</label>
                <input type="radio" class="btn-check" name="rating" id="two" value="2" autocomplete="off">
                <label class="btn shadow-none radius-8px" for="two" data-id="2">2</label>
                <input type="radio" class="btn-check" name="rating" id="three" value="3" autocomplete="off">
                <label class="btn shadow-none radius-8px" for="three"  data-id="3">3</label>
                <input type="radio" class="btn-check" name="rating" value="4" id="four" autocomplete="off">
                <label class="btn shadow-none radius-8px" for="four" data-id="4">4</label>
                <input type="radio" class="btn-check" name="rating" value="5" id="five" autocomplete="off">
                <label class="btn shadow-none radius-8px" for="five" data-id="5">5</label>
                <input type="radio" class="btn-check" name="rating" value="6" id="six" autocomplete="off">
                <label class="btn shadow-none radius-8px" for="six" data-id="6">6</label>
                <input type="radio" class="btn-check" name="rating" value="7" id="seven" autocomplete="off">
                <label class="btn shadow-none radius-8px" for="seven" data-id="7">7</label>
                <input type="radio" class="btn-check" name="rating" value="8" id="eight" autocomplete="off">
                <label class="btn shadow-none radius-8px" for="eight" data-id="8">8</label>
                <input type="radio" class="btn-check" name="rating" value="9" id="nine" autocomplete="off">
                <label class="btn shadow-none radius-8px" for="nine" data-id="9">9</label>
                <input type="radio" class="btn-check" name="rating" value="10" id="ten" autocomplete="off">
                <label class="btn shadow-none radius-8px" for="ten" data-id="10">10</label>
            </div>

            <div class="bottom d-flex mt-2">
                <span class="text-gullgray fs-12 fw-500">{{__('site.very_bad')}}</span>
                <span class="text-gullgray fs-12 fw-500 ms-auto">{{__('site.excellent')}}</span>
            </div>
        </div>

        <div class="col-12 mb-3">
            <label for="" class="text-burning text-uppercase fs-12 fw-bold mb-2">{{__('site.comment')}}</label>
            <textarea name="comment" id="" cols="30"  rows="5" required class="form-control radius-8px shadow-none" placeholder=""></textarea>
        </div>
        <div class="col-12">
            <button type="submit" class="btn btn-send bg-burning text-white text-uppercase radius-16px hover-bg-cello fs-18 fw-600" id="add_review_submit"  data-loading-text="<i class='fa fa-spinner fa-spin '></i> {{__(('site.loading'))}}">{{__('site.sendReview')}}</button>
        </div>
    </div>
</div>
</form>
