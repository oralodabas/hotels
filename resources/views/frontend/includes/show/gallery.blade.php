<div id="gallery" class="mt-100px">
    <!-- Title Area Start -->
    <div class="title-area fs-24 fw-500">
        <i class="icon-image icon mr-24 align-middle text-burning"></i>{{__('site.gallery')}}
    </div>
    <!-- Title Area End -->

    <!-- Row Start -->
    <div class="row mt-3">
        @foreach($dataset['hotel']->images()->get() as $i=>$image)
            @if($i==6)@break @endif
            <div class="@if($i==0){{'col-12 mb-4'}}@elseif($i>0 && $i<3){{'col-6 mb-4'}}@else{{'col-4'}} @endif ">
                <a href="#" class="img-link">
                    <img src="{{asset('storage/'.$image->path.$image->name)}}" class="@if($i==0){{'wide'}}@elseif($i>0 && $i<3){{'half'}}@else{{'little'}} @endif  radius-16px object-cover gallery-two" alt="">
                </a>
            </div>
        @endforeach
    </div>
    <!-- Row End -->
</div>
