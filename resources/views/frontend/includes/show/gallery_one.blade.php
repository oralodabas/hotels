<!-- Gallery 1 Start -->
<div id="galleryOne" class="transition-3s d-flex align-items-center justify-content-center">
    <div class="content w-50 bg-white position-relative">
        <a href="#" class="btn-close position-absolute bg-burning text-white shadow-none close-gallery"><i class="icon-exit"></i></a>
        <div class="gallery-top-area position-relative">
            <div class="prev-arrow position-absolute top-50 bg-white text-dark transition-3s start-0"><i class="icon-chevron-left"></i></div>
            <div class="gallery-for">
                <div class="slide">
                    <img src="assets/img/slider/slider-img-01.jpg" alt="" class="image">
                </div>
                <div class="slide">
                    <img src="assets/img/slider/slider-img-02.jpg" alt="" class="image">
                </div>
                <div class="slide">
                    <img src="assets/img/slider/slider-img-03.jpg" alt="" class="image">
                </div>
                <div class="slide">
                    <img src="assets/img/slider/slider-img-01.jpg" alt="" class="image">
                </div>
                <div class="slide">
                    <img src="assets/img/slider/slider-img-02.jpg" alt="" class="image">
                </div>
                <div class="slide">
                    <img src="assets/img/slider/slider-img-03.jpg" alt="" class="image">
                </div>
                <div class="slide">
                    <img src="assets/img/slider/slider-img-01.jpg" alt="" class="image">
                </div>
                <div class="slide">
                    <img src="assets/img/slider/slider-img-02.jpg" alt="" class="image">
                </div>
            </div>
            <div class="next-arrow position-absolute top-50 end-0 bg-white text-dark transition-3s"><i class="icon-chevron-right"></i></div>
        </div>
        <div class="gallery-nav">
            <div class="slide">
                <img src="assets/img/slider/slider-img-01.jpg" alt="" class="image">
            </div>
            <div class="slide">
                <img src="assets/img/slider/slider-img-02.jpg" alt="" class="image">
            </div>
            <div class="slide">
                <img src="assets/img/slider/slider-img-03.jpg" alt="" class="image">
            </div>
            <div class="slide">
                <img src="assets/img/slider/slider-img-01.jpg" alt="" class="image">
            </div>
            <div class="slide">
                <img src="assets/img/slider/slider-img-02.jpg" alt="" class="image">
            </div>
            <div class="slide">
                <img src="assets/img/slider/slider-img-03.jpg" alt="" class="image">
            </div>
            <div class="slide">
                <img src="assets/img/slider/slider-img-01.jpg" alt="" class="image">
            </div>
            <div class="slide">
                <img src="assets/img/slider/slider-img-02.jpg" alt="" class="image">
            </div>
        </div>
    </div>
</div>
<!-- Gallery 1 End -->
