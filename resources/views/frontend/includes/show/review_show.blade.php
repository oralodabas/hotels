@if($dataset['reviews']->count()>0)
<div id="reviews" class="mt-100px">
    <!-- Title Area Start -->
    <div class="title-area fs-24 fw-500">
        <i class="icon-bubble icon mr-24 align-middle text-burning"></i>Reviews
    </div>
    <!-- Title Area End -->

    <ul class="nav flex-column reviews mt-3">
    @foreach($dataset['reviews'] as $review)
        @php
        $info = json_decode($review->info);
        @endphp

        <!-- Review  Start -->
        <li class="nav-item review d-flex mb-3">


            <!-- Content Start -->
            <div class="content ms-3 pb-3">
                <!-- Header Start -->
                <div class="header d-flex align-items-center">
                    <!-- Name Start -->
                    <div class="name text-cello fs-18 fw-bold">{{isset($info->name) ? $info->name  : ''}}
                        {{isset($info->sur_name) ? $info->sur_name : ''}}</div>
                    <!-- Name End -->

                    <!-- Date Start -->
                    <div class="date ms-auto text-cornflower fs-12 " style="text-align: right;min-width: 400px;">{{\Carbon\Carbon::parse($review->created_at)->format('d-m-Y')}}</div>
                    <!-- Date End -->
                </div>
                <!-- Header End -->

                <!-- Text Start -->
                <div class="mt-2 text-gullgray " >{{$review->description}}</div>
                <!-- Text End -->

            </div>
            <!-- Content End -->
        </li>
        <!-- Review End -->
        @endforeach
    </ul>

</div>
@endif
