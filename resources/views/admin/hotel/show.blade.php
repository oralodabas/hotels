@extends('admin.layouts.master')

@section('HeadScripts')

@endsection
@section('Navbar')
@endsection
@section('Content')
    <div class="layout-px-spacing">
        <div class="row layout-top-spacing">
            <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
                <div class="statbox widget box box-shadow">
                    <div class="widget-header">
                        <div class="row">
                            <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                                <h4>
                                    {{__('Hotel Update')}}
                                </h4>
                            </div>
                        </div>
                    </div>
                    <hr/>
                {!! Form::open(['url'=>route('hotels.update',$dataset['hotel']->id),'enctype'=>"multipart/form-data",'method' => 'put','id'=>'validate']) !!}

                @include('admin.partials.hotel.tab_heads')

                <!-- Tabs Content-->
                    <div class="tab-content" id="simpletabContent">
                        <!-- general content -->
                        <div class="tab-pane fade show active" id="general" role="tabpanel"
                             aria-labelledby="main-tab">
                            @include('admin.partials.hotel.tab_general_content')

                        </div>

                        <!-- hotel content -->
                        @foreach(config('app.available_locales') as $i=>$locale)
                            <div class="tab-pane fade " id="{{$locale}}" role="tabpanel"
                                 aria-labelledby="{{$locale}}-tab">
                                @include('admin.partials.hotel.tab_locale_content')
                            </div>
                        @endforeach

                        <div class="tab-pane fade " id="images" role="tabpanel"
                             aria-labelledby="images-tab">
                            @include('admin.partials.hotel.tab_images_content')
                        </div>

                        <div class="tab-pane fade" id="transfer" role="tabpanel"
                             aria-labelledby="transfer-tab">
                            @include('admin.partials.hotel.tab_transfer')
                        </div>

                        <div class="tab-pane fade " id="faq" role="tabpanel"
                             aria-labelledby="faq-tab">
                            @include('admin.partials.hotel.tab_faq')
                        </div>

                        <div class="tab-pane fade " id="rooms" role="tabpanel"
                             aria-labelledby="rooms-tab">
                            @include('admin.partials.hotel.tab_rooms_content')
                        </div>
                    </div>

                    <!-- End Tab Content -->
                    <div class="col-12 col-md-12 col-lg-12 text-right">
                        <button type="submit" class="btn btn-primary">{{__('Submit')}}</button>
                    </div>
                    {!!Form::close()!!}
                </div>
            </div>

        </div>

    </div>

@endsection

@section('FootScripts')
    <script>
        let facility_url = '{{ route("facility.set.ajax") }}';
        let rooms_url = '{{ route("rooms.set.ajax") }}';
        let state_url = '{{route('post.state.index')}}';
        let city_url = '{{route('post.city.index')}}';
        let delete_room = '{{route('rooms.delete.ajax',['id'=>':id'])}}';
        let token = '{{ csrf_token() }}';


    </script>

    <script src="{{url('/')}}/assets/admin/js/hotel.create.js"></script>

    @if($dataset['selectedCountryId'] >0)
        <script>

            let country_id = $('#country_id').val();
            let url = state_url;
            let formData = {
                '_token': '{{ csrf_token() }}',
                'country_id': country_id
            }
            const promise = ajaxRequest(url, formData,'POST');

            promise.then(function (data) {
                $('#state-dropdown').html('<option value="">Select State</option>');

                $.each(data.states, function (key, value) {
                    let selected = '';
                    if (value.id == {{Request()->old(('state_id')) ?? $dataset['selectedStateId']}}) {
                        selected = 'selected';
                    }
                    $("#state-dropdown").append('<option value="' + value.id + '" ' + selected + ' >' + value.name + '</option>');
                });
            });

        </script>
    @endif

    @if($dataset['selectedStateId'] >0)

        <script>
            let state_id = {{$dataset['selectedStateId']}};
            let url_let = city_url;
            let formDatas = {
                '_token': '{{ csrf_token() }}',
                'state_id': state_id
            }

            const promises = ajaxRequest(url_let, formDatas,'POST');

            promises.then(function (data) {

                $('#city-dropdown').html('<option value="">Select City</option>');
                $.each(data.cities, function (key, value) {
                    let selected = '';
                    if (value.id == {{Request()->old('city_id') ?? $dataset['hotel']->city_id}}) {
                        selected = 'selected';
                    }
                    $("#city-dropdown").append('<option value="' + value.id + '" ' + selected + '>' + value.name + '</option>');
                });
            });

        </script>
    @endif

@endsection

