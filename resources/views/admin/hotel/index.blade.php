@extends('admin.layouts.master')

@section('HeadScripts')
    <link rel="stylesheet" type="text/css" href="{{url('/')}}assets/admin/plugins/table/datatable/datatables.css">
    <link rel="stylesheet" type="text/css" href="{{url('/')}}assets/admin/plugins/table/datatable/dt-global_style.css">
    <link rel="stylesheet" type="text/css" href="{{url('/')}}assets/admin/plugins/select2/select2.min.css">
@endsection
@section('Navbar')
@endsection
@section('Content')
    <div class="layout-px-spacing">
        <div class="row layout-top-spacing">
            <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">

                <button class="btn btn-primary collapsed mb-2" data-toggle="collapse" data-target="#filter"
                        aria-expanded="true" aria-controls="filter">
                    <i class="fa fa-filter mr-1"></i>Filtrele
                </button>
                <a class="btn btn-primary mb-2" href="{{route('hotels.create')}}">
                    <i class="fa fa-plus mr-1"></i>{{__('Ekle')}} </a>
                <div id="toggleAccordion">
                    <div id="filter" class="collapse mt-2" aria-labelledby="headingOne1" data-parent="#toggleAccordion">
                        <div class="widget-content widget-content-area br-6 mb-2">
                            {!! Form::open(['method'=>'get']) !!}
                            <div class="row">
                                <div class="col-xl-6 col-md-6 col-sm-6 col-6">
                                    <div class="form-group mb-4">
                                        <label for="formGroupExampleInput2">Durum</label>
                                        {!! Form::select('status',[''=>'Hepsini Listele','1'=>'Aktif','0'=>'Pasif'], \Illuminate\Support\Facades\Request::get('status'), ['class'=>' form-control basic']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-12 col-lg-12 text-right">
                                <button type="submit" class="btn btn-primary">Filtrele</button>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>


                <div class="widget-content widget-content-area br-6">
                    <div class="table-responsive mb-4 mt-4">
                        <table id="product" class="table table-hover" style="width:100%">
                            <thead>
                            <tr>
                                <th>{{__('Name')}}</th>
                                <th>{{__('City')}}</th>
                                <th>{{__('Status')}}</th>
                                <th>{{__('Process')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($dataset['hotels'] as $data)
                                <tr>
                                    <td>{{$data->getTranslation('name',app()->getLocale())}}</td>
                                    <td>{{$data->city()->first()['name']}}</td>
                                    <td>

                                            <button id="hotel_{{$data->id}}" data-id="{{$data->id}}" class="btn hotel-status btn-@if($data->status == 'passive'){{'danger'}}@else{{'success'}}@endif mb-2">
                                                @if($data->status == 'passive'){{__('Passive')}}@else{{__('Active')}}@endif
                                            </button>

                                    </td>
                                    <td>
                                        <a title="{{__('Edit')}}" href="{{route('hotels.show',['hotel'=>$data->id])}}"><i
                                                class="fas fa-edit"></i></a>
                                        <a title="{{__('Delete')}}" class="delete" onclick="return confirm('{{__('Are you sure you want to delete')}}')" href="#" id="{{$data->id}}"><i
                                                class="fas fa-trash"></i></a>
                                    </td>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{ $dataset['hotels']->links() }}
                    </div>
                </div>
            </div>

        </div>

    </div



@endsection

@section('FootScripts')

    <script>
        let update_hotel_status = '{{route('update.status.hotel',['id'=>':id'])}}';
        let token = '{{ csrf_token() }}';
    </script>

    <script src="{{url('/')}}/assets/admin/js/hotel.create.js"></script>
@endsection

