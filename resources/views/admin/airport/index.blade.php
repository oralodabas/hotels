@extends('admin.layouts.master')

@section('HeadScripts')
    <link rel="stylesheet" type="text/css" href="{{url('/')}}assets/admin/plugins/table/datatable/datatables.css">
    <link rel="stylesheet" type="text/css" href="{{url('/')}}assets/admin/plugins/table/datatable/dt-global_style.css">
    <link rel="stylesheet" type="text/css" href="{{url('/')}}assets/admin/plugins/select2/select2.min.css">
@endsection
@section('Navbar')
@endsection
@section('Content')
    <div class="layout-px-spacing">
        <div class="row layout-top-spacing">
            <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">

                <button class="btn btn-primary collapsed mb-2" data-toggle="collapse" data-target="#filter"
                        aria-expanded="true" aria-controls="filter">
                    <i class="fa fa-filter mr-1"></i>Filtrele
                </button>
                <a class="btn btn-primary mb-2" href="{{route('airports.create')}}">
                    <i class="fa fa-plus mr-1"></i>{{__('Ekle')}} </a>
                <div id="toggleAccordion">
                    <div id="filter" class="collapse mt-2" aria-labelledby="headingOne1" data-parent="#toggleAccordion">
                        <div class="widget-content widget-content-area br-6 mb-2">
                            {!! Form::open(['method'=>'get']) !!}
                            <div class="row">
                                <div class="col-xl-6 col-md-6 col-sm-6 col-6">
                                    <div class="form-group mb-4">
                                        <label for="formGroupExampleInput2">Durum</label>
                                        {!! Form::select('status',[''=>'Hepsini Listele','1'=>'Aktif','0'=>'Pasif'], \Illuminate\Support\Facades\Request::get('status'), ['class'=>' form-control basic']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-12 col-lg-12 text-right">
                                <button type="submit" class="btn btn-primary">Filtrele</button>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>


                <div class="widget-content widget-content-area br-6">
                    <div class="table-responsive mb-4 mt-4">
                        <table id="product" class="table table-hover" style="width:100%">
                            <thead>
                            <tr>
                                <th>{{__('Name')}}</th>
                                <th>{{__('İşlemler')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($dataset['airports'] as $data)
                                <tr>
                                    <td>{{$data->getTranslation('name',app()->getLocale())}}</td>

                                    <td>
                                        <a title="{{__('Edit')}}"
                                           href="{{route('airports.show',['airport'=>$data->id])}}"><i
                                                class="fas fa-edit"></i></a>
                                    </td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{ $dataset['airports']->links() }}
                    </div>
                </div>
            </div>

        </div>

    </div>

@endsection
@section('FootScripts')
    <script>

    </script>

@endsection
