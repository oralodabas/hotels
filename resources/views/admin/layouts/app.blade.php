<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <link rel="icon" type="image/x-icon" href="{{url('/')}}/assets/admin/img/favicon.ico"/>
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:400,600,700" rel="stylesheet">
    <link href="{{url('/')}}/assets/admin//bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="{{url('/')}}/assets/admin/assets/css/plugins.css" rel="stylesheet" type="text/css" />
    <link href="{{url('/')}}/assets/css/authentication/form-2.css" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/assets/admin/css/forms/theme-checkbox-radio.css">
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/assets/admin/css/forms/switches.css">
</head>
<body>
<div id="app">

    <main class="py-4">
        @yield('content')
    </main>
</div>
<script src="{{url('/')}}/assets/admin/js/libs/jquery-3.1.1.min.js"></script>
<script src="{{url('/')}}/assets/admin/bootstrap/js/popper.min.js"></script>
<script src="{{url('/')}}/assets/admin/bootstrap/js/bootstrap.min.js"></script>

<!-- END GLOBAL MANDATORY SCRIPTS -->
<script src="{{url('/')}}/assets/admin/js/authentication/form-2.js"></script>
</body>
</html>
