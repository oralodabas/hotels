<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}" >
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
    <title>{{__('Admin Panel')}}</title>
    <link rel="icon" type="image/x-icon" href="{{url('/')}}/assets/admin/img/favicon.ico"/>
    <link href="{{url('/')}}/assets/admin/css/loader.css" rel="stylesheet" type="text/css"/>
    <script src="{{url('/')}}/assets/admin/js/loader.js"></script>
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:400,600,700" rel="stylesheet">
    <link href="{{url('/')}}/assets/admin/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="{{url('/')}}/assets/admin/css/plugins.css" rel="stylesheet" type="text/css"/>
    <link href="{{ url('/')}}/assets/admin/css/new/app.css" rel="stylesheet">
    <link href="{{ url('/')}}/assets/admin/css/new/vendor.css" rel="stylesheet">
    <link rel="stylesheet" href="{{url('/')}}/assets/admin/fontawesome/css/all.min.css">
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/assets/plugins/select2/select2.min.css">
    <!-- END GLOBAL MANDATORY STYLES -->

    <!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM STYLES -->
    <link href="{{url('/')}}/assets/plugins/apex/apexcharts.css" rel="stylesheet" type="text/css">
    <link href="{{url('/')}}/assets/admin/css/dashboard/dash_1.css" rel="stylesheet" type="text/css"/>
    <!-- END PAGE LEVEL PLUGINS/CUSTOM STYLES -->
    @yield('HeadScripts')
</head>
{{--<body class="alt-menu sidebar-noneoverflow">--}}
<body>
<!-- BEGIN LOADER -->
<div id="load_screen">
    <div class="loader">
        <div class="loader-content">
            <div class="spinner-grow align-self-center"></div>
        </div>
    </div>
</div>
<!--  END LOADER -->


<!--  BEGIN NAVBAR  -->
<div class="header-container fixed-top">
    @include('admin.include.message')
    <header class="header navbar navbar-expand-sm">

        <ul class="navbar-item theme-brand flex-row  text-center">
            <li class="nav-item theme-logo">
                <a href="{{route('dashboard.index')}}">
                    <img src="{{url('/')}}/assets/admin/img/90x90.jpg" class="navbar-logo" alt="logo">
                </a>
            </li>
            <li class="nav-item theme-text">
                <a href="{{route('dashboard.index')}}" class="nav-link"> Admin Panel </a>
            </li>
        </ul>


        @include('admin.partials.header.header-nav')
    </header>
</div>
<!--  END NAVBAR  -->

<!--  BEGIN NAVBAR  -->
<div class="sub-header-container">
    <header class="header navbar navbar-expand-sm">
        <a href="javascript:void(0);" class="sidebarCollapse" data-placement="bottom">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                 stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                 class="feather feather-menu">
                <line x1="3" y1="12" x2="21" y2="12"></line>
                <line x1="3" y1="6" x2="21" y2="6"></line>
                <line x1="3" y1="18" x2="21" y2="18"></line>
            </svg>
        </a>
        <ul class="navbar-nav flex-row">
            <li>
                <div class="page-header">
                    @include('admin.include.breadcrumbs')

                    @yield('Navbar')
                </div>
            </li>
        </ul>
    </header>


</div>

<!--  END NAVBAR  -->
<!--  BEGIN MAIN CONTAINER  -->
<div class="main-container" id="container">

    <div class="overlay"></div>
    <div class="search-overlay"></div>

    <!--  BEGIN SIDEBAR  -->
@include('admin.partials.left.side-bar')
<!--  END SIDEBAR  -->

    <!--  BEGIN CONTENT AREA  -->
    <div id="content" class="main-content">
    @yield('Content')
    <!--  END CONTENT AREA  -->
        <div class="footer-wrapper">


        </div>
    </div>


</div>
<!-- END MAIN CONTAINER -->

<!-- BEGIN GLOBAL MANDATORY SCRIPTS -->
<script src="{{url('/')}}/assets/admin/js/libs/jquery-3.1.1.min.js"></script>
<script src="{{url('/')}}/assets/admin/bootstrap/js/popper.min.js"></script>
<script src="{{url('/')}}/assets/admin/bootstrap/js/bootstrap.min.js"></script>
<script src="{{url('/')}}/assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
<script src="{{url('/')}}/assets/admin/js/app.js"></script>
<script src="{{url('/')}}/assets/plugins/select2/select2.min.js"></script>
<script src="{{url('/')}}/assets/admin/js/jquery.validate.min.js"></script>

<script>
    $(document).ready(function () {
        App.init();
    });
</script>
<script src="{{url('/')}}/assets/admin/js/custom.js"></script>
<!-- END GLOBAL MANDATORY SCRIPTS -->

<!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM SCRIPTS -->
<script src="{{url('/')}}/assets/plugins/apex/apexcharts.min.js"></script>
<script src="{{url('/')}}/assets/admin/js/dashboard/dash_1.js"></script>

<!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM SCRIPTS -->

@yield('FootScripts')
@yield('ajaxFootScript')
</body>
</html>
