<div class="files-wraps  view-grid">
    @if(isset($dataset['images']) && $dataset['images']->count()>0)

        @foreach($dataset['images'] as $image)
            <div class="file-item image/svg is-image" id="imageBox_{{$image->id}}">
                <div title="logo-white" class="inner">
                    <button type="button" id="{{$image->id}}" onclick="deleteImage(this)"
                            rel="{{url('storage/'.sprintf('%s%s',$image->path,$image->name))}}"
                            class="btn btn-success btn-block mb-4 mr-2 selectedImage">Kaldır
                    </button>

                    <div class="file-thumb"><img
                            src="{{url('storage/'.sprintf('%s%s',$image->path,$image->name))}}">
                        <input name="image_title" style="width:119px" value="{{$image->title}}" id="{{$image->id}}"
                               onkeyup="updateTitle(this)">

                        <label>Kapak</label><input name="hotel_image_flag[{{$image->id}}]" value="{{$image->pivot->flag}}"
                                                   @if($image->pivot->flag == 1 ){{'checked'}}@endif type="checkbox"  >

                    </div>

                </div>
            </div>
        @endforeach
            <div id="hiddenImageUploadBox">
                @foreach($dataset['images'] as $image)
                     <input type="hidden" name="selectedImages[]" value="{{$image->id}}">
                @endforeach
        </div>
    @else
        <div id="hiddenImageUploadBox"></div>
    @endif
</div>

@section('ajaxFootScript')
    @parent
    <script>
        let image_url = '{{route('images.get.ajax')}}';
        let image_set_url = '{{route('images.post.ajax')}}';

        $('#files').change(function (e) {
            e.preventDefault();

            let files = e.target.files;

            if (files[0].name.length > 0) {

                let fd = new FormData();
                $.each(files, function (key, image) {
                    console.log(image);
                    fd.append('files[]', image, image.name);
                });


                $.ajax({
                    url: image_set_url,
                    type: "POST",
                    data: fd,
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (data) {

                        $.each(data, function (key, val) {
                            url = '{{url('storage')}}/' + val.path + val.name;

                            $('.view-grid').prepend('<div class="file-item image/svg is-image" id="imageBox_' + val.id + '">\n' +
                                '<div title="logo-white" class="inner">' +
                                ' <button type="button" id="' + val.id + '" onclick="deleteImage(this)"' +
                                ' rel="' + url + '"' +
                                ' class="btn btn-success btn-block mb-4 mr-2 selectedImage">Kaldır' +
                                '  </button>' +
                                ' <div class="file-thumb"><img src="' + url + '" >' +
                                ' </div>' +
                                '  <input name="image_title" style="width:119px" value="' + val.title + '" id="' + val.id + '" onkeyup="updateTitle(this)">' +
                                '  <label>Kapak</label><input name="hotel_image_flag['+val.id+']" value="1" type="checkbox"  id="' + val.id + '">' +
                                '</div>' +
                                '</div>');
                            $('#hiddenImageUploadBox').append('<input type="hidden" name="selectedImages[]" value="' + val.id + '">')

                        });
                    },
                    error: function (data) {
                        let errors = data.responseJSON;
                        console.log(errors);
                    }
                });
            }
        });

        $(document).on('click', '.pagination a', function (event) {
            event.preventDefault();
            getImagesData(this);
        });

        $(document).on('click', '.ajax_search', function (event) {
            event.preventDefault();
            getImagesData(this);
        });


        function getImagesData(e) {

            let page_id = $(e).attr('href') != 'undefined' ?? $(e).attr('href').split('page=')[1];
            let title = $('#ax_search_text').val() ?? '';
            let formData = {
                '_token': token,
                'page': page_id,
                'title': title
            }

            $.ajax({
                url: image_url,
                type: "GET",
                data: formData,
                success: function (data) {
                    $('#fileList').html(data);
                },
                error: function (data) {
                    let errors = data.responseJSON;
                    console.log(errors);
                }
            });
        }

        function insertImages(data) {
            let image_id = $(data).attr('id');
            let image_data = $(data).attr('rel');
            let image_box = $('#imageBox_' + image_id).val();
            $('.attach-demo').append('<div class="image-item" id="image-item-' + image_id + '">\n' +
                '<div class="inner"><span class="delete btn btn-sm btn-danger">' +
                '<i class="fa fa-trash" onclick="deleteImage(' + image_id + ')"></i></span>\n' +
                '<img src="' + image_data + '"></div>\n' +
                '</div>');

            $('.files-wraps').append('<input type="hidden" name="selectedImages[]" value="' + image_id + '">')

            $('#imageBox_' + image_id).fadeOut();
        }

        function deleteImage(data) {
            let imageId = $(data).attr('id');
            $('#imageBox_' + imageId).remove();
            // $('#image-item-' + imageId).fadeOut();
            $('input[name="selectedImages[]"]').map(function () {
                if (this.value == imageId) {
                    $(this).remove();
                }
                ;
            }).get();
        }


        function updateTitle(e) {
            let updateTitleUrl = "{{route('images.title.post.ajax')}}";
            let id = $(e).attr('id');
            let title = $(e).val() ?? '';
            let formData = {
                '_token': token,
                'id': id,
                'title': title
            }

            $.ajax({
                url: updateTitleUrl,
                type: "POST",
                data: formData,
                success: function (data) {
                    console.log(data);
                },
                error: function (data) {
                    let errors = data.responseJSON;
                    console.log(errors);
                }
            });
        }


    </script>
@endsection
