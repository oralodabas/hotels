@extends('admin.layouts.master')

@section('HeadScripts')
    <link href="{{url('/')}}/assets/plugins/file-upload/file-upload-with-preview.min.css" rel="stylesheet" type="text/css">

@endsection
@section('Navbar')
@endsection
@section('Content')
    <div class="layout-px-spacing">
        <div class="row layout-top-spacing">
            <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
                <div class="statbox widget box box-shadow">
                    <div class="widget-header">
                        <div class="row">
                            <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                                <h4>
                                    {{__('Image Insert')}}
                                </h4>
                            </div>
                        </div>
                    </div>
                    <hr/>
                {!! Form::open(['url'=>route('images.store'),'enctype'=>"multipart/form-data",'method' => 'post']) !!}
@csrf
                    <div class="custom-file-container" data-upload-id="mySecondImage">
                        <div class="form-group">
                            <label for="title">{{__('Title')}} </label>
                            <input required id="image_title" type="text" name="image_title" placeholder=""
                                   class="form-control" value="{{ Request()->old('title')}}">
                        </div>

                        <div class="form-group">
                            <label for="image">{{__('Image')}} </label>
                            <input required type="file" name="files[]" multiple class="form-control" accept="image/*">

                        </div>




                    </div>

                    <!-- End Tab Content -->
                    <div class="col-12 col-md-12 col-lg-12 text-right">
                        <button type="submit" class="btn btn-primary">{{__('Submit')}}</button>
                    </div>
                    {!!Form::close()!!}
                </div>
            </div>

        </div>

    </div>

@endsection
@section('FootScripts')
    <script src="{{url('/')}}/assets/plugins/file-upload/file-upload-with-preview.min.js"></script>

    <script>
        var secondUpload = new FileUploadWithPreview('mySecondImage')
    </script>
@endsection

