@extends('admin.layouts.master')

@section('HeadScripts')
    <link rel="stylesheet" type="text/css" href="{{url('/')}}assets/admin/plugins/table/datatable/datatables.css">
    <link rel="stylesheet" type="text/css" href="{{url('/')}}assets/admin/plugins/table/datatable/dt-global_style.css">
    <link rel="stylesheet" type="text/css" href="{{url('/')}}assets/admin/plugins/select2/select2.min.css">
@endsection
@section('Navbar')
@endsection
@section('Content')
    <div class="layout-px-spacing">
        <div class="row layout-top-spacing">
            <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">

                <button class="btn btn-primary collapsed mb-2" data-toggle="collapse" data-target="#filter"
                        aria-expanded="true" aria-controls="filter">
                    <i class="fa fa-filter mr-1"></i>Filtrele
                </button>
                <a class="btn btn-primary mb-2" href="{{route('images.create')}}">
                    <i class="fa fa-plus mr-1"></i>{{__('Ekle')}} </a>
                <div id="toggleAccordion">
                    @include('admin.partials.filter.image')
                    </div>


                <div class="widget-content widget-content-area br-6">
                    <div class="table-responsive mb-4 mt-4">
                        <table id="product" class="table table-hover" style="width:100%">
                            <thead>
                            <tr>
                                <th>{{__('Title')}}</th>
                                <th>{{__('Process')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($dataset['images'] as $data)
                                <tr>
                                    <td>{{$data->title}}</td>
                                    <td>

                                        <a title="{{__('Delete')}}" class="delete" onclick="return confirm('{{__('Are you sure you want to delete')}}')" href="#" id="{{$data->id}}"><i
                                                class="fas fa-trash"></i></a>
                                    </td>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{ $dataset['images']->links() }}
                    </div>
                </div>
            </div>

        </div>

    </div



@endsection
