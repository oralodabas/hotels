@php
    $link ='';
@endphp
<nav class="breadcrumb-one" aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{url('/')}}">Anasayfa</a></li>
        @for($i = 1; $i <= count(Request::segments()); $i++)
            @php
                $link .= "/" . Request::segment($i)
            @endphp
            <li class="breadcrumb-item"><a href="{{$link}}">{{Request::segment($i)}}</a></li>

        @endfor

    </ol>
</nav>

