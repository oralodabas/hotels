<div id="modal_images" class="modal fade"
     tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div id="cdn-browser" class="cdn-browser d-flex flex-column">
                <div class="files-nav flex-shrink-0">
                    <div class="d-flex justify-content-between">
                        <div class="col-left d-flex align-items-center">
                            <!--
                            <div class="filter-item"><input type="text" placeholder="Search file name...." id="ax_search_text"
                                                            class="form-control"></div>
                            <div class="filter-item">
                                <button class="btn btn-default ajax_search"><i class="fa fa-search"></i> ara</button>
                            </div>
                            <div class="filter-item"></div>
                            -->
                        </div>
                        <div class="col-right"><i class="fa-spin fa fa-spinner icon-loading active"
                                                  style="display: none;"></i>

                            <button class="btn btn-success btn-pick-files" ><span><i
                                        class="fa fa-upload"></i> Upload</span>   <input type="file" name="files[]" id="files" multiple></button>
                        </div>
                    </div>
                </div>
                <div display="none" class="upload-new" style="display: none;"><input type="file" name="filepond[]"
                                                                                     class="my-pond"></div>

                 <div class="files-list" id="fileList">

                       @include('admin.image.get_ajax')

                </div>




            </div>

        </div>
    </div>

</div>


