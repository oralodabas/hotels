@foreach(config('app.available_locales') as $i=>$locale)
    <div class="tab-pane fade" id="{{$locale}}" role="tabpanel"
         aria-labelledby="{{$locale}}-tab">

        <div class="widget-content widget-content-area">
            <div class="form-group">
                <label for="name_{{ $locale }}">{{__('Name')}} </label>

                <input id="name_{{ $locale }}" type="text" name="name.{{$locale}}" placeholder=""
                       class="form-control" value="{{  old('name_'.$locale) ?? isset($dataset['airports']) ?  $dataset['airports']->getTranslation('name',$locale,false) : null}}">
            </div>

        </div>
    </div>
@endforeach
@section('FootScripts')
<script>
    let state_url = '{{route('post.state.index')}}';
    let city_url = '{{route('post.city.index')}}';
    let token = '{{ csrf_token() }}';
    $("#country_id").change(function (e) {
        let country_id = $(this).val();
        let formData = {
            '_token': token,
            'country_id': country_id
        }
        let promise = ajaxRequest(state_url, formData, 'POST');

        promise.then(function (data) {
            $('#state-dropdown').html('<option value="">Select State</option>');
            $.each(data.states, function (key, value) {
                $("#state-dropdown").append('<option value="' + value.id + '">' + value.name + '</option>');
            });
        });

    });


    $("#state-dropdown").change(function (e) {
        let state_id = $(this).val();
        let formData = {
            '_token': token,
            'state_id': state_id
        }

        let promise = ajaxRequest(city_url, formData, 'POST');

        promise.then(function (data) {
            $('#city-dropdown').html('<option value="">Select City</option>');
            $.each(data.cities, function (key, value) {
                $("#city-dropdown").append('<option value="' + value.id + '">' + value.name + '</option>');
            });
        });
    });

    function ajaxRequest(url, formData, type) {
        return Promise.resolve($.ajax({
            type: type,
            url: url,
            data: formData,
            dataType: 'json',
            success: function (result) {
                return result;
            },
            error: function (error) {
                // alert('error; ' + eval(error));
            },
        }));
    }
</script>
    @if($dataset['selectedCountryId'] >0)

        <script>

            let country_id = $('#country_id').val();
            let url = state_url;
            let formData = {
                '_token': '{{ csrf_token() }}',
                'country_id': country_id
            }
            const promise = ajaxRequest(url, formData,'POST');

            promise.then(function (data) {
                $('#state-dropdown').html('<option value="">Select State</option>');

                $.each(data.states, function (key, value) {
                    let selected = '';
                    if (value.id == {{Request()->old(('state_id')) ?? $dataset['selectedStateId']}}) {
                        selected = 'selected';
                    }
                    $("#state-dropdown").append('<option value="' + value.id + '" ' + selected + ' >' + value.name + '</option>');
                });
            });

        </script>
    @endif

    @if($dataset['selectedStateId'] >0)

        <script>
            let state_id = {{$dataset['selectedStateId']}};
            let url_let = city_url;
            let formDatas = {
                '_token': '{{ csrf_token() }}',
                'state_id': state_id
            }

            const promises = ajaxRequest(url_let, formDatas,'POST');

            promises.then(function (data) {

                $('#city-dropdown').html('<option value="">Select City</option>');
                $.each(data.cities, function (key, value) {
                    let selected = '';
                    if (value.id == {{Request()->old('city_id') ?? $dataset['airports']->city_id}}) {
                        selected = 'selected';
                    }
                    $("#city-dropdown").append('<option value="' + value.id + '" ' + selected + '>' + value.name + '</option>');
                });
            });

        </script>
    @endif
@endsection
