<div class="form-group">
    <label for="countries">{{__('Country')}} </label>
    <select name="country_id" id="country_id" class="form-control">
        <option value=""> {{__('Please Select To Country')}}</option>
        @foreach($dataset['countries'] as $country )
            <option value="{{$country->id}}"
            @if(old('country_id') ?? $dataset['selectedCountryId'] == $country->id){{'selected'}}@endif>
                {{$country->name}}</option>
        @endforeach
    </select>
</div>

<div class="form-group">
    <label for="state">{{__('State')}}</label>
    <select class="form-control" id="state-dropdown" name="state_id">
    </select>
</div>
<div class="form-group">
    <label for="city">{{__('City')}}</label>
    <select class="form-control" id="city-dropdown" name="city_id">
    </select>
</div>

<div class="form-group">
    <label for="icon_name">{{__('Code Name')}} </label>

        <input  id="code" type="text" name="code" placeholder=""
                class="form-control" value="{{ old('code') ?? isset($dataset['airports']) ?  $dataset['airports']->code : null}}">
</div>


