<!-- Tab Content -->
<ul class="nav nav-tabs  mb-3 mt-3" id="simpletab" role="tablist">
    <li class="nav-item">
        <a class="nav-link active" id="main-tab" data-toggle="tab"
           href="#main"
           role="tabpanel" aria-controls="main"
           aria-selected="true">Main</a>
    </li>
    @foreach(config('app.available_locales') as $i=>$locale)
        <li class="nav-item">
            <a class="nav-link" id="{{$locale}}-tab" data-toggle="tab"
               href="#{{$locale}}"
               role="tabpanel" aria-controls="{{$locale}}"
               aria-selected="false">{{strtoupper($locale)}}</a>
        </li>
    @endforeach

</ul>
