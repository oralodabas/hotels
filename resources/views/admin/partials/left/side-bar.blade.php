<div class="sidebar-wrapper sidebar-theme">

    <nav id="sidebar">
        <div class="shadow-bottom"></div>
        <ul class="list-unstyled menu-categories ps ps--active-y" id="accordionExample">
            <li class="menu">
                <a href="#dashboard" data-toggle="collapse" aria-expanded=@if(count(Request()->segments()) == 1 )"true"@else"false"@endif
                data-active=@if(count(Request()->segments()) == 1 )"true"@else"false"@endif class="dropdown-toggle">
                    <div class="">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                             stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                             class="feather feather-home">
                            <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                            <polyline points="9 22 9 12 15 12 15 22"></polyline>
                        </svg>
                        <span>{{__('Dashboard')}}</span>
                    </div>
                    <div>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                             stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                             class="feather feather-chevron-right">
                            <polyline points="9 18 15 12 9 6"></polyline>
                        </svg>
                    </div>
                </a>
                <ul class="submenu list-unstyled collapse show" id="dashboard" data-parent="#accordionExample" style="">
                    <li class=@if(count(Request()->segments()) == 1 )"active"@else""@endif >
                        <a href="{{route('dashboard.index')}}"> index </a>
                    </li>
                </ul>
            </li>

            <li class="menu">
                <a href="#app" data-toggle="collapse" data-active=@if(count(Request()->segments(1)) > 1 && Request()->segment(2) == 'hotels')"true"@else"false"@endif aria-expanded=@if(count(Request()->segments(1)) > 1 && Request()->segment(2) == 'hotels')"true"@else"false"@endif
                   class="dropdown-toggle collapsed">
                    <div class="">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                             stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                             class="feather feather-cpu">
                            <rect x="4" y="4" width="16" height="16" rx="2" ry="2"></rect>
                            <rect x="9" y="9" width="6" height="6"></rect>
                            <line x1="9" y1="1" x2="9" y2="4"></line>
                            <line x1="15" y1="1" x2="15" y2="4"></line>
                            <line x1="9" y1="20" x2="9" y2="23"></line>
                            <line x1="15" y1="20" x2="15" y2="23"></line>
                            <line x1="20" y1="9" x2="23" y2="9"></line>
                            <line x1="20" y1="14" x2="23" y2="14"></line>
                            <line x1="1" y1="9" x2="4" y2="9"></line>
                            <line x1="1" y1="14" x2="4" y2="14"></line>
                        </svg>
                        <span>Uygulamalar</span>
                    </div>
                    <div>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                             stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                             class="feather feather-chevron-right">
                            <polyline points="9 18 15 12 9 6"></polyline>
                        </svg>
                    </div>
                </a>

                <ul class="submenu list-unstyled collapse @if( count(Request::segments()) >= 2) show @endif" id="app"
                    data-parent="#accordionExample" style="">
                    <li class=@if(count(Request()->segments(1)) > 1 && Request()->segment(2) == 'hotels')"active"@endif>
                        <a href="{{route('hotels.index')}}"> {{_('Hotel List')}}</a>
                    </li>

                    <li class=@if(count(Request()->segments(1)) > 1 && Request()->segment(2) == 'facilities')"active"@endif>
                        <a href="{{route('facilities.index')}}"> {{_('Facility Type List')}}</a>
                    </li>
                    <li class=@if(count(Request()->segments(1)) > 1 && Request()->segment(2) == 'reviews')"active"@endif>
                        <a href="{{route('reviews.index')}}"> {{_('Reviews List')}}</a>
                    </li>

                    <li class=@if(count(Request()->segments(1)) > 1 && Request()->segment(2) == 'airports')"active"@endif>
                        <a href="{{route('airports.index')}}"> {{_('Airport List')}}</a>
                    </li>
                    <li class=@if(count(Request()->segments(1)) > 1 && Request()->segment(2) == 'vehicles')"active"@endif>
                        <a href="{{route('vehicles.index')}}"> {{_('Vehicles List')}}</a>
                    </li>
                    <li class=@if(count(Request()->segments(1)) > 1 && Request()->segment(2) == 'orders')"active"@endif>
                        <a href="{{route('orders.index')}}"> {{_('Orders')}}</a>
                    </li>
                    <li class=@if(count(Request()->segments(1)) > 1 && Request()->segment(2) == 'customers')"active"@endif>
                        <a href="{{route('customers.index')}}"> {{_('Customers')}}</a>
                    </li>
                </ul>

            </li>


            <div class="ps__rail-x" style="left: 0px; bottom: 0px;">
                <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div>
            </div>
            <div class="ps__rail-y" style="top: 0px; height: 469px; right: -4px;">
                <div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 195px;"></div>
            </div>
        </ul>
        <!-- <div class="shadow-bottom"></div> -->

    </nav>

</div>
