<div id="modal_rooms" class="modal fade"
     tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">{{__('Oda Düzenle')}}</h5>

            </div>
            <div class="modal-body">
                <div id="rooms_ajax_hidden_input"></div>
                <div class="form-group">
                    <label for="facilities"
                           class="bold"> {{__('Oda İsmi ')}} </label>
                    <div class="input-group">
                        @foreach(config('app.available_locales') as $i=>$locale)
                            <input type="text" placeholder="{{$locale}} - dili için isim giriniz"
                                   id="rooms_ajax_name_{{$locale}}"
                                   name="rooms_ajax_name_{{$locale}}" class="form-control">

                        @endforeach

                        @foreach(config('app.available_locales') as $i=>$locale)

                            <input type="text" placeholder="{{$locale}} - dili için detay giriniz"
                                   id="rooms_ajax_detail_{{$locale}}"
                                   name="rooms_ajax_detail_{{$locale}}" class="form-control">
                        @endforeach

                    </div>

                </div>


                <div id="cdn-browser-rooms" class="cdn-browser d-flex flex-column">
                    <div class="files-nav flex-shrink-0">
                        <div class="d-flex justify-content-between">
                            <div class="col-right"><i class="fa-spin fa fa-spinner icon-loading active"
                                                      style="display: none;"></i>

                                <button class="btn btn-success btn-pick-files"><span><i
                                            class="fa fa-upload"></i> Upload</span>
                                    <input type="file" name="room_images[]" id="room_images" multiple></button>
                            </div>
                        </div>
                    </div>

                    <div class="files-list"  >
                        <div class="files-wraps  view-grid" id="room-cdn-list"></div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> {{__('Discard')}}</button>
                <button type="button" class="btn btn-primary rooms-save-ajax">{{__('Save')}}</button>
            </div>

        </div>
    </div>

</div>

@section('ajaxFootScript')
    @parent
    <script>
        $('.rooms-save-ajax').click(function (e) {
            e.preventDefault();

            let room_id = $('#room_ajax_id').val();
            let room_image_id = $('input[name="selectedRoomImages[]"]').map(function(){
                return this.value;
            }).get();
            let room_image_flag = $('input[name="hotel_room_image_flag[]"]').map(function(){

                return this.checked == true ? this.value : 0;
            }).get();
            console.log(room_image_flag);

            let rooms_update_url = '{{ route("rooms.update.ajax", ":id") }}';

            rooms_update_url = rooms_update_url.replace(':id', room_id);

            let locale = $("input[name='_locale[]']");
            const formData = {
                '_token': '{{ csrf_token() }}',
                'room_image_id[]' : room_image_id,
                'room_image_flag[]' : room_image_flag
            };


            $.each(locale, function () {
                let locale_name = $(this).val();
                let _name = $('#rooms_ajax_name_' + locale_name).val();
                let _detail = $('#rooms_ajax_detail_' + locale_name).val();
                formData['title_' + locale_name] = _name;
                formData['detail_' + locale_name] = _detail;
            });

            $.ajax({
                url: rooms_update_url,
                type: "PUT",
                data: formData,
                success: function (data) {
                    console.log(data);
                    $('#list_room_name_'+room_id).html($('#rooms_ajax_name_tr').val());
                    alert("form has been successfully");

                },
                error: function (data) {
                    var errors = data.responseJSON;
                    console.log(errors);
                }
            });
        });

        $('#room_images').change(function (e) {
            e.preventDefault();

            let files = e.target.files;
            if (files[0].name.length > 0) {
                let fd = new FormData();

                $.each(files, function (key, image) {
                    fd.append('files[]', image, image.name);
                });

                $.ajax({
                    url: image_set_url,
                    type: "POST",
                    data: fd,
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (data) {
                        $.each(data, function (key, val) {
                            url = '{{url('storage')}}/' + val.path + val.name;

                            $('#room-cdn-list').append(
                                '<div class="file-item image/svg is-image" id="room-image-id-' + val.id + '">\n' +
                                '<div title="logo-white" class="inner">' +
                                ' <button type="button" id="' + val.id + '" onclick="deleteRoomImage(this)"' +
                                ' rel="' + url + '"' +
                                ' class="btn btn-success btn-block mb-4 mr-2 selectedImage">Kaldır' +
                                '  </button>' +
                                ' <div class="file-thumb"><img src="' + url + '" >' +
                                ' </div>' +
                                '  <input name="image_title" style="width:119px" value="' + val.title + '" id="' + val.id + '" onkeyup="updateTitle(this)">' +
                                '  <label>Kapak</label><input name="hotel_room_image_flag[]" value="'+val.id+'" type="checkbox"  id="' + val.id + '">' +
                                '</div>');

                            $('#rooms_ajax_hidden_input').append('<input type="hidden" name="selectedRoomImages[]" id="selectedRoomImages" value="' + val.id + '">')
                        });


                    },
                    error: function (data) {
                        let errors = data.responseJSON;
                        console.log(errors);
                    }
                });
            }
        });

        $('#modal_rooms').on('show.bs.modal', function (e) {
            $('#room-cdn-list').html('');
            $("#rooms_ajax_hidden_input").html('');

            let locale = $("input[name='_locale[]']");
            let roomId = $(e.relatedTarget).data('id');
            let rooms_get_url = '{{ route("rooms.get.ajax", ":id") }}';

            rooms_get_url = rooms_get_url.replace(':id', roomId);

            let formData = {
                '_token': '{{ csrf_token() }}'
            }

            const promise = ajaxRequest(rooms_get_url, formData, 'GET');

            promise.then(function (data) {

                $.each(locale, function () {
                    let locale_name = $(this).val();
                    $('#rooms_ajax_name_' + locale_name).val(data['rooms'].title[locale_name]);
                    $('#rooms_ajax_detail_' + locale_name).val(data['rooms'].detail[locale_name]);
                });


                $.each(data['images'], function (key, val) {
                    url = '{{url('storage')}}/' + val.path + val.name;
                    let checked = "";
                    if(val.pivot.flag == 1) {
                        checked='checked';
                    }
                    $('#room-cdn-list').append(
                        '<div class="file-item image/svg is-image" id="room-image-id-' + val.id + '">\n' +
                        '<div title="logo-white" class="inner">' +
                        ' <button type="button" id="' + val.id + '" onclick="deleteRoomImage(this)"' +
                        ' rel="' + url + '"' +
                        ' class="btn btn-success btn-block mb-4 mr-2 selectedImage">Kaldır' +
                        '  </button>' +
                        ' <div class="file-thumb"><img src="' + url + '" >' +
                        ' </div>' +
                        '  <input name="image_title" style="width:119px" value="' + val.title + '" id="' + val.id + '" onkeyup="updateTitle(this)">' +
                        '  <label>Kapak</label><input name="hotel_room_image_flag[]" value="'+val.id+'" '+checked+' type="checkbox"  id="' + val.id + '">' +
                        '</div></div>');

                    $('#rooms_ajax_hidden_input').append('<input type="hidden" name="selectedRoomImages[]" id="selectedRoomImages" value="' + val.id + '">')
                });
            });

            $("#rooms_ajax_hidden_input").append("<input type='hidden' name='room_ajax_id' id='room_ajax_id' value='" + roomId + "'>")


        });

        function deleteRoomImage(e){
            $('#room-image-id-'+ e.id).fadeOut();
            $('input[name="selectedRoomImages[]"]').map(function(){
                if(this.value == e.id){
                    $(this).remove();
                }  ;
            }).get();
        }

    </script>
@endsection
