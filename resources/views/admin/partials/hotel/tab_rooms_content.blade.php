
<div class="form-group">
    <label for="facilities"
           class="bold"> {{__('Oda İsmi giriniz')}} </label>
    <div class="input-group">
        <div class="input-group-append">
            <button class="btn btn-outline-secondary add-rooms"
                    type="button">
                {{__('Oda Ekle')}}</button>
        </div>
        @foreach(config('app.available_locales') as $i=>$locale)
            <input type="text" placeholder="{{$locale}} - dili için isim giriniz"
                   id="rooms_name_{{$locale}}"
                   name="rooms_name_{{$locale}}" class="form-control">

        @endforeach

            @foreach(config('app.available_locales') as $i=>$locale)

                <input type="text" placeholder="{{$locale}} - dili için detay giriniz"
                       id="rooms_detail_{{$locale}}"
                       name="rooms_detail_{{$locale}}" class="form-control">
            @endforeach

    </div>
</div>

<div class="table-responsive">
    <table class="table table-bordered mb-4">
        <thead>
        <tr>
            <th>Name</th>
            <th></th>
            <th></th>
        </tr>
        </thead>
        <tbody id="room_lists">
        @if($dataset['rooms'])
        @foreach($dataset['rooms'] as $room)
            <tr id="list_rooms_{{$room->id}}"><td id="list_room_name_{{$room->id}}">{{$room->getTranslation('title','tr',false)}}</td>
                <td><input type="button" class="mt-4 btn btn-primary edit-rooms"
                    value="düzenle"
                    data-id="{{$room->id}}"
                    data-toggle="modal"
                    data-target="#modal_rooms"> </td>
                <td><input type="button"
                    class="mt-4 btn btn-primary delete-rooms"
                    value="sil"
                    id="{{$room->id}}">
                    </td>
                </tr>
        @endforeach
        @endif

        </tbody>
    </table>

@include('admin.partials.rooms.room_modal')


