<div class="form-group">
    <label class="control-label">Gallery</label>
    <div class="dungdt-upload-multiple active" data-val="">
        <div class="attach-demo d-flex">

        </div>

        <div class="upload-box">
            <input type="hidden" name="gallery">
            <div class="text-left">
                <button type="button" class="btn btn-info btn-sm btn-field-upload" data-toggle="modal"
                        data-target="#modal_images">
                    <i class="fa fa-plus-circle"></i> {{__('Resim Ekle')}}
                </button>
            </div>
        </div>
    </div>

</div>


@include('admin.partials.images.image_modal')
