<div class="card">
    <div class="card-header" id="headingOne">
        <h5 class="mb-0">
            <a class=" " data-toggle="collapse" href="#facilities" aria-expanded="false" aria-controls="facilities">
                Otel Özellikleri
            </a>
        </h5>
    </div>

    @foreach(config('app.available_locales') as $i=>$locale)
        <input type="hidden" name="_locale[]" id="_locale" value="{{$locale}}" >
    @endforeach
    <div id="facilities" class="collapse" aria-labelledby="headingOne"
         data-parent="#accordion">
        <div class="card-body">
            @foreach($dataset['facilityType'] as $facility)
                <div class="form-group">
                    <label for="facilities" class="bold">{{$facility->name}} </label>
                    <select class="form-control tagging" name="facilities[]" id="facilities_{{$facility->id}}" multiple="multiple">
                        @foreach($facility->facilities()->get() as $i=>$value)
                            <option value="{{$value->id}}"
                            @if( in_array($value->id,$dataset['facilities'])   == $value->id ){{'selected'}}@endif >{{$value->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="facilities"
                           class="bold">{{$facility->name}} {{__('için yeni özellik tanımlayınız')}} </label>
                        <div class="input-group">
                            <div class="input-group-append">
                                <button class="btn btn-outline-secondary add-facilities"
                                        id="{{$facility->id}}"
                                        type="button">
                                    {{__('Ekle')}}</button>
                            </div>
                            @foreach(config('app.available_locales') as $i=>$locale)
                                <input type="text" placeholder="{{$locale}} - dili için isim giriniz"
                                       id="facilities_name_{{$facility->id}}_{{$locale}}"
                                       name="facilities_name_{{$facility->id}}_{{$locale}}" class="form-control">
                            @endforeach
                        </div>
                </div>
            @endforeach
        </div>
    </div>

</div>
