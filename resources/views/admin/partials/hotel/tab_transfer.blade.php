<div class="row">
    <div class="col-lg-12">

        @if($dataset['transfers'])
            @foreach($dataset['transfers'] as $transfer)
                <div id="inputFormRow">
                    <div class="input-group mb-3">
                        <select name="airlines[]" class="form-control">
                            <option value="">{{__('Selected Airports')}}</option>
                            @foreach(json_decode($dataset['airports']) as $airport)
                                <option
                                    value="{{$airport->id}}" @if($transfer->airport_id == $airport->id){{'selected'}} @endif>{{$airport->name->tr}}</option>
                            @endforeach
                        </select>
                        <select name="vehicles[]" class="form-control">
                            <option value="">{{__('Selected vehicles')}}</option>
                            @foreach($dataset['vehicles'] as $vehicles)
                                <option
                                    value="{{$vehicles->id}}"@if($transfer->vehicle_id == $vehicles->id){{'selected'}} @endif >{{$vehicles->name}}</option>
                            @endforeach
                        </select>
                        <input type="text" name="price[]" class="form-control m-input" placeholder="price"
                               value="{{$transfer->price}}" autocomplete="off">
                        <button id="removeRow" type="button" class="btn btn-danger">Remove</button>
                    </div>
                </div>
            @endforeach
        @endif
        <div id="inputFormRow">
            <div class="input-group mb-3">
                <select name="airlines[]" class="form-control">
                    <option value="">{{__('Selected Airports')}}</option>
                    @foreach(json_decode($dataset['airports']) as $airport)
                        <option value="{{$airport->id}}">{{$airport->name->tr}}</option>
                    @endforeach
                </select>
                <select name="vehicles[]" class="form-control">
                    <option value="">{{__('Selected vehicles')}}</option>
                    @foreach($dataset['vehicles'] as $vehicles)
                        <option value="{{$vehicles->id}}">{{$vehicles->name}}</option>
                    @endforeach
                </select>

                <input type="text" name="price[]" class="form-control m-input" placeholder="price" autocomplete="off">
                <button id="removeRow" type="button" class="btn btn-danger">Remove</button>

            </div>
        </div>

        <div id="newRow"></div>
        <button id="addRow" type="button" class="btn btn-info">Add Row</button>
    </div>
</div>
@section('ajaxFootScript')
    @parent
    <script type="text/javascript">
        let airports = '{{$dataset['airports']}}'
        let vehicles = '{{$dataset['vehicles']}}'
        airports = JSON.parse(airports.replace(/&quot;/g, '"'));
        vehicles = JSON.parse(vehicles.replace(/&quot;/g, '"'));

        $("#addRow").click(function () {
            var html = '';
            html += '<div id="inputFormRow">';
            html += '<div class="input-group mb-3">';
            html += ' <select name="airlines[]" class="form-control">';
            $.each(airports, function (key, value) {
                html += '<option value="' + value.id + '">' + value.name.tr + '</option>';
            });
            html += ' </select>';
            html += ' <select name="vehicles[]" class="form-control">';
            $.each(vehicles, function (key, value) {
                html += '<option value="' + value.id + '">' + value.name.tr + '</option>';
            });
            html += ' </select>';
            html += '<input type="text" name="price[]" class="form-control m-input" placeholder="price">';
            html += '<div class="input-group-append">';
            html += '<button id="removeRow" type="button" class="btn btn-danger">Remove</button>';
            html += '</div>';
            html += '</div>';

            $('#newRow').append(html);
        });

        // remove row
        $(document).on('click', '#removeRow', function () {
            $(this).closest('#inputFormRow').remove();
        });
    </script>
@endsection
