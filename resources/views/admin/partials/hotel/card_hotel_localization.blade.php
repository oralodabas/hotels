<div class="card">
    <div class="card-header" id="headingOne">
        <h5 class="mb-0">
            <a class=" " data-toggle="collapse" href="#hotelLocalization"  aria-expanded="false" aria-controls="collapseExample">
                {{__('Otel Bilgileri')}}
            </a>
        </h5>
    </div>
    <div id="hotelLocalization" class="collapse show" aria-labelledby="headingOne"
         data-parent="#accordion">
        <div class="card-body">
            <div class="form-group">
                <label for="name_{{ $locale }}">{{__('Slug')}} </label>
                <input id="slug_{{ $locale }}" type="text" name="slug.{{$locale}}"
                       placeholder=""
                       class="form-control"
                       value="{{  old('slug_'.$locale) ?? isset($dataset['hotel']) ?  \Illuminate\Support\Str::title(str_replace('-', ' ', $dataset['hotel']->getTranslation('slug',$locale,false)))  : null}}">
            </div>
            <div class="form-group">
                <label for="name_{{ $locale }}">{{__('Name')}} </label>
                <input id="name_{{ $locale }}" type="text" name="name.{{$locale}}"
                       placeholder=""
                       class="form-control"
                       value="{{  old('name_'.$locale) ?? isset($dataset['hotel']) ?  $dataset['hotel']->getTranslation('name',$locale,false) : null}}">
            </div>

            <div class="form-group mb-4">
                <label for="title_{{ $locale }}">{{__('Overview')}} </label>
                <textarea class="form-control" id="overview_{{ $locale }}"
                          name="overview.{{ $locale }}"
                          rows="4">{{ old('overview_'.$locale) ?? isset($dataset['hotel']) ?  $dataset['hotel']->getTranslation('overview',$locale,false) : null}}</textarea>
            </div>
        </div>
    </div>
</div>
