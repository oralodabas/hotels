<div class="row">
    <div class="col-lg-12">

        @if(isset($dataset['faq']))
            @foreach($dataset['faq'] as $faq)
                <div id="inputFormRowQuestions">
                    <div class="input-group mb-3">
                        @foreach(config('app.available_locales') as $i=>$locale)
                            <input type="text" placeholder="{{$locale}} - question"
                                   id="faq_question_{{$locale}}"
                                   name="faq_question_{{$locale}}[]" class="form-control"
                                    value="{{$faq->getTranslation('questions',$locale,false)}}"
                            >
                        @endforeach
                        <button id="removeRow" type="button" class="btn btn-danger">{{__('remove')}}</button>

                    </div>
                    <div class="input-group mb-3">
                        @foreach(config('app.available_locales') as $i=>$locale)
                            <textarea cols="3" placeholder="{{$locale}} - answers"  id="faq_answers_{{$locale}}"
                                      name="faq_answers_{{$locale}}[]" class="form-control">{{$faq->getTranslation('answers',$locale,false)}}</textarea>
                        @endforeach
                    </div>

                </div>
            @endforeach
        @endif

        <div id="newRowFaq"></div>
        <button id="addRowQuestions" type="button" class="btn btn-info">Add Row</button>
    </div>
</div>
@section('ajaxFootScript')
@parent
    <script type="text/javascript">
       // let airports = '{{$dataset['airports']}}'
        //let vehicles = '{{$dataset['vehicles']}}'
        //airports = JSON.parse(airports.replace(/&quot;/g, '"'));
        //vehicles = JSON.parse(vehicles.replace(/&quot;/g, '"'));

        $("#addRowQuestions").click(function (e) {
            e.preventDefault();
            let locales = $("input[name='_locale[]']");
            var html = '';

            html += '<div id="inputFormRowQuestions">';
            html += '<div class="input-group mb-3">';
            $.each(locales, function (key, locale) {
                html += '<input type="text" placeholder="'+locale.value+' - question" ' +
                    'name="faq_question_'+locale.value+'[]" class="form-control" />'
            });

            html += '<button id="removeRow" type="button" class="btn btn-danger">Remove</button>';
            html += '</div>';

            html += '<div class="input-group mb-3">';
            $.each(locales, function (key, locale) {
                html += '<textarea cols="3" placeholder="'+locale.value+'- answers" ' +
                    'name="faq_answers_'+locale.value+'[]" class="form-control"></textarea>'
            });
            html += '</div>';
            html += '</div>';

            $('#newRowFaq').append(html);
            e.stopPropagation();
        });

        // remove row
        $(document).on('click', '#removeRow', function () {
            $(this).closest('#inputFormRowQuestions').remove();
        });
    </script>
@endsection
