<!-- Tab Content -->
<ul class="nav nav-tabs  mb-3 mt-3" id="simpletab" role="tablist">
    <li class="nav-item">
        <a class="nav-link active " id="main-tab" data-toggle="tab"
           href="#general"
           role="tabpanel" aria-controls="main"
           aria-selected="true">{{__('Genel')}}</a>
    </li>
    @foreach(config('app.available_locales') as $i=>$locale)
        <li class="nav-item">
            <a class="nav-link" id="{{$locale}}-tab" data-toggle="tab"
               href="#{{$locale}}"
               role="tabpanel" aria-controls="{{$locale}}"
               aria-selected="false">{{strtoupper($locale)}}</a>
        </li>
    @endforeach
    <li class="nav-item">
        <a class="nav-link  " id="image-tab" data-toggle="tab"
           href="#images"
           role="tabpanel" aria-controls="images"
           aria-selected="true">{{__('Images')}}</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="rooms-tab" data-toggle="tab"
           href="#rooms"
           role="tabpanel" aria-controls="rooms"
           aria-selected="true">{{__('Rooms')}}</a>
    </li>
    <li class="nav-item">
        <a class="nav-link " id="transfer-tab" data-toggle="tab"
           href="#transfer"
           role="tabpanel" aria-controls="transfer"
           aria-selected="true">Transfer</a>
    </li>

    <li class="nav-item">
        <a class="nav-link " id="faq-tab" data-toggle="tab"
           href="#faq"
           role="tabpanel" aria-controls="faq"
           aria-selected="true">Faq</a>
    </li>


</ul>
<!-- Tab Content End -->
