<div class="card">
    <div class="card-header" id="headingOne">
        <h5 class="mb-0">
            <a class="#" data-toggle="collapse" href="#hotelGeneral" aria-expanded="false"
               aria-controls="collapseExample">
                Otel Genel Bilgileri
            </a>
        </h5>
    </div>

    <div id="hotelGeneral" class="collapse show" aria-labelledby="headingOne"
         data-parent="#accordion">
        <div class="card-body">
            <div class="widget-content widget-content-area">
                <div class="form-group">
                    <label for="countries">{{__('Country')}} </label>
                    <select name="country_id" id="country_id" class="form-control">
                        <option value=""> {{__('Please Select To Country')}}</option>
                        @foreach($dataset['countries'] as $country )
                            <option value="{{$country->id}}"
                            @if(old('country_id') ?? $dataset['selectedCountryId'] == $country->id){{'selected'}}@endif>
                                {{$country->name}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label for="state">{{__('State')}}</label>
                    <select class="form-control" id="state-dropdown" name="state_id">
                    </select>
                </div>
                <div class="form-group">
                    <label for="city">{{__('City')}}</label>
                    <select class="form-control" id="city-dropdown" name="city_id">
                    </select>
                </div>

                <div class="form-group mb-4">
                    <label for="curl_url">{{__('Hotels.com Url')}} </label>
                    <input id="curl_url" type="text" name="curl_url" placeholder=""
                           class="form-control" value="{{ old('curl_url') ?? isset($dataset['hotel']) ?  $dataset['hotel']->curl_url : null}}">
                </div>

                <div class="form-group mb-4">
                    <label for="google_map_url">{{__('Google Maps Url')}} </label>
                    <input id="google_map_url" type="text" name="google_map_url" placeholder=""
                           class="form-control" value="{{old('google_map_url') ?? isset($dataset['hotel']) ?  $dataset['hotel']->google_map_url : null}}">
                </div>

                <div class="form-group mb-4">
                    <label for="domain_name">{{__('Domain Name')}} </label>
                    <input id="domain_name" type="text" name="domain_name" placeholder=""
                           class="form-control" value="{{old('domain_name') ?? isset($dataset['hotel']) ?  $dataset['hotel']->domain_name : null}}">
                </div>
                <div class="form-group mb-4">
                    <label for="start">{{__('Star')}} </label>
                    <input id="stars" type="text" name="stars" placeholder=""
                           class="form-control" value="{{ old('stars') ?? isset($dataset['hotel']) ?  $dataset['hotel']->stars : null}}">
                </div>

                <div class="form-group mb-4">
                    <label for="start">{{__('Logo')}} </label>
                    <input id="logo" type="file" name="logo"  placeholder=""
                           class="form-control" >
                    @if(isset($dataset['hotel']) && $dataset['hotel']->logo_name != null)
                        <img src="{{asset('storage/uploads/hotels/logo/'.$dataset['hotel']->logo_name)}}" height="100px;">
                        @endif

                </div>

                <div class="form-group mb-4">
                    <label for="domain_name">{{__('Check In  From')}} </label>
                    <input id="domain_name" type="text" name="check_in_from" placeholder=""
                           class="form-control" value="{{old('check_in_from') ?? isset($dataset['hotel']) ?  $dataset['hotel']->check_in_from : null}}">
                </div>

                <div class="form-group mb-4">
                    <label for="domain_name">{{__('Check In  Until')}} </label>
                    <input id="domain_name" type="text" name="check_in_until" placeholder=""
                           class="form-control" value="{{old('check_in_until') ?? isset($dataset['hotel']) ?  $dataset['hotel']->check_in_until : null}}">
                </div>

                <div class="form-group mb-4">
                    <label for="domain_name">{{__('Check Out')}} </label>
                    <input id="domain_name" type="text" name="check_out" placeholder=""
                           class="form-control" value="{{old('check_out') ?? isset($dataset['hotel']) ?  $dataset['hotel']->check_out : null}}">
                </div>
            </div>
        </div>
    </div>
</div>
