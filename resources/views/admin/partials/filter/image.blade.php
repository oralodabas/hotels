<div id="filter" class="collapse mt-2" aria-labelledby="headingOne1" data-parent="#toggleAccordion">
    <div class="widget-content widget-content-area br-6 mb-2">
        {!! Form::open(['method'=>'get']) !!}
        <div class="row">
            <div class="col-xl-6 col-md-6 col-sm-6 col-6">
                <div class="form-group mb-4">
                    <label for="formGroupExampleInput2">Durum</label>
                    {!! Form::select('status',[''=>'Hepsini Listele','1'=>'Aktif','0'=>'Pasif'], \Illuminate\Support\Facades\Request::get('status'), ['class'=>' form-control basic']) !!}
                </div>
            </div>
        </div>
        <div class="col-12 col-md-12 col-lg-12 text-right">
            <button type="submit" class="btn btn-primary">Filtrele</button>
        </div>
        {!! Form::close() !!}
    </div>
</div>
