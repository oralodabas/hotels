
<!-- Modal -->
<div class="modal fade" id="icon_list" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">

            </div>
            <div class="modal-body">
                @foreach(\App\Helpers\getIcons() as $icon)
                   <div><i class='icon-{{$icon}} text-burning fs-20 me-3'></i> icon-{{$icon}}</div>
                @endforeach
            </div>

        </div>
    </div>
</div>

