<div class="form-group">
    <label for="icon_name">{{__('Icon Name')}} </label>
    <div class="input-group">
        <div class="input-group-append">
            <button class="btn btn-outline-secondary show-icons" data-toggle="modal" data-target="#icon_list"  type="button">
                {{__('Icon List')}}</button>
        </div>
        <input  id="icon_name" type="text" name="icon_name" placeholder=""
                class="form-control" value="{{ old('icon_name') ?? isset($dataset['facilities']) ?  $dataset['facilities']->icon_name : null}}">
    </div>
</div>


@include('admin.partials.facilities.modal_icon')
