@foreach(config('app.available_locales') as $i=>$locale)
    <div class="tab-pane fade" id="{{$locale}}" role="tabpanel"
         aria-labelledby="{{$locale}}-tab">

        <div class="widget-content widget-content-area">
            <div class="form-group">
                <label for="name_{{ $locale }}">{{__('Name')}} </label>

                <input id="name_{{ $locale }}" type="text" name="name.{{$locale}}" placeholder=""
                       class="form-control" value="{{  old('name_'.$locale) ?? isset($dataset['facilities']) ?  $dataset['facilities']->getTranslation('name',$locale,false) : null}}">
            </div>

        </div>
    </div>
@endforeach
