@if(isset($dataset['facilities']))
@foreach($dataset['facilities']->facilities()->get() as $facility)
    <div class="form-group">

        <div class="input-group">
            <div class="input-group-append">
                <button class="btn btn-outline-secondary update-facilities"
                        id="{{$facility->id}}"
                        type="button">
                    {{__('Update')}}</button>
            </div>
            @foreach(config('app.available_locales') as $i=>$locale)
                <input type="hidden" name="_locale[]" id="_locale" value="{{$locale}}" >

                <input type="text" placeholder="{{$locale}} - dili için isim giriniz"
                       id="facilities_name_{{$facility->id}}_{{$locale}}"
                       name="facilities_name_{{$facility->id}}_{{$locale}}"
                       class="form-control"
                        value="{{$facility->getTranslation('name',$locale,false)}}">
            @endforeach
        </div>
    </div>
    @endforeach
@endif

@section('FootScripts')
    <script>
        let facility_url = '{{ route("facility.update.ajax",['id'=>':id']) }}';
        let token = '{{ csrf_token() }}';
        $(".update-facilities").click(function (e) {

            let facility_id = $(this).attr('id');
            let locale = $("input[name='_locale[]']");

            facility_url = facility_url.replace(':id', facility_id);

            const formData = {
                '_token': token,
            };

            $.each(locale, function () {
                let locale_name = $(this).val();
                let _name = $('#facilities_name_' + facility_id + '_' + locale_name).val();
                formData['name_' + locale_name] = _name;

            });

            $.ajax({
                url: facility_url,
                type: "PUT",
                data: formData,
                success: function (data) {
                    console.log(data);
                    alert('update completed!');
                 },
                error: function (data) {
                    var errors = data.responseJSON;
                    console.log(errors);
                }
            });

        });
    </script>
@endsection
