
<div class="form-group">
    <label for="icon_name">{{__('Car Capacity')}} </label>

        <input  id="capacity" type="text" name="capacity" placeholder=""
                class="form-control" value="{{ old('capacity') ?? isset($dataset['vehicles']) ?  $dataset['vehicles']->capacity : null}}">
</div>


