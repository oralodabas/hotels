@extends('admin.layouts.master')

@section('HeadScripts')

@endsection
@section('Navbar')
@endsection
@section('Content')
    <div class="layout-px-spacing">
        <div class="row layout-top-spacing">
            <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
                <div class="statbox widget box box-shadow">
                    <div class="widget-header">
                        <div class="row">
                            <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                                <h4>
                                    {{__('Vehicle Insert')}}
                                </h4>
                            </div>
                        </div>
                    </div>
                    <hr/>
                {!! Form::open(['url'=>route('vehicles.store'),'enctype'=>"multipart/form-data",'method' => 'post']) !!}

                  @include('admin.partials.vehicle.tab_heads')
                    <div class="tab-content" id="simpletabContent">
                        <div class="tab-pane fade show active" id="main" role="tabpanel" aria-labelledby="main-tab">
                            @include('admin.partials.vehicle.tab_main')
                        </div>
                        @include('admin.partials.vehicle.tab_localization')


                    </div>
                    <!-- End Tab Content -->
                    <div class="col-12 col-md-12 col-lg-12 text-right">
                        <button type="submit" class="btn btn-primary">{{__('Submit')}}</button>
                    </div>
                    {!!Form::close()!!}
                </div>
            </div>

        </div>

    </div>


@endsection

