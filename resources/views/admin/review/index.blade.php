@extends('admin.layouts.master')

@section('HeadScripts')

@endsection
@section('Navbar')
@endsection
@section('Content')
    <div class="layout-px-spacing">
        <div class="row layout-top-spacing">
            <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">

                <button class="btn btn-primary collapsed mb-2" data-toggle="collapse" data-target="#filter"
                        aria-expanded="true" aria-controls="filter">
                    <i class="fa fa-filter mr-1"></i>Filtrele
                </button>

                <div id="toggleAccordion">
                    <div id="filter" class="collapse mt-2" aria-labelledby="headingOne1" data-parent="#toggleAccordion">
                        <div class="widget-content widget-content-area br-6 mb-2">
                            {!! Form::open(['method'=>'get']) !!}
                            <div class="row">
                                <div class="col-xl-6 col-md-6 col-sm-6 col-6">
                                    <div class="form-group mb-4">
                                        <label for="formGroupExampleInput2">Durum</label>
                                        {!! Form::select('status',[''=>'Hepsini Listele','1'=>'Aktif','0'=>'Pasif'], \Illuminate\Support\Facades\Request::get('status'), ['class'=>' form-control basic']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-12 col-lg-12 text-right">
                                <button type="submit" class="btn btn-primary">Filtrele</button>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>


                <div class="widget-content widget-content-area br-6">
                    <div class="table-responsive mb-4 mt-4">
                        <table id="product" class="table table-hover" style="width:100%">
                            <thead>
                            <tr>
                                <th>{{__('Hotel')}}</th>
                                <th>{{__('Name')}}</th>
                                <th>{{__('Date')}}</th>
                                <th>{{__('Comment')}}</th>
                                <th>{{__('Status')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($dataset['reviews'] as $data)
                                @php
                                $info = json_decode($data->info);
                                @endphp
                                <tr>
                                    <td>{{$data->hotel()->first()->name}}</td>
                                    <td>{{isset($info->name) ? $info->name :'' }} {{isset($info->sur_name) ? $info->sur_name :'' }}</td>
                                    <td>{{\Carbon\Carbon::parse($data->created_at)}}</td>
                                    <td>{{$data->description}}</td>
                                    <td>
                                        <button id="reviews_{{$data->id}}" data-id="{{$data->id}}" class="btn review-status btn-@if($data->status == 'passive'){{'danger'}}@else{{'success'}}@endif mb-2">
                                            @if($data->status == 'passive'){{__('Passive')}}@else{{__('Active')}}@endif
                                        </button></td>


                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{ $dataset['reviews']->links() }}
                    </div>
                </div>
            </div>

        </div>

    </div>

@endsection
@section('FootScripts')
<script>
    let token = '{{ csrf_token() }}';
    let update_reviews_status = "{{route('update.review.status',['id'=>':id'])}}"
    $(".review-status").click(function () {

        var ids = $(this).attr('data-id');
        update_reviews_status = update_reviews_status.replace(':id', ids);

        const formData = {
            '_token': token,
        };


        $.ajax({
            url: update_reviews_status,
            type: "POST",
            data: formData,
            success: function (data) {
                if (data.error) {
                    alert(data.error);
                }
                if (data.status == 'active') {
                    $('#reviews_' + data.id).addClass("btn-success");
                    $('#reviews_' + data.id).removeClass("btn-danger");
                    $('#reviews_' + data.id).text("Active");
                } else {
                    $('#reviews_' + data.id).addClass("btn-danger");
                    $('#reviews_' + data.id).removeClass("btn-success");
                    $('#reviews_' + data.id).text("Passive");
                }
                console.log(data);
            },
            error: function (data) {
                var errors = data.responseJSON;
                console.log(errors);
            }
        });
    });
</script>
@endsection

