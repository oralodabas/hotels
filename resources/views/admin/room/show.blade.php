@extends('admin.layouts.master')

@section('HeadScripts')
@endsection
@section('Navbar')
@endsection
@section('Content')
    <div class="layout-px-spacing">
        <div class="row layout-top-spacing">
            <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
                <div class="statbox widget box box-shadow">
                    <div class="widget-header">
                        <div class="row">
                            <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                                <h4>
                                    {{__('Room Insert')}}
                                </h4>
                            </div>
                        </div>
                    </div>
                    <hr/>
                {!! Form::open(['url'=>route('rooms.update',['room'=>$dataset['room']->id]),'enctype'=>"multipart/form-data",'method' => 'put']) !!}

                <!-- Tab Content -->
                    <ul class="nav nav-tabs  mb-3 mt-3" id="simpletab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="main-tab" data-toggle="tab"
                               href="#main"
                               role="tabpanel" aria-controls="main"
                               aria-selected="true">Hotel Seç</a>
                        </li>
                        @foreach(config('app.available_locales') as $i=>$locale)
                            <li class="nav-item">
                                <a class="nav-link" id="{{$locale}}-tab" data-toggle="tab"
                                   href="#{{$locale}}"
                                   role="tabpanel" aria-controls="{{$locale}}"
                                   aria-selected="false">{{strtoupper($locale)}}</a>
                            </li>
                        @endforeach
                        <li class="nav-item">
                            <a class="nav-link  " id="main-tab" data-toggle="tab"
                               href="#images"
                               role="tabpanel" aria-controls="images"
                               aria-selected="false">{{__('Images')}}</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="simpletabContent">
                        <div class="tab-pane fade show active" id="main" role="tabpanel" aria-labelledby="images-tab">
                            <div class="form-group">
                                <label for="title">{{__('Hotel ismi')}} </label>
                                <input  id="hotel_name" type="text" name="hotel_name" placeholder="{{__('Hotel İsmi Giriniz')}}"
                                        class="typeahead form-control" value="{{ $dataset['room']->hotels()->first()->name}}" autocomplete="off">

                                <input type="hidden" name="hotel_id" id="hotel_id" value="{{ $dataset['room']->hotels()->first()->id}}">
                            </div>
                            <div class="form-group">
                                <label for="quantity">{{__('Oda adeti')}} </label>
                                <input  id="quantity" type="text" name="quantity" placeholder="{{__('Ada Adeti')}}"
                                        class="form-control" value="{{$dataset['room']->hotels()->first()->pivot->quantity}}">
                            </div>
                        </div>

                        @foreach(config('app.available_locales') as $i=>$locale)
                            <div class="tab-pane fade " id="{{$locale}}" role="tabpanel"
                                 aria-labelledby="{{$locale}}-tab">

                                <div class="widget-content widget-content-area">
                                    <div class="form-group">
                                        <label for="title_{{ $locale }}">{{__('Title')}} </label>
                                        <input id="title_{{ $locale }}" type="text" name="title.{{$locale}}" placeholder=""
                                               class="form-control" value="{{$dataset['room']->getTranslation('title',$locale,false)}}">
                                    </div>

                                    <div class="form-group">
                                        <label for="detail_{{ $locale }}">{{__('Detail')}} </label>
                                        <input id="detail_{{ $locale }}" type="text" name="detail.{{$locale}}" placeholder=""
                                               class="form-control" value="{{ $dataset['room']->getTranslation('detail',$locale,false)}}">
                                    </div>
                                </div>
                            </div>
                        @endforeach

                        <div class="tab-pane fade " id="images" role="tabpanel" aria-labelledby="images-tab">
                            <div class="form-group">
                                <label for="title">{{__('Title')}} </label>
                                <input  id="image_title" type="text" name="image_title" placeholder=""
                                        class="form-control" value="@if($dataset['room']->images()->first()){{$dataset['room']->images()->first()->title}}@endif">
                            </div>

                            <div class="form-group">
                                <label for="image">{{__('Image')}} </label>
                                <input type="file" name="files[]" multiple class="form-control" accept="image/*">

                            </div>

                            @foreach($dataset['room']->images()->get() as $i=>$image)
                                <div class="media" id="image_{{$i}}" style="text-align: left">
                                    <div class="n-chk">
                                        <label class="new-control new-radio radio-classic-dark" style="height: 18px;">
                                            <a href="#"
                                               onclick="deleteImage('{{$i}}','{{$image->id}}','{{ $image->path }}','{{ $image->name }}')">
                                                <span class="new-control-indicator">x</span> </a>
                                        </label>
                                    </div>

                                    <div class="media-body">
                                        <img class="rounded" height="100px" style="padding: 10px"
                                             src="{{url('storage/'.sprintf('%s%s',$image->path,$image->name))}}"
                                             alt="pic1">

                                    </div>
                                </div>

                                <!-- media list end-->
                            @endforeach
                        </div>
                    </div>
                    <!-- End Tab Content -->
                    <div class="col-12 col-md-12 col-lg-12 text-right">
                        <button type="submit" class="btn btn-primary">{{__('Submit')}}</button>
                    </div>
                    {!!Form::close()!!}
                </div>
            </div>

        </div>

    </div>

@endsection
@section('FootScripts')
    <script src="{{url('/')}}/assets/js/bootstrap3-typeahead.min.js"></script>

    <script>


        let hotelName = $('#hotel_name').val();
        let formData = {
            '_token': '{{ csrf_token() }}',
            'name': encodeURI(hotelName),

        };

        let path = '{{route('get.hotels.ajax')}}';
        let productIds = new Object();

        $('input.typeahead').typeahead({
            autoSelect: true,
            minLength: 2,
            delay: 400,
            source:  function (query, process) {
                return $.get(path, { query: query }, function (data) {
                    $.getJSON( path,formData, function ( jsonData )
                    {
                        let productNames = new Array();

                        $.each( jsonData, function ( index, product )
                        {
                            productNames.push( product.name );
                            productIds[product.name] = product.id;
                        } );
                        return process(productNames);
                    });
                });
            },
            updater: function (item) {
                console.log(productIds[item],item); //access it here
                $('#hotel_id').val(productIds[item]);
                return item
            }

        });

        function deleteImage(layerId, id, path, name) {
            confirm("Are You sure want to delete image?");

            let url = '{{ route("images.destroy", ":id") }}';
            url = url.replace(':id', id);

            $.ajax({
                url: url,
                type: "DELETE",
                data: {
                    '_token': '{{ csrf_token() }}',
                    'path': path,
                    'name': name,
                    'id': id,
                },
                success: function (data) {
                    $("#image_" + layerId).html(data);
                },
                error: function () {

                }
            });
        }

    </script>
@endsection

