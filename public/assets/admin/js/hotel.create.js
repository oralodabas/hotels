$(document).ready(function () {
    $(".tagging").select2({
        tags: true
    });


    $(".add-facilities").click(function (e) {

        let facility_id = $(this).attr('id');
        let locale = $("input[name='_locale[]']");

        const formData = {
            '_token': token,
            'facility_type_id': facility_id
        };

        $.each(locale, function () {
            let locale_name = $(this).val();
            let _name = $('#facilities_name_' + facility_id + '_' + locale_name).val();
            formData['name_' + locale_name] = _name;

        });

        $.ajax({
            url: facility_url,
            type: "POST",
            data: formData,
            success: function (data) {
                $.each(locale, function () {
                    let locale_name = $(this).val();
                    $('#facilities_name_' + facility_id + '_' + locale_name).val('');
                });
                $("#facilities_" + facility_id).append('<option value="' + data.id + '">' + data.name.tr + '</option>');
            },
            error: function (data) {
                var errors = data.responseJSON;
                console.log(errors);
            }
        });

    });

    $('body').on('click', '.delete-rooms', function (e) {

        let id = $(this).attr('id');
        delete_room = delete_room.replace(':id', id);

        let formData = {
            '_token': token,
            'room_id': id
        }

        let promise = ajaxRequest(delete_room, formData, 'DELETE');

        promise.then(function (data) {
            $('#list_rooms_' + id).remove();
            console.log(data);
        });

    });


    $(".add-rooms").click(function (e) {

        let locale = $("input[name='_locale[]']");
        const formData = {
            '_token': token,
        };

        $.each(locale, function () {
            let locale_name = $(this).val();
            let _name = $('#rooms_name_' + locale_name).val();
            let _detail = $('#rooms_detail_' + locale_name).val();
            formData['title_' + locale_name] = _name;
            formData['detail_' + locale_name] = _detail;
        });

        console.log(formData);
        $.ajax({
            url: rooms_url,
            type: "POST",
            data: formData,
            success: function (data) {
                $.each(locale, function () {
                    let locale_name = $(this).val();
                    $('#rooms_name_' + locale_name).val('');
                });
                $("#room_lists").append('<tr id="list_rooms_' + data.id + '"><td>' + data.title.tr + '</td>' +
                    '<td><input type="button" ' +
                    'class="mt-4 btn btn-primary edit-rooms" ' +
                    'value="düzenle" ' +
                    'data-id="' + data.id + '"' +
                    'data-toggle="modal"' +
                    'data-target="#modal_rooms"> </td>' +
                    '<td><input type="button" ' +
                    'class="mt-4 btn btn-primary delete-rooms" ' +
                    'value="sil" ' +
                    'id="' + data.id + '">' +
                    '</td>' +
                    '</tr>');

                $("#room_lists").append('<input type="hidden" name="rooms[]" id="rooms" value="' + data.id + '">');
            },
            error: function (data) {
                var errors = data.responseJSON;
                console.log(errors);
            }
        });
    });


    $("#country_id").change(function (e) {
        let country_id = $(this).val();
        let formData = {
            '_token': token,
            'country_id': country_id
        }
        let promise = ajaxRequest(state_url, formData, 'POST');

        promise.then(function (data) {
            $('#state-dropdown').html('<option value="">Select State</option>');
            $.each(data.states, function (key, value) {
                $("#state-dropdown").append('<option value="' + value.id + '">' + value.name + '</option>');
            });
        });

    });


    $("#state-dropdown").change(function (e) {
        let state_id = $(this).val();
        let formData = {
            '_token': token,
            'state_id': state_id
        }

        let promise = ajaxRequest(city_url, formData, 'POST');

        promise.then(function (data) {
            $('#city-dropdown').html('<option value="">Select City</option>');
            $.each(data.cities, function (key, value) {
                $("#city-dropdown").append('<option value="' + value.id + '">' + value.name + '</option>');
            });
        });
    });

    $(".hotel-status").click(function () {

        var ids = $(this).attr('data-id');
        url_hotel_status = update_hotel_status.replace(':id', ids);

        const formData = {
            '_token': token,
        };


        $.ajax({
            url: url_hotel_status,
            type: "POST",
            data: formData,
            success: function (data) {
                if (data.error) {
                    alert(data.error);
                }
                if (data.status == 'active') {
                    $('#hotel_' + data.id).addClass("btn-success");
                    $('#hotel_' + data.id).removeClass("btn-danger");
                    $('#hotel_' + data.id).text("Active");
                } else {
                    $('#hotel_' + data.id).addClass("btn-danger");
                    $('#hotel_' + data.id).removeClass("btn-success");
                    $('#hotel_' + data.id).text("Passive");
                }
                console.log(data);
            },
            error: function (data) {
                var errors = data.responseJSON;
                console.log(errors);
            }
        });
    });
});

function ajaxRequest(url, formData, type) {
    return Promise.resolve($.ajax({
        type: type,
        url: url,
        data: formData,
        dataType: 'json',
        success: function (result) {
            return result;
        },
        error: function (error) {
            // alert('error; ' + eval(error));
        },
    }));
}


