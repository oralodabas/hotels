( function($){

    $.fn.scrollSpy = function(options){
      var $Root = $('html');
      var rootScrollBehavior = $Root.css('scroll-behavior');
      var settings = $.extend( {
        offset: 0,
        offsetElement: null,
        activeClass: 'active',
        anchors: ['a[href*=\\#]'],
        ignoreAnchors: [],
        scrollDuration: 0,
        scrollEasing: 'swing',
      }, options );

      if( $.ui === undefined ) {
        // Fallbacks if jQuery UI is not loaded
        settings = $.extend( settings, {
          scrollEasing: 'swing'
        } );
      }

      var scrollTo = function(hash){
        var $Target = $(hash);
        if ( $Target.length ) {
          $Root.css( 'scroll-behavior', 'unset' ); // jquery.animate is not compatible with "scroll-behavior: smooth;"
          $Root.animate( { scrollTop: $Target.offset().top - settings.offset }, {
            duration: settings.scrollDuration,
            easing: settings.scrollEasing,
          } );
          setTimeout(function(){
            $Root.css('scroll-behavior', rootScrollBehavior);
          }, settings.scrollDuration);
        }
      }

      var update = function(){
        // update offset
        if ( $(settings.offsetElement).length ) {
          settings = $.extend( settings, {
            offset: $(settings.offsetElement).height(),
          } );
        }
      }
      update();
      $(window).on('resize', update);

      $(window).on('load', function(){
        if (location.hash) {
          scrollTo(location.hash);
        }
      });

      return this.each(function(){
        var $ScrollSpy = this;
        var $Anchors = $();
        var scrollMap = [];
        var activeNavElement = undefined;

        if ( Array.isArray( settings.anchors ) ) {
          settings.anchors.forEach( function(e) {
            $Anchors = $Anchors.add( $($ScrollSpy).find(e) );
          } );
        }

        if ( Array.isArray( settings.ignoreAnchors ) ) {
          settings.ignoreAnchors.forEach( function(e) {
            $Anchors = $Anchors.filter(':not(' + e + ')');
          } );
        }

        var updateScrollMap = function(){
          $Anchors.each((i,el) => {
            var $Target = $(el.hash);

            if ( $Target.length ) {
              scrollMap.push({
                navElement: el,
                targetOffset: $Target.offset(),
                targetHeight: $Target.outerHeight(),
              });
            }
          });
          scrollMap.sort(function(a,b){
            return a.targetOffset.top - b.targetOffset.top;
          })
          scrollMap.reverse();
        }
        updateScrollMap();
        $(window).on('resize', updateScrollMap);

        var scrollSpy = function(){
          var scrollPos = $(document).scrollTop() + settings.offset;
          var posElement = scrollMap.find(function(el){
            if ( el.targetOffset.top <= scrollPos ) {
              if ( el.targetOffset.top + el.targetHeight >= scrollPos ) {
                return true;
              }
            }
          });

          if ( posElement && posElement.navElement != activeNavElement ) {
            $(activeNavElement).removeClass(settings.activeClass);
            $(posElement.navElement).addClass(settings.activeClass);
            activeNavElement = posElement.navElement;
          } else if (!posElement) {
            $(activeNavElement).removeClass(settings.activeClass);
            activeNavElement = undefined;
          }
        };
        scrollSpy();
        $(document).on('scroll', scrollSpy);

        $Anchors.click(function(e){
          e.preventDefault();
          var hash = e.currentTarget.hash;
          history.pushState({}, '', hash);
          scrollTo(hash);
        });
      });
    }
  })(jQuery);

$(function(){
    $('.slider-area .slider').on('init', function(event, slick) {
        $('.slider-buttons .count').html(slick.currentSlide + 1 + ' / ' + slick.slideCount);
    }).slick({
        slidesToShow: 2,
        infinite: false,
        prevArrow: '.left-arrow',
        nextArrow: '.right-arrow',
        dots: false,
        responsive: [
          {
            breakpoint: 580,
            settings: {
              infinite: true,
              variableWidth: true
            }
          }
        ]
    }).on('beforeChange', function(event, slick, currentSlide, nextSlide) {
        $('.slider-buttons .count').html(nextSlide + 1 + ' / ' + slick.slideCount);
    });


    $('.side-menu').scrollSpy({
        offset: 50,
        scrollDuration: 500, // default: 0
        scrollEasing: 'easeInBack'
    });

    $('input[class*="form-control"]').focus(function(){
        $(this).prev().removeClass('text-gullgray').addClass('text-cello');
    }).focusout(function(){
       $(this).prev().removeClass('text-cello').addClass('text-gullgray');
    });

    $('.select').niceSelect();

	$('.slider-canvas').slick({
		infinite: true,
		dots: false,
		prevArrow: '.slider-canvas-area .prev-arrow',
		nextArrow: '.slider-canvas-area .next-arrow'
	});

	$('.just-number').mask('000');


  $('#galleryOne .gallery-for').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    draggable: false,
    fade: true,
    infinite: true,
    asNavFor: '.gallery-nav',
    prevArrow: '.gallery-top-area .prev-arrow',
    nextArrow: '.gallery-top-area .next-arrow'
  });

  $('#galleryOne .gallery-nav').slick({
    slidesToShow: 8,
    slidesToScroll: 1,
    asNavFor: '.gallery-for',
    draggable: false,
    dots: false,
    centerMode: false,
    focusOnSelect: true,
    responsive: [
      {
        breakpoint: 1399,
        settings: {
          slidesToShow: 5,
          infinite: true,
          arrows: false
        }
      },
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 4,
          infinite: true,
          arrows: false
        }
      },
      {
        breakpoint: 579,
        settings: {
          slidesToShow: 3,
          infinite: true,
          arrows: false
        }
      },
      {
        breakpoint: 410,
        settings: {
          slidesToShow: 2,
          infinite: true,
          arrows: false
        }
      }
    ]
  });

  /* Galler */
  $('.gallery-one').on('click', function(){

    $('#galleryOne').addClass('show');
    $('.shadow').addClass('show');
    return false;
  });

  $('.gallery-two').on('click', function(){
    $('#galleryTwo').addClass('show');
    //$('.shadow').addClass('show');
    return false;
  });

  $('.close-gallery').on('click', function(){
    $('#galleryOne, #galleryTwo').removeClass('show');
    $('.shadow').removeClass('show');
    return false;
  });

  /* Points */
  $('.points .btn').on('click', function(){
    var id = $(this).data('id');
    $('.points .btn').removeClass('very-bad bad normal excellent');

    if(id <= 10){
      $(this).addClass('excellent').prevAll('.btn').addClass('excellent');
    }

    if(id <= 7){
      $('.points .btn').removeClass('excellent');
      $(this).addClass('normal').prevAll('.btn').addClass('normal');
    }

    if(id <= 4){
      $('.points .btn').removeClass('normal');
      $(this).addClass('bad').prevAll('.btn').addClass('bad');
    }

    if(id <= 2){
      $('.points .btn').removeClass('bad');
      $(this).addClass('very-bad').prevAll('.btn').addClass('very-bad');
    }
  });

  $('.phone_tr').mask('+90(000) 000 00 00');
  $('.credit_card').mask('9999 9999 9999 9999');
  $('.mmyy').mask('99/99');
  $('.ccv').mask('9999');

  $('.phone_tr').focus(function(){
    $(this).val('+90');
  });

  if ($(window).width() > 991) {
    $('.sidebar').stickySidebar({
      topSpacing: 50,
      bottomSpacing: 50
    });

    $('.sidebar-2').stickySidebar({
      topSpacing: 260,
      bottomSpacing: 50
    });
 }


  $('#galleryTwo .gallery-for').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    draggable: false,
    fade: true,
    infinite: true,
    asNavFor: '#galleryTwo .gallery-nav',
    prevArrow: '#galleryTwo .gallery-top-area .prev-arrow',
    nextArrow: '#galleryTwo .gallery-top-area .next-arrow'
  });

  $('#galleryTwo .gallery-nav').slick({
    slidesToShow: 8,
    slidesToScroll: 1,
    asNavFor: '#galleryTwo .gallery-for',
    draggable: false,
    dots: false,
    centerMode: false,
    focusOnSelect: true,
    responsive: [
      {
        breakpoint: 1399,
        settings: {
          slidesToShow: 5,
          infinite: true,
          arrows: false
        }
      },
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 4,
          infinite: true,
          arrows: false
        }
      },
      {
        breakpoint: 579,
        settings: {
          slidesToShow: 3,
          infinite: true,
          arrows: false
        }
      },
      {
        breakpoint: 410,
        settings: {
          slidesToShow: 2,
          infinite: true,
          arrows: false
        }
      }
    ]
  });

  $('.lazy').lazy();

  $('.accordion-area .accordion-item').on('click', function(){
    $('.accordion-area .accordion-item input').not($(this).find('input')).prop('checked', false);
  });
});

$('#check_prices_submit').click(function (e){
    e.preventDefault();
    let formData = $('#check_prices').serialize();
    $('#check_prices_submit').html('<span class="spinner-border spinner-border-sm mr-2" role="status" aria-hidden="true"></span>'+loading+'').attr('disabled', true);


    $.ajax({
        url: crawl_url,
        type: "GET",
        data: formData,
        success: function (data) {
            console.log(typeof data);
            console.log(data);
            if(typeof data === 'object' ){
                if (data.length === 0) {
                    $('#check_price_alert').attr('style','display:block');
                    window.setTimeout(function() {
                        $('#check_price_alert').fadeOut();
                    }, 4000);
                }else{
                    let html = '';

                    $.each(data, function (key,room) {
                        html += ' <div class="accordion-item room-detail radius-16px mb-4">';
                        html += '<h2 class="accordion-header" id="headingOne">';
                        html += '<button class="accordion-button radius-16px d-flex shadow-none" type="button"';
                        html += 'data-bs-toggle="collapse" data-bs-target="#itemOne"';
                        html += 'aria-expanded="true" aria-controls="itemOne">';
                        html += '<div class="left d-flex">';
                        html += '<div class="content ms-3">';
                        html += '<div class="title fw-600 text-cello fs-24">'+room.Name+'</div>';
                        html += '<div class="desc text-gullgray fw-light mt-2 fs-14"><i class="icon-bed me-2 text-burning align-middle"></i> '+room.BedType+'</div>';
                        html += '</div>';
                        html += '</div>';
                        html += '</button>';
                        html += '</h2>';

                        html += '<div id="itemOne" class="accordion-collapse collapse show" aria-labelledby="headingOne"';
                        html += 'data-bs-parent="#rooms">';

                        html += '<div class="accordion-body">';
                        html += '<div class="images d-flex justify-content-between" style="overflow: hidden">';
                        $.each(room.Images, function (key,Image) {
                            html += '<a href="#" class="gallery-one"><img src="'+Image+'" class="radius-16px image lazy" alt=""></a>';
                        });
                        html += '</div>';
                        html += '<a class="text-cornflower mt-3 d-inline-block text-hover-cello transition-3s fs-14 room-details" data-bs-toggle="offcanvas" href="#infoOffCanvas" role="button" aria-controls="infoOffCanvas" id="'+room.Id+'">'+show_all_facilities+'<i class="icon-chevron-right align-middle fs-12"></i></a>';
                        html += ' <div class="bottom mt-4 d-block d-lg-flex align-items-center">';
                        html += '<div class="left">';
                        html += '<div class="d-flex d-lg-block align-items-end">';
                        html += '<div class="flex-50">';
                        html += '<div class="price-area">';
                        html += '<div class="old-price text-gullgray text-decoration-line-through fs-12"></div>';
                        html += '<div class="price text-burning fs-24 fw-bold">$'+room.Prices[0].PerNight+'</div>';
                        html += '</div>';
                        html += ' <div class="text-cello fs-12">'+nightly_per_price+'</div>';
                        html += '<div class="flex-50">';
                        html += '<div class="nights-text text-burning my-1 fw-500 fs-12">'+total_price+' $ '+room.Prices[0].Total+'</div>';
                        html += '<div class="cancellation-date text-cornflower py-1 px-2 rounded-pill fs-10 text-center"></div>';
                        html += '</div>';
                        html += '</div>';
                        html += '</div>';
                        html += '<div class="right ms-auto">';
                        html += '<a href="#" class="btn btn-standart radius-12px shadow-none bg-burning text-white text-uppercase  hover-bg-cello btn-mobile-w-100 fw-600">'+book_now+'</a>';
                        html += '</div>';
                        html += '</div>';
                        html += '</div>';
                        html += '</div>';
                        html += '</div>';

                    });

                    $('#room_list').html(html);
                    $('#checkPrices').removeClass('show');
                }
            }else{
                $('#check_price_alert').attr('style','display:block');
                window.setTimeout(function() {
                    $('#check_price_alert').fadeOut();
                }, 4000);
            }
            $('#check_prices_submit').html('check_price').attr('disabled',false);
        },
        error: function (data) {
            $('#check_price_alert').attr('style','display:block');
            window.setTimeout(function() {
                $('#check_price_alert').fadeOut();
            }, 4000);
            $('#check_prices_submit').html('check_price').attr('disabled',false);
            var errors = data.responseJSON;
            console.log(errors);
        }
    });


});

$(document).ready(function(){
    $(document).on('click','.room-details',function() {
        let id = $(this).attr('id');
        const formData = {
            '_token': token,
            'id':id
        };
        $.ajax({
            url: session_get_room,
            type: "GET",
            data: formData,
            success: function (data) {
            console.log(data);
            let html ='';

            html += '<div class="offcanvas-header">';
            html += '<div class="content">';
            html += '<h5 class="offcanvas-title text-cello fs-24 fw-bold" id="infoOffCanvasLabel">'+data["Name"]+'</h5>';
            html += '</div>';
            html += '<button type="button" class="btn-close text-white bg-burning shadow-none rounded-circle" data-bs-dismiss="offcanvas" aria-label="Close"><i class="icon-exit fs-14"></i></button>';
            html += '</div>';
            html +=  '<div class="offcanvas-body">';
            html +=  '<div class="slider-canvas-area position-relative">';
            html +=  '<div class="prev-arrow position-absolute top-50 bg-white  text-dark transition-3s"><i class="icon-chevron-left"></i></div>';
            html +=  '<div class="slider-canvas overflow-hidden">';
                $.each(data["Images"], function (key,Image) {
                    html += '<div class="slide">';
                    html += '<img src="'+Image+'" class="radius-16px object-cover" alt="">';
                    html += '</div>';
                });
            html +=  '</div>';
            html +=  ' <div class="next-arrow position-absolute top-50 bg-white  text-dark transition-3s end-0"><i class="icon-chevron-right"></i></div>';
                html += '</div>';
            html += '<div class="row mt-5">';
            html += '<div class="col-12 col-md-6 d-flex mb-5">';

            html += '<div class="content">';
            html += '<ul class="nav flex-column check-list">';
                $.each(data["Features"], function (key,feature) {
                    html += '<li class="nav-item text-gullgray">'+feature+'</li>';
                })
            html += '</ul>';


            html += '</div>';
            html += '</div>';
            html += '</div>';
                $('#infoOffCanvas').html('');
           $('#infoOffCanvas').append(html);
            $('.slider-canvas').slick({
                infinite: true,
                dots: false,
                prevArrow: '.slider-canvas-area .prev-arrow',
                nextArrow: '.slider-canvas-area .next-arrow'
            });


            },
            error: function (data) {
                var errors = data.responseJSON;
                console.log(errors);
            }
        });

    });
});

$('#add_review_form').submit(function (e){
    e.preventDefault();
    $('#add_review_submit').html('<span class="spinner-border spinner-border-sm mr-2" role="status" aria-hidden="true"></span>'+loading+' ').attr('disabled', true);
    let formData = $('#add_review_form').serialize();

    $.ajax({
        url: add_review,
        type: "POST",
        data: formData,
        success: function (data) {
            console.log(data);
            $('#add_review_submit').html(send_review).attr('disabled',false);
            $('#add_review_form')[0].reset();
        },
        error: function (data) {

            $('#add_review_submit').html(send_review).attr('disabled',false);
            var errors = data.responseJSON;
            console.log(errors);
        }
    });

});







