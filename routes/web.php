<?php

use App\Http\Controllers\Admin\Airpots\AirportController;
use App\Http\Controllers\Admin\CountryStateCity\CountryStateCityController;
use App\Http\Controllers\Admin\Customers\CustomerController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\Facilities\FacilityController;
use App\Http\Controllers\Admin\Faqs\FaqController;
use App\Http\Controllers\Admin\Hotels\HotelController;
use App\Http\Controllers\Admin\Images\ImageController;
use App\Http\Controllers\Admin\Orders\OrderController;
use App\Http\Controllers\Admin\Reviews\ReviewController;
use App\Http\Controllers\Admin\Vehicles\VehicleController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Admin\Rooms\RoomController;
use App\Http\Controllers\Frontend\HomeController as HomeControllerFrontend;
use App\Http\Controllers\Frontend\HotelController as HotelControllerFrontend;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/admin/login', [LoginController::class, 'adminLogin'])->name('admin.login');
Route::post('/admin/login', [LoginController::class, 'adminAuthenticate'])->name('admin.authenticate');
//admin menu
Route::group(['middleware' => 'admin', 'prefix' => 'admin'], function () {
    Route::get('/', [DashboardController::class, 'index'])->name('dashboard.index');
    Route::get('hotels/get-ajax-hotels', [HotelController::class, 'getHotels'])->name('get.hotels.ajax');
    Route::post('hotels/update-status-hotel/{id}', [HotelController::class, 'updateStatusHotel'])->name('update.status.hotel');

    Route::resource('hotels', HotelController::class);
    Route::resource('rooms', RoomController::class);
    Route::post('rooms/set-rooms', [RoomController::class, 'setRooms'])->name('rooms.set.ajax');
    Route::delete('rooms/delete-rooms/{id}', [RoomController::class, 'deleteRooms'])->name('rooms.delete.ajax');
    Route::put('rooms/update-rooms/{id}', [RoomController::class, 'updateRooms'])->name('rooms.update.ajax');
    Route::get('rooms/get-rooms/{id}', [RoomController::class, 'getRooms'])->name('rooms.get.ajax');
    Route::get('images/get-images', [ImageController::class, 'getImages'])->name('images.get.ajax');
    Route::post('images/set-images', [ImageController::class, 'setImages'])->name('images.post.ajax');
    Route::post('images/set-title-images', [ImageController::class, 'setTitleImages'])->name('images.title.post.ajax');
    Route::resource('images', ImageController::class);
    Route::resource('facilities', FacilityController::class);
    Route::get('facilities/get-facility/{id}', [FacilityController::class, 'getFacility'])->name('facility.get.ajax');
    Route::post('facilities/set-facility', [FacilityController::class, 'setFacility'])->name('facility.set.ajax');
    Route::put('facilities/update-facility/{id}', [FacilityController::class, 'updateFacilities'])->name('facility.update.ajax');
    Route::delete('facilities/delete-facility/{id}', [FacilityController::class, 'deleteFacility'])->name('facility.delete.ajax');

    Route::post('review/update-status-review/{id}', [ReviewController::class, 'updateStatusReview'])->name('update.review.status');
    Route::resource('reviews', ReviewController::class);
    Route::resource('faqs', FaqController::class);
    Route::resource('airports', AirportController::class);
    Route::resource('vehicles', VehicleController::class);

    Route::resource('orders', OrderController::class);
    Route::resource('customers', CustomerController::class);

    Route::get('/country-state-city', [CountryStateCityController::class, 'index'])->name('country.index');
    Route::post('/country-state-city/get-state', [CountryStateCityController::class, 'getState'])->name('post.state.index');
    Route::post('/country-state-city/get-city', [CountryStateCityController::class, 'getCity'])->name('post.city.index');
});

Route::get('/crawler',[HotelControllerFrontend::class,'crawler'])->name('crawl.data');
Route::post('/add-review/{id}',[HotelControllerFrontend::class,'addReview'])->name('add.review');
Route::get('/get-session-room',[HotelControllerFrontend::class,'getSessionRoom'])->name('session.get.room');

//web module
Route::get('/', function () {
    return redirect(app()->getLocale());
});
$frontRoutes = function () {
    Route::group([
        'prefix' => '{locale}',
        'where' => ['locale' => '[a-zA-Z]{2}'],
        'middleware' => ['setLocale','subDomain']], function () {
        Route::get('/hotel/{slug}', [HotelControllerFrontend::class, 'show']);
        Route::get('/', [HotelControllerFrontend::class, 'index']);
    });
};
Route::domain('{subdomain}.'.config('app.domain_name'))->group($frontRoutes);
Route::domain(config('domain_name'))->group($frontRoutes);



