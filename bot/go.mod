module hotelsBot

go 1.16

require (
	github.com/gocolly/colly v1.2.0
	github.com/gocolly/colly/v2 v2.1.0
	github.com/joho/godotenv v1.3.0
	github.com/tidwall/gjson v1.8.0
)
