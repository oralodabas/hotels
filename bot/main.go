package main

import (
	"encoding/json"
	"errors"
	"github.com/gocolly/colly/proxy"
	"github.com/gocolly/colly/v2"
	"github.com/joho/godotenv"
	"github.com/tidwall/gjson"
	"io/ioutil"
	"log"
	"math"
	"net/http"
	"os"
	"regexp"
	"strconv"
	"strings"
	"sync"
	"time"
)

type pageInfo struct {
	StatusCode int
	Message    string
	Rooms      []*roomInfo
}

type pageResponse struct {
	Hotels map[string]pageInfo
}

type roomPriceInfo struct {
	PerNight      float64
	NormalPrice   float64
	DiscountPrice float64
	DiscountRate  float64
	Total         float64
	Free          bool
	Features      []string
}

type roomInfo struct {
	Id                  string
	Name                string
	Description         string
	BedType             string
	RoomSize            string
	ViewBadge           string
	MaxOccupancy        string
	BedTypeAndOccupancy string
	Prices              []roomPriceInfo
	Features            []string
	Images              []string
}

type search struct {
	Hotels   []string `json:"hotels"`
	Start    string   `json:"start"`
	End      string   `json:"end"`
	Adults   int      `json:"adults"`
	Children []int    `json:"children"`
}

func handler(w http.ResponseWriter, r *http.Request) {

	headerContentTtype := r.Header.Get("Content-Type")
	if headerContentTtype != "application/json" {
		errorResponse(w, "Content Type is not application/json", http.StatusUnsupportedMediaType)
		return
	}
	var s search
	var unmarshalErr *json.UnmarshalTypeError

	decoder := json.NewDecoder(r.Body)
	decoder.DisallowUnknownFields()
	err := decoder.Decode(&s)
	if err != nil {
		if errors.As(err, &unmarshalErr) {
			errorResponse(w, "Bad Request. Wrong Type provided for field "+unmarshalErr.Field, http.StatusBadRequest)
		} else {
			errorResponse(w, "Bad Request "+err.Error(), http.StatusBadRequest)
		}
		return
	}

	// Instantiate default collector
	c := colly.NewCollector(colly.AllowURLRevisit())

	proxyFileName := "proxyList.txt"
	if Exists(proxyFileName) {
		fileBytes, fileErr := ioutil.ReadFile(proxyFileName)
		if fileErr == nil {
			sliceData := deleteEmpty(strings.Split(string(fileBytes), "\n"))

			if len(sliceData) > 0 {
				rp, proxyErr := proxy.RoundRobinProxySwitcher(sliceData...)
				if proxyErr == nil {
					c.SetProxyFunc(colly.ProxyFunc(rp))
				}
			}
		}
	}

	p := pageResponse{}
	p.Hotels = map[string]pageInfo{}
	var wg sync.WaitGroup
	for _, hotel := range s.Hotels {
		h := hotel
		wg.Add(1)
		go func() {
			defer errorHandler()
			pr := &pageInfo{}
			log.Println("visiting", h)

			c.OnHTML("body", func(e *colly.HTMLElement) {

				body := strings.ReplaceAll(string(e.Response.Body), "PDP:1_02{1}{}", "CroomsAreaC")

				rgx := regexp.MustCompile(`window\.__APOLLO_STATE__=(.*?})<\/script>`)
				rs := rgx.FindStringSubmatch(body)

				gjson.Get(rs[1], "CroomsAreaC.body.roomsAndRates.groups.recommended.rooms").ForEach(func(key, roomData gjson.Result) bool {

					room := addRoom(roomData, s)
					pr.Rooms = append(pr.Rooms, room)

					return true
				})

				gjson.Get(rs[1], "CroomsAreaC.body.roomsAndRates.groups.base.rooms").ForEach(func(key, roomData gjson.Result) bool {

					room := addRoom(roomData, s)
					pr.Rooms = append(pr.Rooms, room)

					return true
				})

			})

			// extract status code
			c.OnResponse(func(r *colly.Response) {
				//log.Println("response received", r.StatusCode)
				pr.StatusCode = r.StatusCode
			})
			c.OnError(func(r *colly.Response, err error) {
				//log.Println("error:", r.StatusCode, err)
				pr.StatusCode = r.StatusCode
			})

			c.Visit(generateUrl(h, s))

			p.Hotels[h] = *pr
			defer wg.Done()
		}()
	}

	wg.Wait()

	// dump results
	b, err := json.Marshal(p)
	if err != nil {
		log.Panicln("failed to serialize response:", err)
		return
	}
	w.Header().Add("Content-Type", "application/json")
	w.Write(b)
}

func addRoom(roomData gjson.Result, s search) *roomInfo {
	roomPrices := make([]roomPriceInfo, 0)
	roomData.Get("ratePlans").ForEach(func(key, priceData gjson.Result) bool {
		roomPrice := roomPriceInfo{}
		roomPrice.Free = priceData.Get("cancellations.0.free").Bool()
		roomPrice.NormalPrice = stringToFloat(priceData.Get("price.old").String())
		roomPrice.DiscountPrice = stringToFloat(priceData.Get("price.current").String())
		roomPrice.PerNight = stringToFloat(priceData.Get("price.priceBreakdown.lineItems.0.feeDetails").String())
		roomPrice.DiscountRate = stringToFloat(priceData.Get("deal.discountPercent").String())

		if roomPrice.NormalPrice == 0 {
			roomPrice.NormalPrice = roomPrice.DiscountPrice
			roomPrice.DiscountPrice = 0
		}

		if roomPrice.Total = roomPrice.NormalPrice; roomPrice.NormalPrice > roomPrice.DiscountPrice {
			roomPrice.Total = roomPrice.DiscountPrice
		}
		if roomPrice.Total == 0 {
			roomPrice.Total = roomPrice.NormalPrice
		}

		if roomPrice.PerNight == 0 {
			startDate, _ := time.Parse("2006-01-02", s.Start)
			endDate, _ := time.Parse("2006-01-02", s.End)
			days := endDate.Sub(startDate).Hours() / 24
			roomPrice.PerNight = math.Round((roomPrice.Total / days) * 100 / 100)
		}

		features := []string{}
		priceData.Get("featureList").ForEach(func(key, feature gjson.Result) bool {
			features = append(features, feature.Get("title").String())
			return true
		})
		roomPrice.Features = features

		roomPrices = append(roomPrices, roomPrice)

		return true
	})

	images := []string{}
	roomData.Get("images.#.fullSizeUrl").ForEach(func(key, image gjson.Result) bool {
		images = append(images, image.String())
		return true
	})

	features := []string{}
	roomData.Get("additionalInfo.details.amenities").ForEach(func(key, feature gjson.Result) bool {
		features = append(features, feature.String())
		return true
	})

	replacer := strings.NewReplacer("<sup>", "", "</sup>", "")

	room := &roomInfo{}
	room.Id = strings.Trim(roomData.Get("roomId").String(), " ")
	room.Name = strings.Trim(roomData.Get("name").String(), " ")
	room.Description = strings.Trim(roomData.Get("additionalInfo.description").String(), " ")
	room.BedType = strings.Trim(roomData.Get("bedTypeAndOccupancy.bedTypes.0").String(), " ")
	room.RoomSize = replacer.Replace(strings.Trim(roomData.Get("additionalInfo.details.exposedAmenities.roomSize").String(), " "))
	room.ViewBadge = strings.Trim(roomData.Get("additionalInfo.viewBadge").String(), " ")
	room.MaxOccupancy = strings.Trim(roomData.Get("maxOccupancy.messageTotal").String()+roomData.Get("maxOccupancy.messageChildren").String(), " ")
	room.BedTypeAndOccupancy = strings.Trim(roomData.Get("bedTypeAndOccupancy.bedTypes.0").String(), " ")
	room.Prices = roomPrices
	room.Images = images
	room.Features = features

	return room
}

func main() {
	godotenv.Load()

	listenPort := os.Getenv("LISTENER_PORT")
	if len(listenPort) == 0 {
		listenPort = "8080"
	}
	addr := ":" + listenPort
	http.HandleFunc("/", handler)

	log.Println("listening on", addr)
	log.Fatal(http.ListenAndServe(addr, nil))
}

func stringToFloat(v string) float64 {
	v = strings.ReplaceAll(v, "$", "")
	v = strings.ReplaceAll(v, ",", ".")
	v = strings.ReplaceAll(v, ".", "")
	v = strings.Trim(v, " ")
	oldPrices := strings.Split(v, " ")

	result := ""
	if len(oldPrices) > 0 {
		for _, vo := range oldPrices {
			if _, err := strconv.ParseFloat(vo, 64); err == nil {
				result = strings.Trim(vo, " ")
			}
		}
	} else {
		result = strings.Trim(v, " ")
	}

	f, _ := strconv.ParseFloat(result, 64)

	return f
}

func errorResponse(w http.ResponseWriter, message string, httpStatusCode int) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(httpStatusCode)

	resp := &pageInfo{}
	resp.StatusCode = httpStatusCode
	resp.Message = message
	jsonResp, _ := json.Marshal(resp)
	w.Write(jsonResp)
}

func generateUrl(hotel string, s search) string {

	ageQuery := ""
	for i, childAge := range s.Children {
		row := strconv.Itoa(i)
		age := strconv.Itoa(childAge)
		ageQuery += ageQuery + "&q-room-0-child-" + row + "-age=" + age
	}
	adults := strconv.Itoa(s.Adults)
	childrenCount := strconv.Itoa(len(s.Children))
	return hotel + "?cur=USD&q-check-in=" + s.Start + "&q-check-out=" + s.End + "&q-rooms=1&q-room-0-adults=" + adults + "&q-room-0-children=" + childrenCount + ageQuery

}

func Exists(name string) bool {
	if _, err := os.Stat(name); err != nil {
		if os.IsNotExist(err) {
			return false
		}
	}
	return true
}

func errorHandler() {
	if r := recover(); r != nil {
		log.Println("errorHandler", r)
	}
}

func deleteEmpty(s []string) []string {
	var r []string
	for _, str := range s {
		if str != "" {
			r = append(r, str)
		}
	}
	return r
}
