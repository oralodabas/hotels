<?php

namespace App\Logging;

use Illuminate\Support\Facades\Session;
use Monolog\Formatter\LineFormatter;

class LoggerFormatter
{

    public function __invoke( $logger)
    {
        foreach ($logger->getHandlers() as $handler)
        {
            $sessionId = Session::getId();

            $handler->setFormatter(new LineFormatter(
                "[%datetime%][".$sessionId."][%level_name%] : %message% %context% %extra% \n", // Format of message in log, default [%datetime%] %channel%.%level_name%: %message% %context% %extra%\n
                "Y-m-d h:i:s", // Datetime format
                true, // allowInlineLineBreaks option, default false
                true  // discard empty Square brackets in the end, default false
            ));
        }
    }
}
