<?php

namespace App\Http\Controllers;

use App\Helpers\GeneralHelper;
use App\Services\NearByTypeService;
use Illuminate\Http\Request;
use Illuminate\View\View;

class NearByTypeController extends Controller
{
    /**
     * @var NearByTypeService
     */
    protected $service;

    /**
     * @var GeneralHelper
     */
    protected $helper;

    protected $pageTitle;

    /**
     * Controller constructor.
     */
    public function __construct()
    {
        $this->service = new NearByTypeService();
        $this->helper = new GeneralHelper();

        $this->pageTitle = __('Near By Types');
    }

    /**
     * @return View
     */
    public function index(): View
    {
        $pageTitle = $this->pageTitle;
        $pageDescription = __('Near By Type List');

        $data = array();

        $nearByTypes = $this->service->index($data);

        return view(
            'dashboard.views.near-by-type.index',
            compact('nearByTypes', 'pageTitle', 'pageDescription')
        );
    }

    /**
     * @return View
     */
    public function create(): View
    {
        $pageTitle = $this->pageTitle;
        $pageDescription = __('New Add');

        return view(
            'dashboard.views.near-by-type.create',
            compact('pageTitle', 'pageDescription')
        );
    }
}
