<?php

namespace App\Http\Controllers;

use App\Helpers\GeneralHelper;
use App\Services\DashboardService;
use Illuminate\View\View;

class DashboardController extends Controller
{
    /**
     * @var DashboardService
     */
    protected $service;

    /**
     * @var GeneralHelper
     */
    protected $helper;

    protected $pageTitle;

    /**
     * Controller constructor.
     */
    public function __construct()
    {
        $this->service = new DashboardService();
        $this->helper = new GeneralHelper();
        $this->pageTitle = __('Dashboard');
    }


    public function index()
    {
        $pageTitle = $this->pageTitle;
        $pageDescription = 'Some description for the page';
        return view('dashboard.dashboard', compact('pageTitle', 'pageDescription'));
    }
}
