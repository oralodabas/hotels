<?php

namespace App\Http\Controllers;

use App\Helpers\GeneralHelper;
use App\Services\LanguageService;
use Illuminate\Http\Request;
use Illuminate\View\View;

class LanguageController extends Controller
{
    /**
     * @var LanguageService
     */
    protected $service;

    /**
     * @var GeneralHelper
     */
    protected $helper;

    protected $pageTitle;

    /**
     * Controller constructor.
     */
    public function __construct()
    {
        $this->service = new LanguageService();
        $this->helper = new GeneralHelper();

        $this->pageTitle = __('Languages');
    }

    /**
     * @return View
     */
    public function index(): View
    {
        $pageTitle = $this->pageTitle;
        $pageDescription = __('Language List');

        $data = array();

        $languages = $this->service->index($data);

        return view(
            'dashboard.views.language.index',
            compact('languages', 'pageTitle', 'pageDescription')
        );
    }

    /**
     * @return View
     */
    public function create(): View
    {
        $pageTitle = $this->pageTitle;
        $pageDescription = __('New Add');

        return view(
            'dashboard.views.language.create',
            compact('pageTitle', 'pageDescription')
        );
    }
}
