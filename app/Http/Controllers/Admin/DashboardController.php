<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\Admin\DashboardServiceInterface;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    /**
     * @var DashboardServiceInterface
     */
    private $service;

    /**
     * DashboardController constructor.
     * @param DashboardServiceInterface $service
     */
    public function __construct(DashboardServiceInterface $service)
    {
        $this->service = $service;
    }

    public function index()
    {
        $dataset = $this->service->index();

        return view('admin.dashboard.index', compact(['dataset']));
    }
}
