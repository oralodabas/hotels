<?php

namespace App\Http\Controllers\Admin\Reviews;

use App\Exceptions\ExceptionInterface;
use App\Http\Controllers\Controller;
use App\Services\Admin\Reviews\ReviewServiceInterface;
use App\Services\BaseServiceInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class ReviewController extends Controller
{

    public const NAME = 'review';

    /**
     * @var ReviewServiceInterface
     */
    private $service;

    public function __construct(ReviewServiceInterface $service)
    {
        $this->service = $service;
    }

    //
    public function index()
    {
        try {
            $dataset = $this->service->index();

            return view('admin.' . self::NAME . '.index', compact(['dataset']));

        } catch (\Exception $e) {
            abort(500);
        }
    }

    public function create()
    {
        // TODO: Implement create() method.
    }

    public function store(Request $request)
    {
        // TODO: Implement store() method.
    }

    public function show(int $id)
    {
        // TODO: Implement show() method.
    }

    public function edit(int $id)
    {
        // TODO: Implement edit() method.
    }

    public function update(int $id, Request $request)
    {
        // TODO: Implement update() method.
    }

    public function destroy(int $id, Request $request)
    {
        // TODO: Implement destroy() method.
    }

    /**
     * @param int $id
     * @param Request $request
     * @return JsonResponse|RedirectResponse
     */
    public function updateStatusReview(int $id,Request $request): JsonResponse
    {
        try {

            $data = $this->service->updateStatusReview($id,$request);
            return Response()->json($data);

        } catch (ExceptionInterface $exception) {
            return  Response()->json(['error'=>$exception->getMessage()]);
        }

    }
}
