<?php

namespace App\Http\Controllers\Admin\Images;

use App\Exceptions\ExceptionInterface;
use App\Http\Controllers\Admin\BaseControllerInterface;
use App\Http\Controllers\Controller;
use App\Http\Requests\ImageStoreRequest;
use App\Services\Admin\Images\ImageServiceInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ImageController extends Controller
{
    /**
     * @var ImageServiceInterface
     */
    private $service;

    /**
     * ImageController constructor.
     * @param ImageServiceInterface $service
     */
    public function __construct(ImageServiceInterface $service)
    {
        $this->service = $service;
    }

    public function index()
    {
        $dataset = $this->service->index();

        return view('admin.image.index', compact(['dataset']));
    }

    public function create()
    {
        $dataset = $this->service->create();

        return view('admin.image.create', compact(['dataset']));
    }

    /**
     * @param ImageStoreRequest $request
     * @return RedirectResponse
     */
    public function store(ImageStoreRequest $request)
    {
        try {
            $this->service->store($request);

            return redirect()
                ->back()
                ->with('success', 'form has been submitted successfully');

        } catch (ExceptionInterface $exception) {
            return back()->withErrors($exception->getMessage());
        }
    }

    public function destroy(int $id,Request $request)
    {
        try {
           return $this->service->destroy($id,$request);

            return redirect()
                ->back()
                ->with('success', 'image has deleted');

        }catch (ExceptionInterface $exception)
        {
            return back()->withErrors($exception->getMessage());
        }
    }


    public function show(int $id)
    {
        // TODO: Implement show() method.
    }

    public function edit(int $id)
    {
        // TODO: Implement edit() method.
    }

    public function update(int $id, Request $request)
    {
        // TODO: Implement update() method.
    }

    /**
     * @param Request $request
     */
    public function getImages(Request $request)
    {
        try {
            if($request->ajax()){
                $dataset = $this->service->getImages($request);

                return view('admin.image.get_ajax', compact(['dataset']));
            }

        }catch (ExceptionInterface $exception)
        {
            return Response()->json(['status'=>false,'error'=>$exception->getMessage()]);
        }
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function setImages(Request $request)
    {
        try {
            if($request->ajax()){

                $dataset = $this->service->setImages($request);
                return Response()->json($dataset);
            }

        }catch (ExceptionInterface $exception)
        {
            return Response()->json(['status'=>false,'error'=>$exception->getMessage()]);
        }
    }


    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function setTitleImages(Request $request)
    {
        try {
            if($request->ajax()){

                $dataset = $this->service->setTitleImages($request);
                return Response()->json($dataset);
            }

        }catch (ExceptionInterface $exception)
        {
            return Response()->json(['status'=>false,'error'=>$exception->getMessage()]);
        }
    }

}
