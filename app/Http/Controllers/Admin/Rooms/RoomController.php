<?php

namespace App\Http\Controllers\Admin\Rooms;

use App\Exceptions\ExceptionInterface;
use App\Http\Controllers\Admin\BaseControllerInterface;
use App\Http\Controllers\Controller;
use App\Http\Requests\RoomStoreRequest;
use App\Http\Requests\SetRoomsRequest;
use App\Services\Admin\Rooms\RoomServiceInterface;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class RoomController extends Controller
{

    /**
     * @var RoomServiceInterface
     */
    private $service;

    public function __construct(RoomServiceInterface $service)
    {
        $this->service = $service;
    }


    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View|Response
     */
    public function index()
    {
        $dataset = $this->service->index();

        return view('admin.room.index', compact(['dataset']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View|Response
     */
    public function create()
    {
        $dataset = $this->service->create();

        return view('admin.room.create', compact(['dataset']));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param RoomStoreRequest $request
     * @return RedirectResponse
     */
    public function store(RoomStoreRequest $request)
    {
        try {
            $this->service->store($request);

            return redirect()
                ->back()
                ->with('success', 'form has been submitted successfully');

        } catch (ExceptionInterface $exception) {
            return back()->withErrors($exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Application|Factory|View|RedirectResponse
     */
    public function show(int $id)
    {
        try {

            $dataset = $this->service->show($id);

            return view('admin.room.show', compact(['dataset']));

        } catch (ExceptionInterface $exception) {
            return back()->withErrors($exception->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param RoomStoreRequest $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(RoomStoreRequest  $request, int $id)
    {
        try {
            $this->service->update($id,$request);

            return redirect()
                ->back()
                ->with('success', 'form has been updated successfully');

        } catch (ExceptionInterface $exception) {
            return back()->withErrors($exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function setRooms(SetRoomsRequest $request)
    {
        try {
            $data = $this->service->setRooms($request);
            return response()->json($data);

        } catch (ExceptionInterface $exception) {
            return response()->json(['success' => false,'error'=>$exception->getMessage()]);
        }
    }

    public function updateRooms(int $id, Request $request)
    {
        try {


            $data = $this->service->updateRooms($id, $request);
            return response()->json($data);

        } catch (ExceptionInterface $exception) {
            return response()->json(['success' => false,'error'=>$exception->getMessage()]);
        }
    }

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function getRooms(int $id)
    {
        try {
            $data = $this->service->getRooms($id);
            return response()->json($data);

        } catch (ExceptionInterface $exception) {
            return response()->json(['success' => false,'error'=>$exception->getMessage()]);
        }
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function deleteRooms(Request $request)
    {
        try {
            $data = $this->service->deleteRooms($request);
            return response()->json($data);

        } catch (ExceptionInterface $exception) {
            return response()->json(['success' => false,'error'=>$exception->getMessage()]);
        }
    }
}
