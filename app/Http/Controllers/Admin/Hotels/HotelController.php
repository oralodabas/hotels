<?php

namespace App\Http\Controllers\Admin\Hotels;

use App\Exceptions\ExceptionInterface;
use App\Http\Controllers\Controller;
use App\Http\Requests\HotelStoreRequest;
use App\Services\Admin\Hotels\HotelServiceInterface;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class HotelController extends Controller
{
    public const NAME = 'hotel';

    /**
     * @var HotelServiceInterface
     */
    private $service;

    public function __construct(HotelServiceInterface $service)
    {
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View|Response
     */
    public function index(): View
    {
        $dataset = $this->service->index();

        return view('admin.' . self::NAME . '.index', compact(['dataset']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View|Response
     */
    public function create(): View
    {
        $dataset = $this->service->create();

        return view('admin.' . self::NAME . '.create', compact(['dataset']));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param HotelStoreRequest $request
     * @return RedirectResponse
     */
    public function store(HotelStoreRequest $request): RedirectResponse
    {
        try {

            $hotel = $this->service->store($request);

            return redirect(route('hotels.update',$hotel->id))
                ->with('success', 'form has been submitted successfully');

        } catch (ExceptionInterface $exception) {
            return back()->withErrors($exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Application|Factory|View|RedirectResponse
     */
    public function show(int $id)
    {
        try {
            $dataset = $this->service->show($id);

            return view('admin.' . self::NAME . '.show', compact(['dataset']));

        } catch (ExceptionInterface $exception) {
            return back()->withErrors($exception->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit(int $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param HotelStoreRequest $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(int $id, HotelStoreRequest $request)
    {
        try {
            $this->service->update($id, $request);

            return redirect()
                ->back()
                ->with('success', 'form has been updated successfully');

        } catch (ExceptionInterface $exception) {
            return back()->withErrors($exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * @param Request $request
     * @return JsonResponse|RedirectResponse
     */
    public function getHotels(Request $request): JsonResponse
    {
        try {

            $data = $this->service->getHotels($request);
            return Response()->json($data);

        } catch (ExceptionInterface $exception) {
            return back()->withErrors($exception->getMessage());
        }

    }

    /**
     * @param int $id
     * @param Request $request
     * @return JsonResponse|RedirectResponse
     */
    public function updateStatusHotel(int $id,Request $request): JsonResponse
    {
        try {

            $data = $this->service->updateStatusHotel($id,$request);
            return Response()->json($data);

        } catch (ExceptionInterface $exception) {
            return  Response()->json(['error'=>$exception->getMessage()]);
        }

    }


}
