<?php

namespace App\Http\Controllers\Admin\Facilities;

use App\Exceptions\ExceptionInterface;
use App\Http\Controllers\Controller;
use App\Http\Requests\SetFacilityRequest;
use App\Services\Admin\Facilities\FacilityServiceInterface;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class FacilityController extends Controller
{
    public const NAME = 'facility';

    /**
     * @var FacilityServiceInterface
     */
    private $service;

    public function __construct(FacilityServiceInterface $service)
    {
        $this->service = $service;
    }

    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        try {
            $dataset = $this->service->index();

            return view('admin.' . self::NAME . '.index', compact(['dataset']));

        } catch (Exception $e) {
            abort(500);
        }
    }

    /**
     * @return Application|Factory|View
     */
    public function create()
    {
        try {

            $dataset = $this->service->create();

            return view('admin.' . self::NAME . '.create', compact(['dataset']));

        } catch (Exception $e) {
            abort(500);
        }
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        try {
            $this->service->store($request);

            return redirect()
                ->back()
                ->with('success', 'form has been submitted successfully');

        } catch (ExceptionInterface $exception) {
            return back()->withErrors($exception->getMessage());
        }
    }

    /**
     * @param int $id
     * @return Application|Factory|View|RedirectResponse
     */
    public function show(int $id): View
    {
        try {
            $dataset = $this->service->show($id);

            return view('admin.' . self::NAME . '.show', compact(['dataset']));

        } catch (ExceptionInterface $exception) {
            return back()->withErrors($exception->getMessage());
        }
    }

    public function edit(int $id)
    {
        // TODO: Implement edit() method.
    }

    /**
     * @param int $id
     * @param Request $request
     * @return RedirectResponse
     */
    public function update(int $id, Request $request): RedirectResponse
    {
        try {
            $this->service->update($id, $request);

            return redirect()
                ->back()
                ->with('success', 'form has been updated successfully');

        } catch (ExceptionInterface $exception) {
            return back()->withErrors($exception->getMessage());
        }
    }

    public function destroy(int $id, Request $request)
    {
        // TODO: Implement destroy() method.
    }

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function getFacility(int $id)
    {
        $data = $this->service->getFacility($id);

        return response()->json($data);

    }

    public function setFacility(SetFacilityRequest $request)
    {
        try {
            $data = $this->service->setFacility($request);

            return response()->json($data);

        } catch (ExceptionInterface $exception) {
            return response()->json(['success' => false,'error'=>$exception->getMessage()]);
        }
    }

    public function deleteFacility(int $id)
    {
        $this->service->deleteFacility($id);
        return response()->json(['success' => 'Form is successfully submitted!']);
    }

    public function updateFacilities(int $id,Request $request)
    {
        try {
            $data = $this->service->updateFacilities($id,$request);

            return response()->json($data);

        } catch (ExceptionInterface $exception) {
            return response()->json(['success' => false,'error'=>$exception->getMessage()]);
        }
    }
}
