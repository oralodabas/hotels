<?php


namespace App\Http\Controllers\Admin;


use Illuminate\Http\Request;

interface BaseControllerInterface
{
    /**
     * @return mixed
     */
    public function index();

    /**
     * @return mixed
     */
    public function create();

    /**
     * @param  $request
     * @return mixed
     */
    public function store($request);

    /**
     * @param int $id
     * @return mixed
     */
    public function show(int $id);

    /**
     * @param int $id
     * @return mixed
     */
    public function edit(int $id);

    /**
     * @param int $id
     * @param  $request
     * @return mixed
     */
    public function update(int $id, $request);

    /**
     * @param int $id
     * @param  $request
     * @return mixed
     */
    public function destroy(int $id, $request);


}
