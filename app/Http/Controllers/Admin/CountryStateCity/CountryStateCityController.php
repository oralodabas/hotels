<?php

namespace App\Http\Controllers\Admin\CountryStateCity;

use App\Http\Controllers\Controller;
use App\Services\Admin\CountryStateCity\CountryStateCityServiceInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class CountryStateCityController extends Controller
{
    /**
     * @var CountryStateCityServiceInterface
     */
    private $service;

    public function __construct(CountryStateCityServiceInterface $service)
    {
        $this->service = $service;
    }

    /**
     * @return mixed
     */
    public function index()
    {
        return $this->service->index();
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function getState(Request $request) : JsonResponse
    {
        return $request->ajax()
            ? response()->json($this->service->getState($request))
            : response()->json([]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function getCity(Request $request) : JsonResponse
    {
        return $request->ajax()
            ? response()->json($this->service->getCity($request))
            : response()->json([]);
    }
}
