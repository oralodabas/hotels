<?php

namespace App\Http\Controllers;

use App\Helpers\GeneralHelper;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\View\View;

class UserController extends Controller
{
    /**
     * @var UserService
     */
    protected $service;

    /**
     * @var GeneralHelper
     */
    protected $helper;

    protected $pageTitle;

    /**
     * Controller constructor.
     */
    public function __construct()
    {
        $this->service = new UserService();
        $this->helper = new GeneralHelper();

        $this->pageTitle = __('Users');
    }

    /**
     * @return View
     */
    public function index(): View
    {
        $pageTitle = $this->pageTitle;
        $pageDescription = __('User List');

        $data = array();

        $users = $this->service->index($data);

        return view(
            'dashboard.views.user.index',
            compact('users', 'pageTitle', 'pageDescription')
        );
    }

    /**
     * @return View
     */
    public function create(): View
    {
        $pageTitle = $this->pageTitle;
        $pageDescription = __('New Add');

        return view(
            'dashboard.views.user.create',
            compact('pageTitle', 'pageDescription')
        );
    }
}
