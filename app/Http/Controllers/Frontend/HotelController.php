<?php

namespace App\Http\Controllers\Frontend;

use App\Exceptions\ExceptionInterface;
use App\Http\Controllers\Controller;
use App\Http\Requests\CrawlUrlRequest;
use App\Services\Frontend\Hotel\HotelServiceInterface;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Redirect;

class HotelController extends Controller
{
    /**
     * @var HotelServiceInterface
     */
    private $service;

    /**
     * HotelController constructor.
     * @param HotelServiceInterface $service
     */
    public function __construct(HotelServiceInterface $service)
    {
        $this->service = $service;
    }

    /**
     * @param Request $request
     * @return Application|Factory|View
     */
    public function index(Request $request)
    {
        $dataset = $this->service->index($request);

        return view('frontend.hotel.index', compact(['dataset']));
    }

    /**
     * @param Request $request
     * @return Application|Factory|View|RedirectResponse|Redirector
     */
    public function show(  Request $request)
    {
        try {

            $dataset = $this->service->show($request);

            return view('frontend.hotel.show', compact(['dataset']));

        } catch (ExceptionInterface $e) {
            return Redirect('/');
        }

    }

    /**
     * @param CrawlUrlRequest $request
     * @return JsonResponse|RedirectResponse
     */
    public function crawler(CrawlUrlRequest $request)
    {
        try {

            $data = $this->service->crawler($request);

            return Response()->json($data);

        } catch (ExceptionInterface $exception) {
            return Response()->json($exception->getMessage());

        }
    }

    /**
     * @param Request $request
     * @return JsonResponse|RedirectResponse
     */
    public function getSessionRoom(Request $request)
    {
        try {
            $data = $this->service->getSessionRoom($request);
            return Response()->json($data);

        } catch (ExceptionInterface $exception) {
            return Response()->json($exception->getMessage());
        }
    }


    /**
     * @param int $id
     * @param Request $request
     * @return JsonResponse|RedirectResponse
     */
    public function addReview(int $id,Request $request)
    {
        try {
            $data = $this->service->addReview($id,$request);
            return Response()->json($data);

        } catch (ExceptionInterface $exception) {
            return Response()->json($exception->getMessage());
        }
    }

}
