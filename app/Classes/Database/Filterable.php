<?php

namespace App\Classes\Database;

use Illuminate\Support\Str;

class Filterable
{

    private $query;
    private $filters;
    private $class;

    /**
     * Filterable constructor.
     * @param $query
     * @param $filters
     * @param $class
     */
    public function __construct($query, $filters, $class)
    {
        $this->query = $query;
        $this->filters = $filters;
        $this->class = $class;
    }

    /**
     * @return mixed
     */
    public function init()
    {
        foreach ($this->filters as $name => $value) {
            $class = $this->class . "\\" . Str::ucfirst($name);

            if (!class_exists($class)) {
                continue;
            }

            if ($value != null && strlen($value)) {
                return (new $class($this->query))->handle($value);
            }

        }

        return $this->query;
    }
}
