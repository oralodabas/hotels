<?php


namespace App\Classes\Tools;


use App\Exceptions\ServiceException;
use App\Models\Hotel;
use Illuminate\Http\Client\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;

class CrawlerHotel implements CrawlerHotelInterface
{

    /**
     * @var Request
     */
    private $request;

    /**
     * CrawlerHotel constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     *
     * @return array
     * @throws ServiceException
     */
    public function init(): array
    {
        try {

            if ($room = Session::get($this->request->curl_url . $this->request->start .
                $this->request->end . $this->request->adults)) {
                return $room;
            }

            $postData = $this->parser();
            $url = env('API_BOT_URL','dd');
            Log::info($url);

            $http = Http::post(env('API_BOT_URL',''), $postData)->json();

            $rooms = $this->getRooms($http);

            if (!empty($rooms)) {
                Session::put('curl_url', $this->request->curl_url);
                Session::put('start', $this->request->start);
                Session::put('end', $this->request->end);
                Session::put('adults', $this->request->adults);

                Session::put($this->request->curl_url . $this->request->start .
                    $this->request->end . $this->request->adults, $rooms);
            }

            return $rooms;

        } catch (\Exception $e) {
            throw new ServiceException($e->getMessage());
        }
    }

    /**
     * @return array
     * @throws ServiceException
     */
    private function parser(): array
    {

        $data[self::CURL_START_DATE_NAME] = $this->request->start;

        $data[self::CURL_END_DATE_NAME] = $this->request->end;

        $data[self::CURL_ADULTS_NAME] = (int)$this->request->adults;

        $data[self::CURL_CHILDREN_NAME] = [(int)$this->request->children];

        $data[self::CURL_HOTELS_NAME] = [$this->request->curl_url];

        return $data;
    }

    /**
     * @param array $hotels
     * @return array
     * @throws ServiceException
     */
    private function getRooms(array $hotels): array
    {
        foreach ($hotels as $hotel) {
            foreach ($hotel as $rooms) {
                if ($rooms[self::PARSE_ROOMS_NAME] == null)
                    return [];

                if (isset($rooms[self::PARSE_ROOMS_NAME]) && $rooms[self::PARSE_ROOMS_NAME] != null) {
                    return $rooms[self::PARSE_ROOMS_NAME];
                }
            }
        }

        throw new ServiceException('not found rooms', [$hotels]);

    }

}
