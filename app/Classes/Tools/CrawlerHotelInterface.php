<?php


namespace App\Classes\Tools;


interface CrawlerHotelInterface
{

    public const API_REMOTE_URL = 'https://bot.95.179.252.160.getmoss.site/';
    public const API_BOT_URL = 'http://127.0.0.1:7171';
    public const PARAM_HOTEL_COM_START_DATE = 'q-check-in';
    public const PARAM_HOTEL_COM_END_DATE = 'q-check-out';
    public const PARAM_HOTEL_COM_ADULTS = 'q-room-0-adults';
    public const PARAM_HOTEL_COM_CHILDREN = 'q-room-0-children';
    public const PARAM_HOTEL_COM_CHILD_AGE = 'q-room-0-child-0-age';

    public const CURL_START_DATE_NAME = 'start';
    public const CURL_END_DATE_NAME = 'end';
    public const CURL_ADULTS_NAME = 'adults';
    public const CURL_CHILDREN_NAME = 'children';
    public const CURL_HOTELS_NAME = 'hotels';
    public const PARSE_HOTEL_NAME = 'Hotels';
    public const PARSE_ROOMS_NAME = 'Rooms';
}
