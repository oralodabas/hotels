<?php


namespace App\Classes\Tools;


/**
 * Class status
 *
 * provides status check and returns text
 */
class StatusChecker
{
    const ACTIVE = 1;
    const ACTIVE_TEXT = 'Aktif';
    const PASSIVE = 0;
    const PASSIVE_TEXT = 'Pasif';
    const DANGERS_CLASS_TEXT = 'danger';
    const PRIMARY_CLASS_TEXT = 'primary';
    const SELECTED = true;

    /**
     * @var int|null
     */
    private $status;

    public function __construct(int $status = null)
    {
        $this->status = $status;
    }

    /**
     * @return array
     */
    public function init(): array
    {
        return $this->status == self::ACTIVE
            ? ['name' => self::ACTIVE_TEXT, 'class' => self::PRIMARY_CLASS_TEXT]
            : ['name' => self::PASSIVE_TEXT, 'class' => self::DANGERS_CLASS_TEXT];
    }

    /**
     * @return array[]
     */
    public function show(): array
    {
        return [
            1 => [
                'name' => self::ACTIVE_TEXT,
                'id' => self::ACTIVE, 'selected' => $this->status === self::ACTIVE ?? false
            ],
            2 => [
                'name' => self::PASSIVE_TEXT,
                'id' => self::PASSIVE, 'selected' => $this->status === self::PASSIVE ?? false
            ]
        ];
    }
}
