<?php

namespace App\Helpers;

use App\Classes\Tools\StatusChecker;
use Illuminate\Http\Request;
use Stevebauman\Purify\Purify;

function getIcons()
{
    return ["star", "marker", "bubble", "chevron-right", "right-arrow", "left-arrow", "support", "question", "out", "in", "image", "heart",
        "dots", "door", "bed", "bag", "bathroom", "building", "buildings", "cabinet", "car", "check", "confirm", "cup", "cutlery", "design", "pacman"
        , "obstacle", "marker-2", "smile", "secure", "key", "racket", "plane", "phone", "guest", "pet", "park", "panda", "devices", "wifi", "slider",
        "chevron-up", "chevron-down", "exit", "chevron-left", "female", "male"];
}

/**
 * @param int $status
 * @return array[]
 */
function getCheckStatus(int $status = 0): array
{
    $handle = new StatusChecker($status);

    return $handle->init();
}

/**
 * * Translatable fields return json text
 *
 * @param array $translatableFields
 * @param Request $request
 * @return array
 */
function translatableData(array $translatableFields, Request $request): array
{
    $data = [];

    $languages = config('app.available_locales');

    foreach ($languages as $language) {
        foreach ($translatableFields as $i => $val) {
            $data[$val][$language] = $request->{$val . '_' . $language};
        }
    }

    return $data;
}



class GeneralHelper
{
    /**
     * @param Request $request
     * @param array|null $config
     * @return array
     */
    public function clean(Request $request, ?array $config = null): array
    {
        $cleanData = $request->all();

        $this->cleanWithPurify($cleanData, $config);

        return $cleanData;
    }

    /**
     * @param $cleanData
     * @param array|null $config
     */
    private function cleanWithPurify(&$cleanData, ?array $config = null)
    {
        $from_entity = array("/&lt;/", "/&gt;/");
        $from_change = array("/</", "/>/");
        $to_change = array("<", ">");
        $to_entity = array("&lt;", "&gt;");

        if (!is_array($cleanData)) {
            $cleanData = preg_replace($from_entity, $to_change, $cleanData);
            $cleanData = html_entity_decode($cleanData);

            if (is_array($config)) {
                $cleanData = (new Purify())->clean($cleanData, $config);
            } else {
                $cleanData = (new Purify())->clean($cleanData);
            }

            $cleanData = htmlspecialchars_decode(trim($cleanData));
            $cleanData = preg_replace($from_change, $to_entity, $cleanData);

            return;
        }

        /*if we have an array we call ourselves for each element*/
        foreach ($cleanData as $key => &$element) {
            if ($key === 'files') {
                return;
            }

            $this->cleanWithPurify($element, $config);
        }
    }


}
