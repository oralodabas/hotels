<?php


namespace App\Exceptions;


interface ExceptionInterface
{
    /**
     * @return int
     */
    public function getCode();

    /**
     * @return string
     */
    public function getMessage();

    /**
     * @return array|bool|float|int|mixed|object|string|null
     */
    public function getData();
}
