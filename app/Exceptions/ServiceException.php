<?php


namespace App\Exceptions;

use Illuminate\Support\Facades\Log;
use Throwable;

class ServiceException extends AbstractException implements ExceptionInterface
{

    /**
     * EmailError constructor.
     * @param string|null $message
     * @param string|null $code
     * @param array $data
     * @param Throwable|null $previous
     */
    public function __construct(?string $message = "", $data = [], ?string $code = "",
                                ?Throwable $previous = null)
    {
        parent::__construct($message ?? 'Service Exception', $code, $previous);
        Log::channel('service')->warning($message, $data);
    }


}
