<?php


namespace App\Exceptions;

use Illuminate\Support\Facades\Log;
use Throwable;

class RepositoryException extends AbstractException implements ExceptionInterface
{

    /**
     * EmailError constructor.
     * @param string|null $message
     * @param string|null $code
     * @param array $data
     * @param Throwable|null $previous
     */
    public function __construct(?string $message = "", $data = [], ?string $code = "",
                                ?Throwable $previous = null)
    {
        parent::__construct($message ?? 'Repository Exception', $code, $previous);
        $this->data = $data;

        Log::channel('repository')->warning($message, $data);
    }


}
