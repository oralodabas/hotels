<?php


namespace App\Exceptions;

use Exception;
use Throwable;

abstract class AbstractException extends Exception implements ExceptionInterface
{

    protected $data;

    public function __construct($message = "", $data = [], $code = 0, ?Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
        $this->data = $data;

    }

    public function getData()
    {
        return $this->data;
    }
}
