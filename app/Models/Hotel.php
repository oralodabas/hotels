<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Spatie\Translatable\HasTranslations;

class Hotel extends GeneralModel
{
    use HasFactory;
    use HasTranslations;

    public const STATUS_ACTIVE = 'active';
    public const STATUS_PASSIVE = 'passive';

    protected $fillable = [
        'name',
        'overview',
        'slug',
        'google_map_url',
        'domain_name',
        'stars',
        'city_id',
        'lat',
        'lng',
        'curl_url',
        'check_in_from',
        'check_in_until',
        'check_out',
        'logo_name',
        'status',
        'is_deleted'
    ];

    protected $casts = [
      'slug'=>'json',
      'name'=>'json',
      'overview'=>'json',
    ];

    public $translatable = [
        'name', 'overview','slug'
    ];

    /**
     * @return BelongsTo
     */
    public function city(): BelongsTo
    {
        return $this->belongsTo(City::class);
    }

    /**
     * @return BelongsToMany
     */
    public function images(): BelongsToMany
    {
        return $this->belongsToMany(Image::class)->withPivot('flag');
    }

    public function rooms()
    {
        return $this->belongsToMany(Room::class);
    }

    public function facilities()
    {
        return $this->belongsToMany(Facility::class);
    }

    public function reviews()
    {
        return $this->hasMany(Review::class);
    }

    public function faqs()
    {
        return $this->hasMany(FrequentlyAskedQuestion::class);
    }
}
