<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

abstract class GeneralModel extends Model
{
    /**
     * set fillable fields
     */
    protected $fillable = [];

    /**
     * set string fields for filtering
     * @var array
     */
    protected $likeFilterFields = [];

    /**
     * set boolean fields for filtering
     * @var array
     */
    protected $boolFilterFields = [];

    /**
     * add filtering
     *
     * @param $builder  : query builder
     * @param  array|null  $filters  : array of filters
     * @return mixed
     */
    public function scopeFilter($builder, ?array $filters = [])
    {
        if (!$filters) {
            return $builder;
        }

        $tableName = $this->getTable();

        $defaultFillableFields = $this->fillable;

        foreach ($filters as $field => $value) {
            if ($value === 'all') {
                $value = null;
            }

            if (in_array($field, $this->boolFilterFields) && $value != null) {
                $builder->where($field, $value);

                continue;
            } elseif (in_array($field, $this->boolFilterFields) && $value === 0) {
                $builder->where($field, $value);

                continue;
            }

            if (!in_array($field, $defaultFillableFields) || !$value) {
                continue;
            }

            if (in_array($field, $this->likeFilterFields)) {
                $builder->where($tableName.'.'.$field, 'iLIKE', '%'.trim($value).'%');
            } elseif (is_array($value)) {
                $builder->whereIn($field, $value);
            } else {
                $builder->where($field, $value);
            }
        }

        return $builder;
    }

    /**
     * @param $query
     * @param $status
     * @return mixed
     */
    public function scopeStatus($query, $status)
    {
        return $query->where('status', $status);
    }

    /**
     * @param $query
     * @param $status
     * @return mixed
     */
    public function scopeNotStatus($query, $status)
    {
        return $query->whereNot('status', $status);
    }
}
