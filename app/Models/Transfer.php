<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Translatable\HasTranslations;

class Transfer extends Model
{

    protected $fillable = [
        'id', 'hotel_id', 'airport_id','vehicle_id','price'
    ];

}
