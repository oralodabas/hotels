<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class Commission extends GeneralModel
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = [
        'hotel_id',
        'type',
        'amount',
        'status'
    ];
}
