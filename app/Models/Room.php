<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Spatie\Translatable\HasTranslations;

class Room extends GeneralModel
{
    use HasFactory;
    use HasTranslations;

    public $timestamps = false;

    protected $fillable = [
        'title',
        'detail',
        'status'
    ];

    public $translatable = [
        'title','detail'
    ];

    /**
     * @return BelongsToMany
     */
    public function images(): BelongsToMany
    {
        return $this->belongsToMany(Image::class)->withPivot('flag');;
    }

    public function hotels()
    {
        return $this->belongsToMany(Hotel::class)->withPivot('quantity');;
    }
}
