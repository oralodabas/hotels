<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Review extends Model
{

    public const STATUS_ACTIVE = 'active';
    public const STATUS_PASSIVE = 'passive';

    protected $fillable = [
        'id', 'info', 'description','point','hotel_id', 'status'
    ];

    public function hotel()
    {
        return $this->belongsTo(Hotel::class);
    }



}
