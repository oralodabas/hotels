<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Translatable\HasTranslations;

class Vehicle extends Model
{
    use HasTranslations;

    protected $fillable = [
        'id', 'name', 'capacity'
    ];

    public $translatable = [
        'name'
    ];

}
