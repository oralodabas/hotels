<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\Translatable\HasTranslations;

class SeoRoute extends GeneralModel
{
    use HasFactory;
    use HasTranslations;

    public $timestamps = false;

    protected $fillable = [
        'parent_id',
        'parent_type',
        'path',
        'title',
        'description',
        'keywords',
        'detail',
        'status'
    ];

    public $translatable = [
        'title',
        'description',
        'keywords',
        'detail'
    ];
}
