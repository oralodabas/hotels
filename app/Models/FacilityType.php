<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\Translatable\HasTranslations;

class FacilityType extends GeneralModel
{
    use HasFactory;
    use HasTranslations;

    public $timestamps = false;

    protected $fillable = [
        'name',
        'icon_name',
        'status'
    ];

    public $translatable = [
        'name'
    ];

    public function facilities()
    {
        return $this->hasMany(Facility::class);
    }
}
