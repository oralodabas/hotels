<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\Translatable\HasTranslations;

class NearByType extends GeneralModel
{
    use HasFactory;
    use HasTranslations;

    public $timestamps = false;

    protected $fillable = [
        'title',
        'type',
        'status'
    ];

    public $translatable = [
        'title',
        'type'
    ];
}
