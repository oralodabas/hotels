<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Facility extends Model
{
    use HasFactory;
    use HasTranslations;

    public $timestamps = false;

    protected $fillable = [
        'name',
        'facility_type_id'
    ];

    public $translatable = [
        'name'
    ];

    public function facilityType()
    {
        return $this->belongsTo(FacilityType::class);
    }
}
