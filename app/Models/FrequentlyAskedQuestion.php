<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\Translatable\HasTranslations;

class FrequentlyAskedQuestion extends GeneralModel
{
    use HasFactory;
    use HasTranslations;

    public $timestamps = false;

    protected $fillable = [
        'questions',
        'answers',
        'hotel_id',
        'status'
    ];

    public $translatable = [
        'questions','answers'
    ];


}
