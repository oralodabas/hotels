<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class HotelProperty extends GeneralModel
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = [
        'hotel_id',
        'property_id',
        'property_type',
        'property_value'
    ];
}
