<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class Language extends GeneralModel
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = [
        'key',
        'title',
        'status'
    ];
}
