<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\Translatable\HasTranslations;

class RoomAmenity extends GeneralModel
{
    use HasFactory;
    use HasTranslations;

    public $timestamps = false;

    protected $fillable = [
        'title',
        'parent_id',
        'value_type',
        'status'
    ];

    public $translatable = [
        'title',
        'value_type'
    ];
}
