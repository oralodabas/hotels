<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\Translatable\HasTranslations;

class File extends GeneralModel
{
    use HasFactory;
    use HasTranslations;

    public $timestamps = false;

    protected $fillable = [
        'title',
        'parent_id',
        'parent_type',
        'path',
        'type',
        'file_type'
    ];

    public $translatable = [
        'title'
    ];
}
