<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class HotelCommentLike extends GeneralModel
{
    use HasFactory;

    protected $fillable = [
        'hotel_comment_id',
        'type',
        'ip'
    ];
}
