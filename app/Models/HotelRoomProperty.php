<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class HotelRoomProperty extends GeneralModel
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = [
        'hotel_room_id',
        'room_amenity_id',
        'property_value'
    ];
}
