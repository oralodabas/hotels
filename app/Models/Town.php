<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class Town extends GeneralModel
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = [
        'title',
        'district_id',
        'status'
    ];
}
