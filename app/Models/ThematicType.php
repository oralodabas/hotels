<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\Translatable\HasTranslations;

class ThematicType extends GeneralModel
{
    use HasFactory;
    use HasTranslations;

    public $timestamps = false;

    protected $fillable = [
        'title',
        'status'
    ];

    public $translatable = [
        'title'
    ];
}
