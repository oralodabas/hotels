<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\Translatable\HasTranslations;

class HotelComment extends GeneralModel
{
    use HasFactory;
    use HasTranslations;

    protected $fillable = [
        'hotel_id',
        'name',
        'email',
        'country',
        'travel_month',
        'travel_year',
        'rating',
        'comment_plus',
        'comment_minus',
        'status'
    ];

    public $translatable = [
        'comment_plus',
        'comment_minus'
    ];
}
