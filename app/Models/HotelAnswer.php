<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\Translatable\HasTranslations;

class HotelAnswer extends GeneralModel
{
    use HasFactory;
    use HasTranslations;

    public $timestamps = false;

    protected $fillable = [
        'hotel_id',
        'faq_id',
        'answer',
        'status'
    ];

    public $translatable = [
        'answer'
    ];
}
