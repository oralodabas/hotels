<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\Translatable\HasTranslations;

class HotelRoom extends GeneralModel
{
    use HasFactory;
    use HasTranslations;

    public $timestamps = false;

    protected $fillable = [
        'hotel_id',
        'room_id',
        'quantity',
        'detail'
    ];

    public $translatable = [
        'detail'
    ];
}
