<?php

namespace App\Repositories;

use App\Models\HotelRoom;

class HotelRoomRepository
{
    /**
     * @var HotelRoom
     */
    protected $hotelRoom;

    /**
     * @var int
     */
    protected $perPage = 25;

    /**
     * Repository constructor.
     */
    public function __construct()
    {
        $this->hotelRoom = new HotelRoom();
    }

    /**
     * @param  array  $data
     * @return mixed
     */
    public function index(array $data)
    {
        return $this->hotelRoom->filter($data)->paginate($this->perPage);
    }

    /**
     * @param  int  $id
     * @return mixed
     */
    public function show(int $id)
    {
        return $this->hotelRoom->findOrFail($id);
    }

    /**
     * @param  array  $data
     * @return mixed
     */
    public function store(array $data)
    {
        return $this->hotelRoom->create($data);
    }

    /**
     * @param  array  $data
     * @param  int  $id
     * @return mixed
     */
    public function update(array $data, int $id)
    {
        $hotel = $this->hotelRoom->findOrFail($id);

        $hotel->fill($data);

        $hotel->save();

        return $hotel;
    }

    /**
     * @param  int  $id
     * @return mixed
     */
    public function destroy(int $id)
    {
        $hotel = $this->hotelRoom->findOrFail($id);

        $hotel->delete();

        return $hotel;
    }
}
