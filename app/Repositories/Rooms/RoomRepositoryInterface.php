<?php


namespace App\Repositories\Rooms;


use App\Repositories\BaseRepositoryInterface;

interface RoomRepositoryInterface extends BaseRepositoryInterface
{

    public function getTranslatable();
}
