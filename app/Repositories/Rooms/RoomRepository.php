<?php


namespace App\Repositories\Rooms;


use App\Models\Room;
use App\Repositories\AbstractRepository;

class RoomRepository extends AbstractRepository implements RoomRepositoryInterface
{

    public function __construct(Room $entity)
    {
        parent::__construct($entity);
    }

    /**
     * @return array
     */
    public function getTranslatable() : array
    {
        return $this->entity->getTranslatableAttributes();
    }
}
