<?php


namespace App\Repositories\Faq;


use App\Repositories\BaseRepositoryInterface;

interface FaqInterfaceRepository extends BaseRepositoryInterface
{

    public function getTranslatable(): array;
    public function deleteFaqForHotelId(int $id);
}
