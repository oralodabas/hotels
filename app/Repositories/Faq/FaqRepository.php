<?php


namespace App\Repositories\Faq;


use App\Models\FrequentlyAskedQuestion;
use App\Repositories\AbstractRepository;

class FaqRepository extends AbstractRepository implements FaqInterfaceRepository
{

    public function __construct(FrequentlyAskedQuestion $entity)
    {
        parent::__construct($entity);
    }
    /**
     * @return array
     */
    public function getTranslatable(): array
    {
        return $this->entity->getTranslatableAttributes();
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function deleteFaqForHotelId(int $id)
    {
       return $this->entity->where('hotel_id',$id)->delete();
    }
}
