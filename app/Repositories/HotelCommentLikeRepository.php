<?php

namespace App\Repositories;

use App\Models\HotelCommentLike;

class HotelCommentLikeRepository
{
    /**
     * @var HotelCommentLike
     */
    protected $hotelCommentLike;

    /**
     * @var int
     */
    protected $perPage = 25;

    /**
     * Repository constructor.
     */
    public function __construct()
    {
        $this->hotelCommentLike = new HotelCommentLike();
    }

    /**
     * @param  array  $data
     * @return mixed
     */
    public function index(array $data)
    {
        return $this->hotelCommentLike->filter($data)->paginate($this->perPage);
    }

    /**
     * @param  int  $id
     * @return mixed
     */
    public function show(int $id)
    {
        return $this->hotelCommentLike->findOrFail($id);
    }

    /**
     * @param  array  $data
     * @return mixed
     */
    public function store(array $data)
    {
        return $this->hotelCommentLike->create($data);
    }

    /**
     * @param  array  $data
     * @param  int  $id
     * @return mixed
     */
    public function update(array $data, int $id)
    {
        $hotel = $this->hotelCommentLike->findOrFail($id);

        $hotel->fill($data);

        $hotel->save();

        return $hotel;
    }

    /**
     * @param  int  $id
     * @return mixed
     */
    public function destroy(int $id)
    {
        $hotel = $this->hotelCommentLike->findOrFail($id);

        $hotel->delete();

        return $hotel;
    }
}
