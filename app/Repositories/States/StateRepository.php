<?php


namespace App\Repositories\States;


use App\Models\State;
use App\Repositories\AbstractRepository;

class StateRepository extends AbstractRepository implements StateRepositoryInterface
{
    public function __construct(State $entity)
    {
        parent::__construct($entity);
    }
}
