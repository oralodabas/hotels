<?php

namespace App\Repositories;

use App\Models\Hotel;

class HotelRepository
{
    /**
     * @var Hotel
     */
    protected $hotel;

    /**
     * @var int
     */
    protected $perPage = 25;

    /**
     * Repository constructor.
     */
    public function __construct()
    {
        $this->hotel = new Hotel();
    }

    /**
     * @param  array  $data
     * @return mixed
     */
    public function index(array $data)
    {
        return $this->hotel->filter($data)->paginate($this->perPage);
    }

    /**
     * @param  int  $id
     * @return mixed
     */
    public function show(int $id)
    {
        return $this->hotel->findOrFail($id);
    }

    /**
     * @param  array  $data
     * @return mixed
     */
    public function store(array $data)
    {
        return $this->hotel->create($data);
    }

    /**
     * @param  array  $data
     * @param  int  $id
     * @return mixed
     */
    public function update(array $data, int $id)
    {
        $hotel = $this->hotel->findOrFail($id);

        $hotel->fill($data);

        $hotel->save();

        return $hotel;
    }

    /**
     * @param  int  $id
     * @return mixed
     */
    public function destroy(int $id)
    {
        $hotel = $this->hotel->findOrFail($id);

        $hotel->delete();

        return $hotel;
    }
}
