<?php


namespace App\Repositories\Images;


use App\Exceptions\RepositoryException;
use App\Models\Image;
use App\Repositories\AbstractRepository;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ImageRepository extends AbstractRepository implements ImageRepositoryInterface
{

    public function __construct(Image $entity)
    {
        parent::__construct($entity);
    }

    /**
     * @param Request $request
     * @return array
     * @throws RepositoryException
     */
    public function uploadImageOrCreate(Request $request)
    {
        try {
            $images = $request->file('files');

            $ids = [];
            $data = [];

            foreach ($images as $image) {

                $date = Carbon::now()->format('y-m-d');
                $hashName = sprintf(Str::random(8) . '.%s', $image->extension());
                $destinationPath = sprintf("uploads/hotels/%s/", $date);
                $image->storeAs($destinationPath, $hashName, 'public');


                $data = [
                    'title' => $request->image_title,
                    'name' => $hashName,
                    'path' => $destinationPath,
                ];

                $insert =  $this->create($data);

                $ids[] = $insert->id;
            }

            return $ids;

        } catch (Exception $e) {
            throw new RepositoryException($e->getMessage(), $data);
        }
    }
}
