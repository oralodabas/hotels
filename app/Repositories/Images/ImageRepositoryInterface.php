<?php


namespace App\Repositories\Images;


use App\Repositories\BaseRepositoryInterface;
use Illuminate\Http\Request;

interface ImageRepositoryInterface extends BaseRepositoryInterface
{
    public function uploadImageOrCreate(Request $request);
}
