<?php


namespace App\Repositories\Images\Filters;


use App\Repositories\AbstractFilter;
use App\Repositories\FilterInterface;

class Title extends AbstractFilter implements FilterInterface
{
    public function handle($value)
    {
        return $this->query->where('title','LIKE' ,"%$value%");
    }
}
