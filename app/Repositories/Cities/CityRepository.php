<?php


namespace App\Repositories\Cities;


use App\Models\City;
use App\Repositories\AbstractRepository;

class CityRepository extends AbstractRepository implements CityRepositoryInterface
{

    public function __construct(City $entity)
    {
        parent::__construct($entity);
    }
}
