<?php


namespace App\Repositories\Airports;


use App\Models\Airport;
use App\Repositories\AbstractRepository;

class AirportRepository extends AbstractRepository implements AirportRepositoryInterface
{

    public function __construct(Airport $entity)
    {
        parent::__construct($entity);
    }

    /**
     * @return array
     */
    public function getTranslatable() : array
    {
        return $this->entity->getTranslatableAttributes();
    }
}
