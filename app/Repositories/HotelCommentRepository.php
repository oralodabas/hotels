<?php

namespace App\Repositories;

use App\Models\HotelComment;

class HotelCommentRepository
{
    /**
     * @var HotelComment
     */
    protected $hotelComment;

    /**
     * @var int
     */
    protected $perPage = 25;

    /**
     * Repository constructor.
     */
    public function __construct()
    {
        $this->hotelComment = new HotelComment();
    }

    /**
     * @param  array  $data
     * @return mixed
     */
    public function index(array $data)
    {
        return $this->hotelComment->filter($data)->paginate($this->perPage);
    }

    /**
     * @param  int  $id
     * @return mixed
     */
    public function show(int $id)
    {
        return $this->hotelComment->findOrFail($id);
    }

    /**
     * @param  array  $data
     * @return mixed
     */
    public function store(array $data)
    {
        return $this->hotelComment->create($data);
    }

    /**
     * @param  array  $data
     * @param  int  $id
     * @return mixed
     */
    public function update(array $data, int $id)
    {
        $hotel = $this->hotelComment->findOrFail($id);

        $hotel->fill($data);

        $hotel->save();

        return $hotel;
    }

    /**
     * @param  int  $id
     * @return mixed
     */
    public function destroy(int $id)
    {
        $hotel = $this->hotelComment->findOrFail($id);

        $hotel->delete();

        return $hotel;
    }
}
