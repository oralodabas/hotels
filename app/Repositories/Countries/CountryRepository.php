<?php


namespace App\Repositories\Countries;


use App\Models\Country;
use App\Repositories\AbstractRepository;

class CountryRepository extends AbstractRepository implements CountryRepositoryInterface
{

    public function __construct(Country $entity)
    {
        parent::__construct($entity);
    }
}
