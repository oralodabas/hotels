<?php


namespace App\Repositories\Facilities;


use App\Repositories\BaseRepositoryInterface;

interface FacilityRepositoryInterface extends BaseRepositoryInterface
{
    public function getTranslatable();
}
