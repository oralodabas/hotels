<?php


namespace App\Repositories\Facilities;


use App\Models\Facility;
use App\Repositories\AbstractRepository;

class FacilityRepository extends AbstractRepository implements FacilityRepositoryInterface
{

    public function __construct(Facility $entity)
    {
        parent::__construct($entity);
    }

    /**
     * @return array
     */
    public function getTranslatable() : array
    {
        return $this->entity->getTranslatableAttributes();
    }
}
