<?php


namespace App\Repositories;


use App\Exceptions\RepositoryException;
use App\Classes\Database\Filterable;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Request;
use App\Exceptions\ModelNotFoundException;

abstract class AbstractRepository implements BaseRepositoryInterface
{
    /**
     * @var mixed
     */
    protected $entity;

    public function __construct($entity)
    {
        $this->entity = $entity;
    }


    /**
     * @param null $paginate
     * @return mixed
     * @throws \ReflectionException
     */
    public function all($paginate = null)
    {
        return $this->processPagination($this->entity, $paginate);
    }

    /**
     * @param $id
     * @return mixed
     * @throws ModelNotFoundException
     */
    public function find($id)
    {
        $model = $this->entity->find($id);

        if (!$model) {
            throw new ModelNotFoundException(
                'Not Found Find ' . get_class($this->entity->getModel()) . ' id:' . $id,
                ['model' => get_class($this->entity->getModel()), 'id' => $id ?? 0]);
        }

        return $model;
    }

    /**
     * @param $column
     * @param $value
     * @param null $paginate
     * @param string $orderBy
     * @return mixed
     * @throws \ReflectionException
     */
    public function findWhere($column, $value, $paginate = null, $orderBy = ['id','DESC'])
    {
        $query = $this->entity->where($column, $value);

        $query->orderBy($orderBy[0],$orderBy[1]);

        return $this->processPagination($query, $paginate);
    }

    /**
     * @param array $data
     * @param null $paginate
     * @return mixed
     * @throws \ReflectionException
     */
    public function findWhereOrWhere(array $data, $paginate = null)
    {
        $query = $this->entity->where(function ($query) use ($data) {
            $index = 0;
            foreach ($data as $column => $value) {
                $clause = $index == 0 ? 'where' : 'orWhere';
                $query->$clause($column, $value);
                $index++;
            }
        });

        return $this->processPagination($query, $paginate);
    }

    /**
     * @param array $data
     * @param null $paginate
     * @return mixed
     */
    public function findWhereAndWhere(array $data, $paginate = null)
    {
        $query = $this->entity->where(function ($query) use ($data) {
            foreach ($data as $column => $value) {
                $query->where($column, $value);
            }
        });


        return $this->processPagination($query, $paginate);
    }

    /**
     * @param $column
     * @param $value
     * @return mixed
     */
    public function findWhereFirst($column, $value)
    {
        $model = $this->entity->where($column, $value)->first();

        if (!$model) {
            throw new ModelNotFoundException(
                'Not Found Find Where Data ' . get_class($this->entity->getModel()) .
                ' column:' . $column,
                [
                    'model' => get_class($this->entity->getModel()),
                    'column' => $column, 'value' => $value
                ]);
        }

        return $model;
    }

    /**
     * @param $columns
     * @param $value
     * @param null $paginate
     * @return mixed
     * @throws \ReflectionException
     */
    public function findWhereLike($columns, $value, $paginate = null)
    {
        $query = $this->entity;

        if (is_string($columns)) {
            $columns = [$columns];
        }

        foreach ($columns as $column) {
            $query->orWhere($column, 'like', $value);
        }

        return $this->processPagination($query, $paginate);
    }

    /**
     * @param $perPage
     * @return mixed
     */
    public function paginate($perPage = 10)
    {
        return $this->entity->paginate($perPage);
    }

    /**
     * @param array $properties
     * @return mixed
     * @throws RepositoryException
     */
    public function create(array $properties)
    {
        try {
            return $this->entity->create($properties);

        } catch (\Exception $e) {

            throw new RepositoryException($e->getMessage(), $properties);
        }
    }

    /**
     * @param $id
     * @param array $properties
     * @return mixed
     */
    public function update($id, array $properties)
    {
        $model = $this->find($id);
        $model->update($properties);

        return $model;
    }


    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        return $this->find($id)->delete();
    }

    /**
     * @param $criteria
     * @return mixed
     */
    public function withCriteria(...$criteria)
    {
        $criteria = Arr::flatten($criteria);

        foreach ($criteria as $criterion) {
            $this->entity = $criterion->apply($this->entity);
        }

        return $this;
    }


    /**
     * @param $query
     * @param $paginate
     * @return mixed
     * @throws \ReflectionException
     */
    private function processPagination($query, $paginate)
    {
        try {
            $query = $this->filterBy($query);
            $query = $query->orderByDesc('id');

            return $paginate ? $query->paginate($paginate) : $query->get();

        } catch (\Exception $e) {
            throw new RepositoryException($e->getMessage());
        }

    }

    /**
     * @param $query
     * @return mixed
     * @throws \ReflectionException
     */
    public function filterBy($query)
    {
        $reflector = new \ReflectionClass(get_class($this));
        $class = $reflector->getNamespaceName() . "\\Filters";
        $filters = Request::all();

        $filter = new Filterable($query, $filters, $class);

        return $filter->init();

    }

    /**
     * @param string $column
     * @param array $ids
     * @param null $paginate
     * @return mixed
     * @throws \ReflectionException
     */
    public function whereIn(string $column, array $ids, $paginate = null)
    {
        $query = $this->entity->wherein($column, $ids);

        return $this->processPagination($query, $paginate);
    }

    /**
     * @param array $properties
     * @return mixed
     * @throws RepositoryException
     */
    public function updateOrCreate(array $properties)
    {
        try {
            return $this->entity->updateOrCreate($properties);

        } catch (\Exception $e) {

            throw new RepositoryException($e->getMessage(), $properties);
        }
    }

    /**
     * @return array
     */
    public function getTranslatable(): array
    {
        return $this->entity->getTranslatableAttributes();
    }
}
