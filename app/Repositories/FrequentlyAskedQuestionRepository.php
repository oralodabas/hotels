<?php

namespace App\Repositories;

use App\Models\FrequentlyAskedQuestion;

class FrequentlyAskedQuestionRepository
{
    /**
     * @var FrequentlyAskedQuestion
     */
    protected $frequentlyAskedQuestion;

    /**
     * @var int
     */
    protected $perPage = 25;

    /**
     * Repository constructor.
     */
    public function __construct()
    {
        $this->frequentlyAskedQuestion = new FrequentlyAskedQuestion();
    }

    /**
     * @param  array  $data
     * @return mixed
     */
    public function index(array $data)
    {
        return $this->frequentlyAskedQuestion->filter($data)->paginate($this->perPage);
    }

    /**
     * @param  int  $id
     * @return mixed
     */
    public function show(int $id)
    {
        return $this->frequentlyAskedQuestion->findOrFail($id);
    }

    /**
     * @param  array  $data
     * @return mixed
     */
    public function store(array $data)
    {
        return $this->frequentlyAskedQuestion->create($data);
    }

    /**
     * @param  array  $data
     * @param  int  $id
     * @return mixed
     */
    public function update(array $data, int $id)
    {
        $hotel = $this->frequentlyAskedQuestion->findOrFail($id);

        $hotel->fill($data);

        $hotel->save();

        return $hotel;
    }

    /**
     * @param  int  $id
     * @return mixed
     */
    public function destroy(int $id)
    {
        $hotel = $this->frequentlyAskedQuestion->findOrFail($id);

        $hotel->delete();

        return $hotel;
    }
}
