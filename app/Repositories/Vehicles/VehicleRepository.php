<?php


namespace App\Repositories\Vehicles;


use App\Models\Vehicle;
use App\Repositories\AbstractRepository;

class VehicleRepository extends AbstractRepository implements VehicleRepositoryInterface
{

    public function __construct(Vehicle $entity)
    {
        parent::__construct($entity);
    }


    /**
     * @return array
     */
    public function getTranslatable() : array
    {
        return $this->entity->getTranslatableAttributes();
    }

}
