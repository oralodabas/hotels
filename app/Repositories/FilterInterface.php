<?php


namespace App\Repositories;


interface FilterInterface
{
    public function handle($value);

}
