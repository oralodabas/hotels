<?php

namespace App\Repositories;

use App\Models\RoomAmenity;

class RoomAmenityRepository
{
    /**
     * @var RoomAmenity
     */
    protected $roomAmenity;

    /**
     * @var int
     */
    protected $perPage = 25;

    /**
     * Repository constructor.
     */
    public function __construct()
    {
        $this->roomAmenity = new RoomAmenity();
    }

    /**
     * @param  array  $data
     * @return mixed
     */
    public function index(array $data)
    {
        return $this->roomAmenity->filter($data)->paginate($this->perPage);
    }

    /**
     * @param  int  $id
     * @return mixed
     */
    public function show(int $id)
    {
        return $this->roomAmenity->findOrFail($id);
    }

    /**
     * @param  array  $data
     * @return mixed
     */
    public function store(array $data)
    {
        return $this->roomAmenity->create($data);
    }

    /**
     * @param  array  $data
     * @param  int  $id
     * @return mixed
     */
    public function update(array $data, int $id)
    {
        $hotel = $this->roomAmenity->findOrFail($id);

        $hotel->fill($data);

        $hotel->save();

        return $hotel;
    }

    /**
     * @param  int  $id
     * @return mixed
     */
    public function destroy(int $id)
    {
        $hotel = $this->roomAmenity->findOrFail($id);

        $hotel->delete();

        return $hotel;
    }
}
