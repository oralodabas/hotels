<?php


namespace App\Repositories;


use Illuminate\Support\Collection;

interface BaseRepositoryInterface
{
    public function all($paginate = null);

    public function find($id);

    public function findWhere($column, $value, $paginate = null, $orderBy = []);

    public function findWhereFirst($column, $value);

    public function findWhereOrWhere(array $data, $paginate = null);

    public function findWhereAndWhere(array $data, $paginate = null);

    public function whereIn(string $column, array $ids, $paginate = null);

    /**
     * @param string $column
     * @param string|array $value
     * @param int $paginate
     * @return Collection
     */
    public function findWhereLike(string $column, $value, $paginate = 0);

    public function paginate($perPage = 10);

    public function create(array $properties);

    public function updateOrCreate(array $properties);

    public function update($id, array $properties);

    public function delete($id);

    public function withCriteria(...$criteria);

}
