<?php


namespace App\Repositories\FacilityType;


use App\Models\FacilityType;
use App\Repositories\AbstractRepository;

class FacilityTypeRepository extends AbstractRepository implements FacilityTypeRepositoryInterface
{

    public function __construct(FacilityType $entity)
    {
        parent::__construct($entity);
    }
}
