<?php


namespace App\Repositories\FacilityType;


use App\Repositories\BaseRepositoryInterface;

interface FacilityTypeRepositoryInterface extends BaseRepositoryInterface
{

    public function getTranslatable();
}
