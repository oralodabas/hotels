<?php


namespace App\Repositories\Hotels;


use App\Exceptions\RepositoryException;
use App\Models\Hotel;
use App\Repositories\AbstractRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use phpDocumentor\Reflection\Utils;

class HotelRepository extends AbstractRepository implements HotelRepositoryInterface
{
    public function __construct(Hotel $entity)
    {
        parent::__construct($entity);
    }

    /**
     * @return array
     */
    public function getTranslatable(): array
    {
        return $this->entity->getTranslatableAttributes();
    }

    /**
     * @param Request $request
     * @throws RepositoryException
     */
    public function uploadLogo(Request $request): void
    {
        try {
            $image = $request->file('logo');

            $hashName = sprintf(Str::random(8) . '.%s', $image->extension());
            $destinationPath = "uploads/hotels/logo/";
            $image->storeAs($destinationPath, $hashName, 'public');

            $request->merge(['logo_name' => $hashName]);

        } catch (\Exception $e) {
            throw new RepositoryException($e->getMessage());
        }

    }

    public function getHotels()
    {

        $name = 'name->'.app()->getLocale();
        $query = $this->entity->where('status','active');
        $query->whereNotNull($name);

        return $query->get();
    }
}
