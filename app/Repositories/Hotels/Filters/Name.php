<?php


namespace App\Repositories\Hotels\Filters;


use App\Repositories\AbstractFilter;
use App\Repositories\FilterInterface;

class Name extends AbstractFilter implements FilterInterface
{
    public function handle($value)
    {
        return $this->query->where('name->tr','LIKE' ,"%$value%");
    }
}
