<?php


namespace App\Repositories\Hotels;


use App\Repositories\BaseRepositoryInterface;
use Illuminate\Http\Request;

interface HotelRepositoryInterface extends BaseRepositoryInterface
{
    public function getTranslatable(): array;
    public function uploadLogo(Request $request): void;
}
