<?php

namespace App\Repositories;

use App\Models\City;

class CityRepository
{
    /**
     * @var City
     */
    protected $city;

    /**
     * @var int
     */
    protected $perPage = 25;

    /**
     * Repository constructor.
     */
    public function __construct()
    {
        $this->city = new City();
    }

    /**
     * @param  array  $data
     * @return mixed
     */
    public function index(array $data)
    {
        return $this->city->filter($data)->paginate($this->perPage);
    }

    /**
     * @param  int  $id
     * @return mixed
     */
    public function show(int $id)
    {
        return $this->city->findOrFail($id);
    }

    /**
     * @param  array  $data
     * @return mixed
     */
    public function store(array $data)
    {
        return $this->city->create($data);
    }

    /**
     * @param  array  $data
     * @param  int  $id
     * @return mixed
     */
    public function update(array $data, int $id)
    {
        $city = $this->city->findOrFail($id);

        $city->fill($data);

        $city->save();

        return $city;
    }

    /**
     * @param  int  $id
     * @return mixed
     */
    public function destroy(int $id)
    {
        $city = $this->city->findOrFail($id);

        $city->delete();

        return $city;
    }
}
