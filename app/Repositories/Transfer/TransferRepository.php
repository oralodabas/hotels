<?php


namespace App\Repositories\Transfer;


use App\Models\Transfer;
use App\Repositories\AbstractRepository;

class TransferRepository extends AbstractRepository implements TransferRepositoryInterface
{

    public function __construct(Transfer $entity)
    {
        parent::__construct($entity);
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function deleteTransferForHotelId(int $id)
    {
        $data = $this->findWhere('hotel_id',$id)->each(function ($transfer, $key) {
            $transfer->delete();
        });;

        return $this->entity->delete($data->toArray());
    }
}
