<?php


namespace App\Repositories\Transfer;


use App\Repositories\BaseRepositoryInterface;

interface TransferRepositoryInterface extends BaseRepositoryInterface
{

    public function deleteTransferForHotelId(int $id);
}
