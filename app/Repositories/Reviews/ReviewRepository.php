<?php


namespace App\Repositories\Reviews;


use App\Models\Review;
use App\Repositories\AbstractRepository;

class ReviewRepository extends AbstractRepository implements ReviewRepositoryInterface
{


    public function __construct(Review $entity)
    {
        parent::__construct($entity);
    }
}
