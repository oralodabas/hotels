<?php

namespace App\Providers;

//front
use App\Services\Admin\Airports\AirportService;
use App\Services\Admin\Airports\AirportServiceInterface;
use App\Services\Admin\Facilities\FacilityService;
use App\Services\Admin\Facilities\FacilityServiceInterface;
use App\Services\Admin\Reviews\ReviewService;
use App\Services\Admin\Reviews\ReviewServiceInterface;
use App\Services\Admin\Rooms\RoomService;
use App\Services\Admin\Rooms\RoomServiceInterface;
use App\Services\Admin\Vehicles\VehicleService;
use App\Services\Admin\Vehicles\VehicleServiceInterface;
use  App\Services\Frontend\Hotel\HotelServiceInterface as FrontHotelInterface;
use  App\Services\Frontend\Hotel\HotelService as FrontHotelService;

//admin
use App\Services\Admin\CountryStateCity\CountryStateCityService;
use App\Services\Admin\CountryStateCity\CountryStateCityServiceInterface;
use App\Services\Admin\DashboardService;
use App\Services\Admin\DashboardServiceInterface;
use App\Services\Admin\Hotels\HotelService;
use App\Services\Admin\Hotels\HotelServiceInterface;
use App\Services\Admin\Images\ImageService;
use App\Services\Admin\Images\ImageServiceInterface;
use Illuminate\Support\ServiceProvider;

class ServicesProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //Backend
        $this->app->bind(HotelServiceInterface::class, HotelService::class);
        $this->app->bind(DashboardServiceInterface::class, DashboardService::class);
        $this->app->bind(CountryStateCityServiceInterface::class, CountryStateCityService::class);
        $this->app->bind(ImageServiceInterface::class, ImageService::class);
        $this->app->bind(RoomServiceInterface::class, RoomService::class);
        $this->app->bind(FacilityServiceInterface::class, FacilityService::class);
        $this->app->bind(AirportServiceInterface::class, AirportService::class);
        $this->app->bind(VehicleServiceInterface::class, VehicleService::class);
        $this->app->bind(ReviewServiceInterface::class, ReviewService::class);

        //Frontend
        $this->app->bind(FrontHotelInterface::class, FrontHotelService::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
