<?php

namespace App\Providers;

use App\Repositories\Airports\AirportRepository;
use App\Repositories\Airports\AirportRepositoryInterface;
use App\Repositories\Cities\CityRepository;
use App\Repositories\Cities\CityRepositoryInterface;
use App\Repositories\Countries\CountryRepository;
use App\Repositories\Countries\CountryRepositoryInterface;
use App\Repositories\Facilities\FacilityRepository;
use App\Repositories\Facilities\FacilityRepositoryInterface;
use App\Repositories\FacilityType\FacilityTypeRepository;
use App\Repositories\FacilityType\FacilityTypeRepositoryInterface;
use App\Repositories\Faq\FaqInterfaceRepository;
use App\Repositories\Faq\FaqRepository;
use App\Repositories\Hotels\HotelRepository;
use App\Repositories\Hotels\HotelRepositoryInterface;
use App\Repositories\Images\ImageRepository;
use App\Repositories\Images\ImageRepositoryInterface;
use App\Repositories\Reviews\ReviewRepository;
use App\Repositories\Reviews\ReviewRepositoryInterface;
use App\Repositories\Rooms\RoomRepositoryInterface;
use App\Repositories\Rooms\RoomRepository;
use App\Repositories\States\StateRepository;
use App\Repositories\States\StateRepositoryInterface;
use App\Repositories\Transfer\TransferRepository;
use App\Repositories\Transfer\TransferRepositoryInterface;
use App\Repositories\Vehicles\VehicleRepository;
use App\Repositories\Vehicles\VehicleRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class RepositoryProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(HotelRepositoryInterface::class,HotelRepository::class);
        $this->app->bind(CountryRepositoryInterface::class,CountryRepository::class);
        $this->app->bind(StateRepositoryInterface::class,StateRepository::class);
        $this->app->bind(CityRepositoryInterface::class,CityRepository::class);
        $this->app->bind(ImageRepositoryInterface::class,ImageRepository::class);
        $this->app->bind(RoomRepositoryInterface::class,RoomRepository::class);
        $this->app->bind(FacilityTypeRepositoryInterface::class,FacilityTypeRepository::class);
        $this->app->bind(FacilityRepositoryInterface::class,FacilityRepository::class);
        $this->app->bind(AirportRepositoryInterface::class,AirportRepository::class);
        $this->app->bind(VehicleRepositoryInterface::class,VehicleRepository::class);
        $this->app->bind(TransferRepositoryInterface::class,TransferRepository::class);
        $this->app->bind(FaqInterfaceRepository::class,FaqRepository::class);
        $this->app->bind(ReviewRepositoryInterface::class,ReviewRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
