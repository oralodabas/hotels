<?php


namespace App\Services\Admin;


interface DashboardServiceInterface
{
    public const perPage = 50;
    public function index();
}
