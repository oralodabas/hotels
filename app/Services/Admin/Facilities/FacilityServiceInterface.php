<?php


namespace App\Services\Admin\Facilities;


use App\Services\BaseServiceInterface;
use Illuminate\Http\Request;

interface FacilityServiceInterface extends BaseServiceInterface
{
    public const PER_PAGE = 50;

    public function setFacility(Request $request);

    public function updateFacilities(int $id,Request $request);

    public function getFacility(int $id);

    public function deleteFacility(int $id);
}
