<?php

namespace App\Services\Admin\Facilities;

use App\Exceptions\ServiceException;
use App\Repositories\Facilities\FacilityRepositoryInterface;
use App\Repositories\FacilityType\FacilityTypeRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use function App\Helpers\translatableData;

class FacilityService implements FacilityServiceInterface
{
    public const NAME = 'facilities';

    /**
     * @var FacilityTypeRepositoryInterface
     */
    private $repository;
    /**
     * @var FacilityRepositoryInterface
     */
    private $facilityRepository;


    public function __construct(FacilityTypeRepositoryInterface $repository,
                                FacilityRepositoryInterface $facilityRepository)
    {
        $this->repository = $repository;
        $this->facilityRepository = $facilityRepository;
    }

    /**
     * @return array|mixed
     * @throws ServiceException
     */
    public function index()
    {
        try {
            ${self::NAME} = $this->repository->all(self::PER_PAGE);

            return [
                self::NAME => ${self::NAME},
            ];

        } catch (\Exception $e) {
            throw new ServiceException(__METHOD__ . $e->getMessage());
        }
    }

    public function create()
    {
        // TODO: Implement create() method.
    }

    /**
     * @param Request $request
     * @return mixed|void
     * @throws ServiceException
     */
    public function store(Request $request)
    {
        try {
            $translatableData = translatableData($this->repository->getTranslatable(), $request);

            $request->merge($translatableData);

            DB::beginTransaction();
            $this->repository->create($request->all());

            DB::commit();

        } catch (\Exception $e) {
            DB::rollBack();
            throw new ServiceException(__METHOD__ . $e->getMessage());
        }
    }

    /**
     * @param int $id
     * @return array|mixed
     * @throws ServiceException
     */
    public function show(int $id)
    {
        try {

            ${self::NAME} = $this->repository->find($id);

            return [
                self::NAME => ${self::NAME},
            ];

        } catch (\Exception $exception) {
            throw new ServiceException('error' . $exception->getMessage());
        }
    }

    public function edit(int $id)
    {
        // TODO: Implement edit() method.
    }

    /**
     * @param int $id
     * @param Request $request
     * @return mixed|void
     * @throws ServiceException
     */
    public function update(int $id, Request $request)
    {
        try {
            $translatableData = translatableData($this->repository->getTranslatable(), $request);

            $request->merge($translatableData);
            DB::beginTransaction();
            $this->repository->update($id, $request->all());
            DB::commit();

        } catch (\Exception $e) {
            DB::rollBack();
            throw new ServiceException(__METHOD__ . $e->getMessage());
        }
    }

    public function destroy(int $id, Request $request)
    {
        // TODO: Implement destroy() method.
    }

    /**
     * @param Request $request
     * @throws ServiceException
     */
    public function setFacility(Request $request)
    {
        try {
            $translatableData = translatableData($this->facilityRepository->getTranslatable(), $request);

            $request->merge($translatableData);

            DB::beginTransaction();

            $data = $this->facilityRepository->create($request->all());

            DB::commit();

            return $data;
        } catch (\Exception $e) {
            DB::rollBack();
            throw new ServiceException(__METHOD__ . $e->getMessage());
        }
    }

    public function deleteFacility(int $id)
    {
        return $this->facilityRepository->delete($id);
    }

    public function getFacility(int $id)
    {
        return $this->facilityRepository->findWhere('facility_type_id', $id, null, ['id', 'DESC']);
    }

    /**
     * @param int $id
     * @param Request $request
     * @return mixed
     * @throws ServiceException
     */
    public function updateFacilities(int $id,Request $request)
    {
        try {
            $translatableData = translatableData($this->facilityRepository->getTranslatable(), $request);

            $request->merge($translatableData);

            DB::beginTransaction();

            $data = $this->facilityRepository->update($id,$request->all());

            DB::commit();

            return $data;
        } catch (\Exception $e) {
            DB::rollBack();
            throw new ServiceException(__METHOD__ . $e->getMessage());
        }
    }
}
