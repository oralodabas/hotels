<?php


namespace App\Services\Admin;

use App\Exceptions\ServiceException;

/**
 * Class DashboardService
 * @package App\Services\Admin
 */
class DashboardService implements DashboardServiceInterface
{

    /**
     * DashboardService constructor.
     */
    public function __construct()
    {
        // TODO: Implement repository .
    }

    /**
     * @return bool
     * @throws ServiceException
     */
    public function index()
    {
        try {
            return true;

        } catch (\Exception $e) {
            throw new ServiceException(__METHOD__ . $e->getMessage());
        }
    }
}
