<?php


namespace App\Services\Admin\CountryStateCity;


use App\Exceptions\ServiceException;
use App\Repositories\Cities\CityRepository;
use App\Repositories\Cities\CityRepositoryInterface;
use App\Repositories\Countries\CountryRepositoryInterface;
use App\Repositories\States\StateRepository;
use App\Repositories\States\StateRepositoryInterface;
use Illuminate\Http\Request;

class CountryStateCityService implements CountryStateCityServiceInterface
{

    /**
     * @var CountryRepositoryInterface
     */
    private $countryRepository;
    /**
     * @var StateRepository
     */
    private $stateRepository;
    /**
     * @var CityRepository
     */
    private $cityRepository;

    public function __construct(CountryRepositoryInterface $countryRepository,
                                StateRepositoryInterface $stateRepository,
                                CityRepositoryInterface $cityRepository)
    {

        $this->countryRepository = $countryRepository;
        $this->stateRepository = $stateRepository;
        $this->cityRepository = $cityRepository;
    }

    public function index()
    {
        return $this->countryRepository->all();
    }

    /**
     * @param Request $request
     * @return array
     * @throws ServiceException
     */
    public function getState(Request $request): array
    {
        try {

            $states = $this->stateRepository->findWhere('country_id', $request->country_id);

            return [
                'states' => $states
            ];

        } catch (\Exception $e) {
            throw new ServiceException(__METHOD__ . $e->getMessage());
        }

    }

    /**
     * @param Request $request
     * @return array
     * @throws ServiceException
     */
    public function getCity(Request $request)
    {
        try {

            $cities = $this->cityRepository->findWhere('state_id', $request->state_id);

            return [
                'cities' => $cities
            ];

        } catch (\Exception $e) {
            throw new ServiceException(__METHOD__ . $e->getMessage());
        }
    }
}
