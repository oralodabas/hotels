<?php


namespace App\Services\Admin\CountryStateCity;


use App\Services\BaseServiceInterface;
use Illuminate\Http\Request;

interface CountryStateCityServiceInterface
{
    public const PerPage = 50;

    public function index();

    public function getState(Request $request);

    public function getCity(Request $request);

}
