<?php


namespace App\Services\Admin\Vehicles;


use App\Exceptions\ServiceException;
use App\Repositories\Vehicles\VehicleRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use function App\Helpers\translatableData;

class VehicleService implements VehicleServiceInterface
{
    /**
     * @var VehicleRepositoryInterface
     */
    private $repository;

    /**
     * VehicleService constructor.
     * @param VehicleRepositoryInterface $repository
     */
    public function __construct(VehicleRepositoryInterface  $repository)
    {
        $this->repository = $repository;
    }

    public const NAME = 'vehicles';

    /**
     * @return array|mixed
     * @throws ServiceException
     */
    public function index()
    {
        try {

            ${self::NAME} = $this->repository->all(self::PER_PAGE);

            return [
                self::NAME => ${self::NAME},
            ];

        } catch (\Exception $e) {
            throw new ServiceException(__METHOD__ . $e->getMessage());
        }
    }

    public function create()
    {

    }

    public function store(Request $request)
    {
        try {
            $translatableData = translatableData($this->repository->getTranslatable(), $request);

            $request->merge($translatableData);

            DB::beginTransaction();
            $this->repository->create($request->all());

            DB::commit();

        } catch (\Exception $e) {
            DB::rollBack();
            throw new ServiceException(__METHOD__ . $e->getMessage());
        }
    }

    public function show(int $id)
    {
        try {

            ${self::NAME} = $this->repository->find($id);


            return [
                self::NAME => ${self::NAME}
            ];

        } catch (\Exception $exception) {
            throw new ServiceException('error' . $exception->getMessage());
        }
    }

    public function edit(int $id)
    {
        // TODO: Implement edit() method.
    }

    public function update(int $id, Request $request)
    {
        try {
            $translatableData = translatableData($this->repository->getTranslatable(), $request);

            $request->merge($translatableData);
            DB::beginTransaction();
            $this->repository->update($id, $request->all());
            DB::commit();

        } catch (\Exception $e) {
            DB::rollBack();
            throw new ServiceException(__METHOD__ . $e->getMessage());
        }
    }

    public function destroy(int $id, Request $request)
    {
        // TODO: Implement destroy() method.
    }
}
