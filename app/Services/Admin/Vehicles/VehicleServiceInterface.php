<?php


namespace App\Services\Admin\Vehicles;


use App\Services\BaseServiceInterface;

interface VehicleServiceInterface extends BaseServiceInterface
{
    public const PER_PAGE = 50;

}
