<?php


namespace App\Services\Admin\Hotels;


use App\Exceptions\ServiceException;
use App\Http\Requests\HotelStoreRequest;
use App\Models\Hotel;
use App\Repositories\Airports\AirportRepositoryInterface;
use App\Repositories\Cities\CityRepositoryInterface;
use App\Repositories\Countries\CountryRepositoryInterface;
use App\Repositories\FacilityType\FacilityTypeRepositoryInterface;
use App\Repositories\Faq\FaqInterfaceRepository;
use App\Repositories\Hotels\HotelRepositoryInterface;
use App\Repositories\Images\ImageRepositoryInterface;
use App\Repositories\States\StateRepositoryInterface;
use App\Repositories\Transfer\TransferRepositoryInterface;
use App\Repositories\Vehicles\VehicleRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use phpDocumentor\Reflection\Types\Collection;

/**
 * Class HotelService
 * @package App\Services\Hotels
 */
class HotelService implements HotelServiceInterface
{

    /**
     * @var HotelRepositoryInterface
     */
    private $repository;
    /**
     * @var CountryRepositoryInterface
     */
    private $countryRepository;
    /**
     * @var StateRepositoryInterface
     */
    private $stateRepository;
    /**
     * @var CityRepositoryInterface
     */
    private $cityRepository;
    /**
     * @var ImageRepositoryInterface
     */
    private $imageRepository;
    /**
     * @var FacilityTypeRepositoryInterface
     */
    private $facilityTypeRepository;
    /**
     * @var AirportRepositoryInterface
     */
    private $airportRepository;
    /**
     * @var VehicleRepositoryInterface
     */
    private $vehicleRepository;
    /**
     * @var TransferRepositoryInterface
     */
    private $transferRepository;
    /**
     * @var FaqInterfaceRepository
     */
    private $faqRepository;

    /**
     * HotelService constructor.
     * @param HotelRepositoryInterface $repository
     * @param CountryRepositoryInterface $countryRepository
     * @param StateRepositoryInterface $stateRepository
     * @param CityRepositoryInterface $cityRepository
     * @param ImageRepositoryInterface $imageRepository
     * @param FacilityTypeRepositoryInterface $facilityTypeRepository
     * @param AirportRepositoryInterface $airportRepository
     * @param VehicleRepositoryInterface $vehicleRepository
     * @param TransferRepositoryInterface $transferRepository
     * @param FaqInterfaceRepository $faqRepository
     */
    public function __construct(HotelRepositoryInterface $repository,
                                CountryRepositoryInterface $countryRepository,
                                StateRepositoryInterface $stateRepository,
                                CityRepositoryInterface $cityRepository,
                                ImageRepositoryInterface $imageRepository,
                                FacilityTypeRepositoryInterface $facilityTypeRepository,
                                AirportRepositoryInterface $airportRepository,
                                VehicleRepositoryInterface $vehicleRepository,
                                TransferRepositoryInterface $transferRepository,
                                FaqInterfaceRepository $faqRepository)
    {
        $this->repository = $repository;
        $this->countryRepository = $countryRepository;
        $this->stateRepository = $stateRepository;
        $this->cityRepository = $cityRepository;
        $this->imageRepository = $imageRepository;
        $this->facilityTypeRepository = $facilityTypeRepository;
        $this->airportRepository = $airportRepository;
        $this->vehicleRepository = $vehicleRepository;
        $this->transferRepository = $transferRepository;
        $this->faqRepository = $faqRepository;
    }

    /**
     * @return array
     * @throws ServiceException
     */
    public function index(): array
    {
        try {
            $hotels = $this->repository->all(self::PerPage);

            return [
                'hotels' => $hotels,
            ];

        } catch (\Exception $e) {
            throw new ServiceException(__METHOD__ . $e->getMessage());
        }
    }

    /**
     * @return array|mixed
     */
    public function create(): array
    {
        $countries = $this->countryRepository->all();
        $facilityType = $this->facilityTypeRepository->all();
        $images = null;//$this->imageRepository->all(self::PerPage);
        $airports = $this->airportRepository->all();
        $vehicles = $this->vehicleRepository->all();

        return [
            'countries' => $countries,
            'facilityType' => $facilityType,
            'images' => $images,
            'selectedStateId' => 0,
            'selectedCountryId' => 0,
            'facilities' => [],
            'hotelImages' => null,
            'rooms' => null,
            'airports' => $airports,
            'vehicles' => $vehicles,
            'transfers' => null,
            'faq' => [],
        ];
    }

    private function mapImages($images,$flag)
    {
        return collect($images)->map(function ($i) use($flag) {
            $flag = isset($flag[$i]) ? 1 : 0;
            return ['image_id'=>$i,'flag' => $flag];
        });
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws ServiceException
     */
    public function store(Request $request)
    {
        try {


            $dataTranslatable = $this->translatableData($request);
            $this->translatableSlug($dataTranslatable, $request);


            DB::beginTransaction();

            if ($request->has('logo')) {
                $this->repository->uploadLogo($request);
            }

            $hotel = $this->repository->create($request->all());

            if ($request->has('selectedImages')) {
                $hotel->images()->attach($this->mapImages($request->selectedImages,$request->hotel_image_flag));
            }

            if ($request->has('facilities')) {
                $hotel->facilities()->attach($request->facilities);
            }

            if ($request->has('rooms')) {
                $hotel->rooms()->attach($request->rooms);
            }

            $this->transferCreate($hotel->id, $request);
            $this->faqCreate($hotel->id, $request);

            DB::commit();

            return $hotel;

        } catch (\Exception $e) {
            DB::rollBack();
            throw new ServiceException(__METHOD__ . $e->getMessage());
        }
    }

    /**
     * @param int $id
     * @return array|mixed
     * @throws ServiceException
     */
    public function show(int $id): array
    {
        try {

            $hotel = $this->repository->find($id);

            $facilityType = $this->facilityTypeRepository->all();
            $images = $hotel->images()->get();
            $airports = $this->airportRepository->all();
            $vehicles = $this->vehicleRepository->all();

            $selectedState = $hotel->city()->first()
                ? $hotel->city()->first()->state()->first(['id', 'name'])
                : null;

            $selectedCountry = $hotel->city()->first()
                ? $hotel->city()->first()->state()->first()->country()->first(['id', 'name'])
                : null;

            $countries = $this->countryRepository->all();

            $states = $selectedCountry
                ? $this->stateRepository->findWhere('country_id', $selectedCountry->id)
                : null;

            $cities = $selectedState
                ? $this->cityRepository->findWhere('state_id', $selectedState->id)
                : null;

            if (!$hotel)
                throw new ServiceException('Not found data');


            return [
                'hotel' => $hotel,
                'countries' => $countries,
                'states' => $states,
                'cities' => $cities,
                'selectedStateId' => $selectedState ? $selectedState->id : 0,
                'selectedCountryId' => $selectedCountry ? $selectedCountry->id : 0,
                'facilityType' => $facilityType,
                'images' => $images,
                'rooms' => $hotel->rooms()->get()->count() > 0
                    ? $hotel->rooms()->get()
                    : null,
                'facilities' => $hotel->facilities()->get()->count() > 0
                    ? $hotel->facilities()->get()->pluck('id')->toArray()
                    : [],
                'hotelImages' => $hotel->images()->get()->count() > 0
                    ? $hotel->images()->get()
                    : null,
                'airports' => $airports,
                'vehicles' => $vehicles,
                'transfers' => $this->transferRepository->findWhere('hotel_id', $id),
                'faq' => $this->faqRepository->findWhere('hotel_id', $id),
            ];

        } catch (\Exception $e) {
            throw new ServiceException(__METHOD__ . $e->getMessage());
        }
    }

    public function edit(int $id)
    {
        // TODO: Implement edit() method.
    }

    /**
     * @param int $id
     * @param Request $request
     * @return mixed
     * @throws ServiceException
     */
    public function update(int $id, Request $request)
    {
        try {

            $dataTranslatable = $this->translatableData($request);

            $this->translatableSlug($dataTranslatable, $request);

            DB::beginTransaction();

            if ($request->has('logo')) {
                $this->repository->uploadLogo($request);
            }

            $hotel = $this->repository->update($id, $request->all());

            $hotel->images()->sync($this->mapImages($request->selectedImages,$request->hotel_image_flag));


            if ($request->has('facilities')) {
                $hotel->facilities()->sync($request->facilities);
            }

            if ($request->has('rooms')) {
                $hotel->rooms()->attach($request->rooms);
            }

            $this->transferCreate($id, $request);
            $this->faqCreate($id, $request);

            DB::commit();

            return $hotel;

        } catch (\Exception $e) {
            throw new ServiceException(__METHOD__ . $e->getMessage());
        }
    }

    public function destroy(int $id, $request)
    {
        // TODO: Implement destroy() method.ImageContro
    }

    /**
     * @param Request $request
     * @return array
     */
    private function translatableData(Request $request): array
    {
        $hotelInsertableData = [];
        $languages = config('app.available_locales');

        foreach ($languages as $language) {
            foreach ($this->repository->getTranslatable() as $i => $val) {
                $hotelInsertableData[$val][$language] = $request->{$val . '_' . $language};
            }
        }

        $request->merge($hotelInsertableData);

        return $hotelInsertableData;

    }

    /**
     * @param array $translatableNames
     * @param Request $request
     */
    private function translatableSlug(array $translatableNames, Request $request): void
    {
        $slugs = [];

        foreach ($translatableNames['slug'] as $language => $name) {
            $slugs['slug'][$language] = Str::slug($name);
        }

        $request->merge($slugs);

    }

    /**
     * @param Request $request
     * @return mixed
     * @throws ServiceException
     */
    public function getHotels(Request $request)
    {
        try {
            $data = [];
            $hotels = $this->repository->all(self::PerPage);
            foreach ($hotels as $hotel) {
                $data[] = [
                    'id' => $hotel->id,
                    'name' => $hotel->getTranslation('name', 'tr'),
                ];
            }
            return $data;
        } catch (\Exception $e) {
            throw new ServiceException('ajax request error.. ' . $e->getMessage());
        }
    }

    /**
     * @param int $id
     * @param Request $request
     * @return mixed
     * @throws ServiceException
     */
    public function updateStatusHotel(int $id, Request $request)
    {
        try {

            $hotel = $this->repository->find($id);

            if ($hotel->getTranslation('name', app()->getLocale())) {
                $status = $hotel->status == Hotel::STATUS_ACTIVE ? Hotel::STATUS_PASSIVE : Hotel::STATUS_ACTIVE;
                $data = ['status' => $status];
                return $this->repository->update($id, $data);
            }
            throw new ServiceException('missing fields');
        } catch (\Exception $e) {
            throw new ServiceException('ajax request error.. ' . $e->getMessage());
        }
    }

    private function transferCreate(int $id, Request $request): void
    {
        if (isset($request->airlines) && (count($request->airlines) == count($request->vehicles)) == count($request->price)) {

            for ($i = 0; $i < count($request->price); $i++) {
                if ($request->airlines[$i] != null && $request->vehicles[$i] != null
                    && $request->price[$i] != null) {
                    if ($i == 0) {
                        $this->transferRepository->deleteTransferForHotelId($id);
                    }
                    $this->transferRepository->create([
                        'hotel_id' => $id,
                        'airport_id' => $request->airlines[$i],
                        'vehicle_id' => $request->vehicles[$i],
                        'price' => $request->price[$i],
                    ]);
                }
            }
        }

    }

    private function faqCreate(int $id,Request $request)
    {
        $data = [
            'hotel_id'=>$id
        ];

        $languages = config('app.available_locales');

        if(isset($request->faq_question_tr))
        {

            for ($i = 0; $i < count($request->faq_question_tr); $i++) {
                if ($i == 0) {
                   $this->faqRepository->deleteFaqForHotelId($id);
                }
                foreach ($languages as $language) {
                        $data['questions'][$language] = $request->{'faq_question_'. $language}[$i];
                        $data['answers'][$language] = $request->{'faq_answers_'. $language}[$i];
                }

                $this->faqRepository->create($data);
            }
        }
    }
}
