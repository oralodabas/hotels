<?php


namespace App\Services\Admin\Hotels;


use App\Services\BaseServiceInterface;
use Illuminate\Http\Request;

interface HotelServiceInterface extends BaseServiceInterface
{
    public const PerPage = 50;

    public function getHotels(Request $request);

    public function updateStatusHotel(int $id, Request $request);
}
