<?php


namespace App\Services\Admin\Images;


use App\Exceptions\ServiceException;
use App\Repositories\Images\ImageRepositoryInterface;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class ImageService implements ImageServiceInterface
{
    /**
     * @var ImageRepositoryInterface
     */
    private $repository;

    /**
     * ImageService constructor.
     * @param ImageRepositoryInterface $repository
     */
    public function __construct(ImageRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @return mixed
     */
    public function index()
    {
        $images = $this->repository->all(self::perPage);

        return [
            'images' => $images,
        ];
    }

    public function create()
    {
        // TODO: Implement create() method.
    }

    /**
     * @param Request $request
     * @return RedirectResponse|mixed
     * @throws ServiceException
     */
    public function store(Request $request)
    {
        try {

            $data = $this->repository->uploadImageOrCreate($request);

            if (count($data) == 0) {
                throw new ServiceException('Empty is data');
            }

            return back()->with('success', 'Images uploaded successfully');

        } catch (Exception $e) {
            throw new ServiceException(__METHOD__ . $e->getMessage());

        }
    }

    public function show(int $id)
    {
        // TODO: Implement show() method.
    }

    public function edit(int $id)
    {
        // TODO: Implement edit() method.
    }

    public function update(int $id, Request $request)
    {
        // TODO: Implement update() method.
    }

    public function destroy(int $id, Request $request)
    {
        try {
            $image = $this->repository->find($id);
            if (!$image) {
                throw new ServiceException('Not Found Images');
            }
            Storage::delete($request->path . $request->name);
            $this->repository->delete($id);

        } catch (\Exception $e) {
            throw new ServiceException(__METHOD__ . $e->getMessage());
        }

    }

    public function getImages($request)
    {
        return [
            'images' => $this->repository->all(self::perPage)
        ];
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws ServiceException
     */
    public function setImages(Request $request)
    {
        try {

            $id = $this->repository->uploadImageOrCreate($request);

            if (count($id) == 0) {
                throw new ServiceException('Empty is data');
            }

            return $this->repository->whereIn('id', $id);


        } catch (Exception $e) {
            throw new ServiceException(__METHOD__ . $e->getMessage());

        }
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws ServiceException
     */
    public function setTitleImages(Request $request)
    {
        try {

            return $this->repository->update($request->id, $request->all());

        } catch (Exception $e) {
            throw new ServiceException(__METHOD__ . $e->getMessage());

        }
    }


}
