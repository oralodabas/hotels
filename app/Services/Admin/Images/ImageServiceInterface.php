<?php


namespace App\Services\Admin\Images;


use App\Services\BaseServiceInterface;
use Illuminate\Http\Request;

interface ImageServiceInterface extends BaseServiceInterface
{
    public const perPage = 50;
    public function getImages(Request $request);
    public function setImages(Request $request);
    public function setTitleImages(Request $request);

}
