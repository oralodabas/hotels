<?php


namespace App\Services\Admin\Airports;


use App\Services\BaseServiceInterface;

interface AirportServiceInterface extends BaseServiceInterface
{

    public const PER_PAGE = 50;

}
