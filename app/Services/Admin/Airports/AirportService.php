<?php


namespace App\Services\Admin\Airports;


use App\Exceptions\ServiceException;
use App\Repositories\Airports\AirportRepositoryInterface;
use App\Repositories\Countries\CountryRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use function App\Helpers\translatableData;

class AirportService implements AirportServiceInterface
{
    /**
     * @var AirportRepositoryInterface
     */
    private $repository;
    /**
     * @var CountryRepositoryInterface
     */
    private $countryRepository;

    /**
     * AirportService constructor.
     * @param AirportRepositoryInterface $repository
     * @param CountryRepositoryInterface $countryRepository
     */
    public function __construct(AirportRepositoryInterface $repository,
                                CountryRepositoryInterface $countryRepository)
    {

        $this->repository = $repository;
        $this->countryRepository = $countryRepository;
    }

    public const NAME = 'airports';

    /**
     * @return array|mixed
     * @throws ServiceException
     */
    public function index()
    {
        try {

            ${self::NAME} = $this->repository->all(self::PER_PAGE);

            return [
                self::NAME => ${self::NAME},
            ];

        } catch (\Exception $e) {
            throw new ServiceException(__METHOD__ . $e->getMessage());
        }
    }

    public function create()
    {
        $countries = $this->countryRepository->all();

        return [
            'countries' => $countries,
            'selectedCountryId' => 0,
            'selectedStateId' => 0,

        ];

    }

    public function store(Request $request)
    {
        try {
            $translatableData = translatableData($this->repository->getTranslatable(), $request);

            $request->merge($translatableData);

            DB::beginTransaction();
            $this->repository->create($request->all());

            DB::commit();

        } catch (\Exception $e) {
            DB::rollBack();
            throw new ServiceException(__METHOD__ . $e->getMessage());
        }
    }

    public function show(int $id)
    {
        try {

            ${self::NAME} = $this->repository->find($id);
            $selectedState = ${self::NAME}->city()->first()
                ? ${self::NAME}->city()->first()->state()->first(['id', 'name'])
                : null;

            $selectedCountry = ${self::NAME}->city()->first()
                ? ${self::NAME}->city()->first()->state()->first()->country()->first(['id', 'name'])
                : null;

            $countries = $this->countryRepository->all();

            return [
                self::NAME => ${self::NAME},
                'countries' => $countries,
                'selectedStateId' => $selectedState ? $selectedState->id : 0,
                'selectedCountryId' => $selectedCountry ? $selectedCountry->id : 0,
            ];

        } catch (\Exception $exception) {
            throw new ServiceException('error' . $exception->getMessage());
        }
    }

    public function edit(int $id)
    {
        // TODO: Implement edit() method.
    }

    public function update(int $id, Request $request)
    {
        try {
            $translatableData = translatableData($this->repository->getTranslatable(), $request);

            $request->merge($translatableData);
            DB::beginTransaction();
            $this->repository->update($id, $request->all());
            DB::commit();

        } catch (\Exception $e) {
            DB::rollBack();
            throw new ServiceException(__METHOD__ . $e->getMessage());
        }
    }

    public function destroy(int $id, Request $request)
    {
        // TODO: Implement destroy() method.
    }
}
