<?php


namespace App\Services\Admin\Rooms;


use App\Services\BaseServiceInterface;
use Illuminate\Http\Request;

interface RoomServiceInterface extends BaseServiceInterface
{
    public const PerPage = 50;

    public function setRooms(Request $request);
    public function updateRooms(int $id, Request $request);
    public function getRooms(int $id);
    public function deleteRooms(Request $request);
}
