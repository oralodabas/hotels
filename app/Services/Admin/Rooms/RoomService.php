<?php


namespace App\Services\Admin\Rooms;


use App\Exceptions\ServiceException;
use App\Repositories\Hotels\HotelRepositoryInterface;
use App\Repositories\Images\ImageRepositoryInterface;
use App\Repositories\Rooms\RoomRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use function App\Helpers\translatableData;

class RoomService implements RoomServiceInterface
{
    /**
     * @var RoomRepositoryInterface
     */
    private $repository;
    /**
     * @var ImageRepositoryInterface
     */
    private $imageRepository;
    /**
     * @var HotelRepositoryInterface
     */
    private $hotelRepository;

    public function __construct(RoomRepositoryInterface $repository,
                                ImageRepositoryInterface $imageRepository,
                                HotelRepositoryInterface $hotelRepository)
    {
        $this->repository = $repository;
        $this->imageRepository = $imageRepository;
        $this->hotelRepository = $hotelRepository;
    }

    /**
     * @return array|mixed
     * @throws ServiceException
     */
    public function index()
    {
        try {
            $rooms = $this->repository->all(self::PerPage);

            return [
                'rooms' => $rooms,
            ];

        } catch (\Exception $e) {
            throw new ServiceException(__METHOD__ . $e->getMessage());
        }
    }

    public function create()
    {
        // TODO: Implement create() method.
    }

    public function store(Request $request)
    {
        try {

            $this->translatableData($request);
            $hotel = $this->hotelRepository->find($request->hotel_id);
            DB::beginTransaction();

            $rooms = $this->repository->create($request->all());

            $rooms->hotels()->attach($hotel, ['quantity' => $request->quantity]);
            if ($request->hasFile('files')) {
                $files = $this->imageRepository->uploadImageOrCreate($request);
                $rooms->images()->attach($files);
            }

            DB::commit();

        } catch (\Exception $e) {
            DB::rollBack();
            throw new ServiceException(__METHOD__ . $e->getMessage());
        }
    }

    public function show(int $id)
    {
        try {

            $room = $this->repository->find($id);

            return [
                'room' => $room
            ];

        } catch (\Exception $exception) {
            throw new ServiceException('error' . $exception->getMessage());
        }
    }

    public function edit(int $id)
    {
        // TODO: Implement edit() method.
    }

    public function update(int $id, Request $request)
    {
        try {
            $this->translatableData($request);
            $hotel = $this->hotelRepository->find($request->hotel_id);

            DB::beginTransaction();

            $rooms =  $this->repository->update($id,$request->all());
            $rooms->hotels()->sync($hotel, ['quantity' => $request->quantity]);

            if ($request->hasFile('files')) {
                $files = $this->imageRepository->uploadImageOrCreate($request);
                $rooms->images()->attach($files);
            }

            DB::commit();

        } catch (\Exception $e) {
            DB::rollBack();
            throw new ServiceException(__METHOD__ . $e->getMessage());
        }
    }

    public function destroy(int $id, Request $request)
    {
        // TODO: Implement destroy() method.
    }

    /**
     * @param Request $request
     * @return array
     */
    private function translatableData(Request $request): array
    {
        $roomsInsertableData = [];

        $languages = config('app.available_locales');


        foreach ($languages as $language) {
            foreach ($this->repository->getTranslatable() as $i => $val) {
                $roomsInsertableData[$val][$language] = $request->{$val . '_' . $language};
            }
        }

        $request->merge($roomsInsertableData);

        return $roomsInsertableData;

    }

    /**
     * @param Request $request
     * @return mixed
     * @throws ServiceException
     */
    public function setRooms(Request $request)
    {
        try {

            $data =  translatableData($this->repository->getTranslatable(),$request);
            $request->merge($data);

            return $this->repository->create($request->all());

        } catch (\Exception $e) {

            throw new ServiceException(__METHOD__ . $e->getMessage());
        }
    }

    /**
     * @param int $id
     * @param Request $request
     * @return mixed
     * @throws ServiceException
     */
    public function updateRooms(int $id, Request $request)
    {
        try {

            $room = $this->repository->find($id);

            $data =  translatableData($this->repository->getTranslatable(),$request);
            $request->merge($data);

            $room->images()->sync($this->mapImages($request->room_image_id,$request->room_image_flag));

            $room = $this->repository->update($id,$request->all());

            return [
                'rooms' => $room,
                'images' => $room->images()->get(),
            ];

        } catch (\Exception $e) {

            throw new ServiceException(__METHOD__ . $e->getMessage());
        }
    }

    private function mapImages($images,$flag)
    {
        return collect($images)->map(function ($i) use($flag) {
            $flag = in_array($i,$flag) ? 1 : 0;
            return ['image_id'=>$i,'flag' => $flag];
        });

    }


    /**
     * @param int $id
     * @return mixed
     * @throws ServiceException
     */
    public function getRooms(int $id)
    {
        try {

            $room = $this->repository->find($id);

            return [
                'rooms' => $room,
                'images' => $room->images()->get(),
            ];

        } catch (\Exception $e) {

            throw new ServiceException(__METHOD__ . $e->getMessage());
        }
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws ServiceException
     */
    public function deleteRooms(Request $request)
    {
        try {

           return $this->repository->delete($request->room_id);


        } catch (\Exception $e) {

            throw new ServiceException(__METHOD__ . $e->getMessage());
        }
    }
}
