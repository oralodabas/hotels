<?php


namespace App\Services\Admin\Reviews;


use App\Services\BaseServiceInterface;
use Illuminate\Http\Request;

interface ReviewServiceInterface extends BaseServiceInterface
{

    const PER_PAGE = 50;

    public function updateStatusReview(int $id,Request $request);
}
