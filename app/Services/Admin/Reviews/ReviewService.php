<?php


namespace App\Services\Admin\Reviews;


use App\Exceptions\ServiceException;
use App\Models\Review;
use App\Repositories\Reviews\ReviewRepositoryInterface;
use Illuminate\Http\Request;

class ReviewService implements ReviewServiceInterface
{

    const NAME = 'reviews';
    /**
     * @var ReviewRepositoryInterface
     */
    private $repository;

    public function __construct(ReviewRepositoryInterface $repository)
    {

        $this->repository = $repository;
    }

    /**
     * @return array|mixed
     * @throws ServiceException
     */
    public function index()
    {
        try {

            ${self::NAME} = $this->repository->all(self::PER_PAGE);

            return [
                self::NAME => ${self::NAME},
            ];

        } catch (\Exception $e) {
            throw new ServiceException(__METHOD__ . $e->getMessage());
        }
    }

    public function create()
    {
        // TODO: Implement create() method.
    }

    public function store(Request $request)
    {
        // TODO: Implement store() method.
    }

    public function show(int $id)
    {
        // TODO: Implement show() method.
    }

    public function edit(int $id)
    {
        // TODO: Implement edit() method.
    }

    public function update(int $id, Request $request)
    {
        // TODO: Implement update() method.
    }

    public function destroy(int $id, Request $request)
    {
        // TODO: Implement destroy() method.
    }

    /**
     * @param int $id
     * @param Request $request
     * @return mixed
     * @throws ServiceException
     */
    public function updateStatusReview(int $id, Request $request)
    {
        try {

            $review = $this->repository->find($id);
            $status = $review->status == Review::STATUS_ACTIVE ? Review::STATUS_PASSIVE : Review::STATUS_ACTIVE;
            $data = ['status' => $status];

            return $this->repository->update($id, $data);

        } catch (\Exception $e) {
            throw new ServiceException('ajax request error.. ' . $e->getMessage());
        }
    }
}
