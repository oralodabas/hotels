<?php


namespace App\Services;


use Illuminate\Http\Request;

interface BaseServiceInterface
{
    /**
     * @return mixed
     */
    public function index();

    /**
     * @return mixed
     */
    public function create();

    /**
     * @param $request
     * @return mixed
     */
    public function store(Request $request);

    /**
     * @param int $id
     * @return mixed
     */
    public function show(int $id);

    /**
     * @param int $id
     * @return mixed
     */
    public function edit(int $id);

    /**
     * @param int $id
     * @param  $request
     * @return mixed
     */
    public function update(int $id,Request $request);

    /**
     * @param int $id
     * @param  $request
     * @return mixed
     */
    public function destroy(int $id,Request $request);


}
