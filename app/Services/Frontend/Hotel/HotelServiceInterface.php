<?php


namespace App\Services\Frontend\Hotel;


use Illuminate\Http\Request;

interface HotelServiceInterface
{
    public function show(Request $request);
    public function index(Request $request);
    public function crawler(Request $request);
    public function getSessionRoom(Request $request);
    public function addReview(int $id,Request $request);
}
