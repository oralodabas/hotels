<?php


namespace App\Services\Frontend\Hotel;


use App\Classes\Tools\CrawlerHotel;
use App\Exceptions\ServiceException;
use App\Models\Hotel;
use App\Repositories\Hotels\HotelRepositoryInterface;
use App\Repositories\Reviews\ReviewRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class HotelService implements HotelServiceInterface
{
    const ADULTS = 2;
    const CHILDREN = 0;
    /**
     * @var HotelRepositoryInterface
     */
    private $repository;

    private $locale;
    /**
     * @var ReviewRepositoryInterface
     */
    private $reviewRepository;

    /**
     * HotelService constructor.
     * @param HotelRepositoryInterface $repository
     * @param ReviewRepositoryInterface $reviewRepository
     */
    public function __construct(HotelRepositoryInterface $repository, ReviewRepositoryInterface $reviewRepository)
    {
        $this->repository = $repository;
        $this->reviewRepository = $reviewRepository;
    }


    /**
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        $hotels = $this->repository->getHotels();

        $groups = $hotels->groupBy(function ($item, $key) use ($hotels) {
            $name = $item->getTranslation('name', app()->getLocale(), false) ?? null;
            return $name[0];

        })->sortBy(function ($item, $key) {
            return $key;
        });

        return [
            'hotelGroups' => $groups
        ];
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws ServiceException
     */
    public function show(Request $request)
    {

        $columnName = 'slug->' . app()->getLocale();

        $hotels = $this->repository->findWhere($columnName, $request->slug);
        $hotel = $hotels->first();

        if ($hotel == null) {
            throw new ServiceException('not found page!');
        }

        $reviews = $this->reviewRepository->findWhereAndWhere(['hotel_id'=>$hotel->id,'status'=>'active'],5);

        $facilities = $hotel->facilities()->get()->groupBy('facility_type_id')->map(function ($item) {
            return [
                'name' => $item->first()->facilityType()->first()->getTranslation('name', app()->getLocale(), false) ?? null,
                'icon_name' => $item->first()->facilityType()->first()->icon_name,
                'facilities' => $item,

            ];
        });


        return [
            'hotel' => $hotel,
            'facilities' => $facilities,
            'reviews' => $reviews
        ];
    }

    public function crawler(Request $request)
    {
        try {
            $data = [
                'start' => Carbon::parse($request->start)->format('Y-m-d'),
                'end' => Carbon::parse($request->end)->format('Y-m-d'),
                'adults' => $request->adults > 0 ? $request->adults : self::ADULTS,
                'children' => $request->kids > 0 ? $request->children : self::CHILDREN,
            ];

            $request->merge($data);

            $crawler = new CrawlerHotel($request);

            return $crawler->init();

        } catch (\Exception $e) {
            throw new ServiceException('ajax request error.. ' . $e->getMessage());

        }

    }

    /**
     * @param Request $request
     * @return array|mixed
     * @throws ServiceException
     */
    public function getSessionRoom(Request $request)
    {
        try {
            $key = Session::get('curl_url') . Session::get('start') .
                Session::get('end') . Session::get('adults');

            $rooms = Session::get($key) ? Session::get($key) : [];

            foreach ($rooms as $room) {
                if ($room['Id'] == $request->id) {
                    return $room;
                }
            }

            return [];


        } catch (\Exception $e) {
            throw new ServiceException('get session_error.. ' . $e->getMessage());

        }
    }

    /**
     * @param int $id
     * @param Request $request
     * @return mixed
     * @throws ServiceException
     */
    public function addReview(int $id, Request $request)
    {
        try {
            $hotel = $this->repository->find($id);

            if (!$hotel)
                throw new ServiceException('comment not found hotel.. ');


            $info = [
                'name' => $request->name,
                'sur_name' => $request->sur_name,
                'email' => $request->email,
                'country' => $request->country,
                'month' => $request->month,
                'year' => $request->year,
            ];

            $data = [
                'description' => Str::limit($request->comment, 5000),
                'point' => $request->rating,
                'hotel_id' => $id,
                'info' => json_encode($info),
            ];

            return $this->reviewRepository->create($data);


        } catch (\Exception $e) {
            throw new ServiceException('add review error.. ' . $e->getMessage());

        }
    }
}
